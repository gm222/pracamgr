using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BAD
{
    /// <summary>
    /// BAD reactor searches all components on the gameobject which implement IReactorLoad, then executes the 
    /// graphs returned by those components.
    /// </summary>
    [Serializable]
    public class BADReactor : MonoBehaviour
    {
        [Range(0f, 10f)]
        public float tickDuration = 0f;
        [System.NonSerialized]
        public bool fastforward = false;
        [System.NonSerialized]
        public bool pause = false;
        [System.NonSerialized]
        public bool step = false;

        public bool debug = false;

        public float deltaTime { get; private set; }
        [SerializeField]
        public List<Node> runningGraphs = new List<Node>();

        //public TextAsset badCode;
        public Blackboard blackboard = new Blackboard();

        Coroutine _coroutine = null;


        public void StartLogic()
        {
            _actionFinished = false;
            foreach (Node roots in runningGraphs)
            {
                //Reset(roots);
                _coroutine = StartCoroutine(RunReactor(roots.GetNodeTask()));
            }
        }

        public void Reset(Node node)
        {
            node.running = false;
            node.state = null;
            Branch branch = node as Branch;
            if (branch != null)
            {
                foreach (Node child in branch.children)
                {
                    Reset(child);
                }
            }
        }

        public void ClearRuningGraphs()
        {
            runningGraphs.Clear();
        }

        public void StopLocal()
        {
            _actionFinished = true;

            if (_coroutine != null)
                StopCoroutine(_coroutine);
            _coroutine = null;
        }

        private bool _actionFinished = false;

        //TODO: Make a GameManager that manipulate with the time (GUI buttons to speed it)
        IEnumerator RunReactor(IEnumerator<BAD.NodeResult> task)
        {
            //This helps mitigate the stampeding herd problem when the level starts with many AI's waiting to start.
            yield return new WaitForSeconds(UnityEngine.Random.Range(0f, 0.15f));
            //TODO: zrobi� w gamemanager, �e jak b�dzie false wtedy zbieramy info o wszystkich obiektach, po prostu przeszukuj�c obiekty a nast�pni� mo�na z nich
            //dane np. ilo�� kul w magazynie, gdzie si� znajduje, odczyta� ostatnie zadanie, jak daleko jest przeciwnik i wrzuci� do silnika mutacji
            while (!_actionFinished || !TGameManager.Instance.IsBreak)
            {
                WaitForSeconds delay = null;
                if (tickDuration > 0f)
                    delay = new WaitForSeconds(tickDuration);

                float startTick = Time.time;

#if UNITY_EDITOR
                if (step)
                {
                    step = false;
                    pause = true;
                    yield return delay;
                    task.MoveNext();
                }

                if (pause)
                {
                    yield return null;
                    continue;
                }

                if (fastforward)
                {
                    fastforward = false;
                    yield return null;
                }
                else
                    yield return delay;

                try
                {
                    task.MoveNext();
                }
                catch (Exception) { };

#else
                if (fastforward) {
                    fastforward = false;
                    yield return null;
                } else {
                    yield return delay;
                } 
                task.MoveNext ();
#endif
                deltaTime = Time.time - startTick;
            }

            //runningGraphs.Clear();
        }
    }
}


