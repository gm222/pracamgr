using System.Collections.Generic;
using UnityEngine;

namespace BAD {

    public class BB : Leaf {
        private Symbol op, _name;
        private object value;

        public override void Apply(object[] args) {
            if(args.Length >= 1)
                op = (Symbol)args[0];
            if(args.Length >= 2)
                _name = (Symbol)args[1];
            if(args.Length >= 3)
                value = args[2];
        }

        public override string ToString() {
            if(op == null || _name == null || value == null)
                return "BB";
            return string.Format("BB " + op.name + " " + _name.name + " " + value.ToString());
        }

        public override IEnumerator<NodeResult> NodeTask() {
#if UNITY_EDITOR
            yield return NodeResult.Continue;
#endif
            float v;
            if(value is float)
                v = (float)value;
            else
                v = reactor.blackboard.Get((string)value);

            switch(op) {
                case "set":
                    reactor.blackboard.Set(_name, v);
                    break;

                case "inc":
                    reactor.blackboard.Inc(_name, v);
                    break;

                case "dec":
                    reactor.blackboard.Dec(_name, v);
                    break;

                case "mul":
                    reactor.blackboard.Mul(_name, v);
                    break;

                case "div":
                    reactor.blackboard.Div(_name, v);
                    break;

                case "rnd":
                    reactor.blackboard.Set(_name, Random.value);
                    break;
            }
            yield return NodeResult.Success;
        }
    }
}