using System.Collections.Generic;
using UnityEngine;

namespace BAD
{

    /// <summary>
    /// Sleep for a number of seconds before returning Success. Variance parameter modifies the duration by a random amount.
    /// </summary>
    public class Sleep : Leaf
    {
        public float seconds = 1;
        public float variance = 0.1f;

        public override void Apply(object[] arguments)
        {
            seconds = (float)arguments[0];
        }

        //protected override void ResolveArguments() {
        //    seconds = GetArg<float>(0);
        //    variance = GetArg<float>(1);
        //}

        public override IEnumerator<NodeResult> NodeTask()
        {
            float N = Time.time;
            float T = seconds + UnityEngine.Random.Range(-variance / 2, variance / 2);
            while (Time.time - T <= N)
                yield return NodeResult.Continue;
            yield return NodeResult.Success;
        }
    }
}