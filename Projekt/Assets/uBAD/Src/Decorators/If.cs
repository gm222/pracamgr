using System.Collections.Generic;

namespace BAD
{

    /// <summary>
    /// Runs it's child node if a System.Func<bool> method returns true.
    /// </summary>
    public class If : Decorator
    {
        private ComponentMethodLookup method;

        public override void Apply(object[] args)
        {
            this.method = (ComponentMethodLookup)args[0];
        }

        public override IEnumerator<NodeResult> NodeTask()
        {
            if (ChildIsMissing())
            {
                yield return NodeResult.Failure;
            }
            bool result = (bool)method.Invoke();
            if (result)
            {
                IEnumerator<NodeResult> task = children[0].GetNodeTask();
                while (task.MoveNext())
                {
                    yield return task.Current;
                }
            }
            else
            {
                yield return NodeResult.Failure;
            }
        }


    }
}