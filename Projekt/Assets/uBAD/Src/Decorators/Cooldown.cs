using System.Collections.Generic;
using UnityEngine;

namespace BAD
{

    /// <summary>
    /// Runs a child node if it's cooldown timer less than 0;
    /// When the child executes the timer is set to T+seconds. Used for rate limiting.
    /// </summary>
    public class Cooldown : Decorator
    {
        public float seconds = 1;
        private float lastRunTime = -1;

        //protected override void ResolveArguments() {
        //    seconds = GetArg<float>(0);
        //}

        public override void Apply(object[] arguments)
        {
            seconds = (float)arguments[0];
        }

        public override IEnumerator<NodeResult> NodeTask()
        {
            if (ChildIsMissing())
            {
                yield return NodeResult.Failure;
                yield break;
            }
            if (lastRunTime < 0)
                lastRunTime = Time.time;
            float T = Time.time - lastRunTime;
            if (T > seconds)
            {
                lastRunTime = Time.time;
                IEnumerator<NodeResult> task = children[0].GetNodeTask();
                while (task.MoveNext())
                {
                    NodeResult t = task.Current;
                    if (t == NodeResult.Continue)
                    {
                        yield return NodeResult.Continue;
                    }
                    else if (t == NodeResult.Success)
                    {
                        yield return NodeResult.Success;
                        break;
                    }
                    else if (t == NodeResult.Failure)
                    {
                        yield return NodeResult.Failure;
                        break;
                    }
                }
            }
            else
            {
                yield return NodeResult.Failure;
            }
        }


    }
}