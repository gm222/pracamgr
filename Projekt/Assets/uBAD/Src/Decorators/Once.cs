using System.Collections.Generic;

namespace BAD
{

    /// <summary>
    /// Runs child once then disables itself.
    /// </summary>
    public class Once : Decorator
    {

        public override IEnumerator<NodeResult> NodeTask()
        {
            if (ChildIsMissing())
            {
                yield return NodeResult.Failure;
                yield break;
            }

            IEnumerator<NodeResult> task = children[0].GetNodeTask();
            while (task.MoveNext())
            {
                if (task.Current != NodeResult.Continue)
                    enabled = false;
                yield return task.Current;
            }
        }


    }
}