using System.Collections.Generic;
using UnityEngine;

namespace BAD
{

    /// <summary>
    /// Only runs child if probability value is met.
    /// </summary>
    public class Chance : Decorator
    {
        public float probability = 0.5f;

        //protected override void ResolveArguments() {
        //    probability = GetArg<float>(0);
        //}

        public override void Apply(object[] arguments)
        {
            float tmp = (float)arguments[0];

            if (tmp > 1f) tmp = 1f;
            if (tmp < 0f) tmp = 0f;

            probability = tmp;
        }
        public override IEnumerator<NodeResult> NodeTask()
        {
            if (ChildIsMissing())
            {
                yield return NodeResult.Failure;
                yield break;
            }
            if (Random.value <= probability)
            {
                IEnumerator<NodeResult> task = children[0].GetNodeTask();
                while (task.MoveNext())
                {
                    yield return task.Current;
                }
            }
            else
            {
                yield return NodeResult.Failure;
            }
        }


    }
}