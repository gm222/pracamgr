using System.Collections.Generic;
using UnityEngine;

namespace BAD
{

    /// <summary>
    /// Fails if child does not succeed within a time limit.
    /// </summary>
    public class TimeLimit : Decorator
    {
        public float seconds = 1;

        //protected override void ResolveArguments() {
        //    seconds = GetArg<float>(0);
        //}

        public override void Apply(object[] arguments)
        {
            seconds = (float)arguments[0];
        }

        public override IEnumerator<NodeResult> NodeTask()
        {
            if (ChildIsMissing())
            {
                yield return NodeResult.Failure;
                yield break;
            }
            float start = Time.time;
            IEnumerator<NodeResult> task = children[0].GetNodeTask();
            while (task.MoveNext())
            {
                NodeResult t = task.Current;
                if ((Time.time - start) > seconds)
                {
                    children[0].Abort();
                    yield return NodeResult.Failure;
                    break;
                }
                if (t == NodeResult.Continue)
                {
                    yield return NodeResult.Continue;
                }
                else
                {
                    if (t == NodeResult.Failure)
                    {
                        yield return NodeResult.Failure;
                        break;
                    }
                    if (t == NodeResult.Success)
                    {
                        yield return NodeResult.Success;
                        break;
                    }
                }
            }
        }


    }
}