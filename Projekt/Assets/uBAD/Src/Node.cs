﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BAD
{
    [Serializable]
    public class Node : ScriptableObject
    {
        public BADReactor reactor;
        public bool enabled = true;
        public NodeResult? state = null;
        public bool running = false;
        public Branch parent = null;
        public object[] arguments;
        public Vector2 position = Vector2.zero;
        public int ID;
        public string nodeName = string.Empty;
        public int NodeType = -1;

        public virtual void Apply(object[] arguments)
        {
            this.arguments = arguments;
        }

        public virtual T GetArg<T>(int index)
        {
            object obj = arguments[index];
            if (obj is Symbol)
            {
                Symbol s = obj as Symbol;
                obj = reactor.blackboard.Get(s.name);
            }
            return (T)obj;
        }

        protected virtual void ResolveArguments()
        {
        }

        public Node()
        {
            nodeName = GetType().Name;
        }

        public IEnumerator<NodeResult> GetNodeTask()
        {
            ResolveArguments();
#if UNITY_EDITOR
            if (reactor.debug)
                return DebugNodeTask();
            else
                return NodeTask();
#else
            return NodeTask ();
#endif
        }

        private IEnumerator<NodeResult> DebugNodeTask()
        {
            IEnumerator<NodeResult> task = NodeTask();
            while (true)
            {
                Debug.Log(string.Format("{0} : {1} : {2} : ID: {3}", ToString(), task.Current, running, ID));
                if (task.MoveNext())
                {
                    running = true;
                    state = task.Current;
                    if (state != NodeResult.Continue)
                    {

                        yield return NodeResult.Continue;
                        running = false;
                        state = null;
                    }
                    yield return task.Current;
                }
                else
                {
                    throw new System.Exception(string.Format("Premature enumerator exit: {0}", task));
                }
            }
        }

        public virtual IEnumerator<NodeResult> NodeTask()
        {
            yield break;
        }

        public virtual void Abort()
        {
            state = null;
            running = false;
        }

        public override string ToString()
        {
            return string.Format("{0}", nodeName);
        }
    }
}