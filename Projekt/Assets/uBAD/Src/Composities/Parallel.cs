using System.Collections.Generic;

namespace BAD
{

    /// <summary>
    /// Run all nodes simultaneously, failing or succeeding based on the policy field.
    /// </summary>
    public class Parallel : Branch
    {
        public ParallelPolicy policy;

        public override void Apply(object[] args)
        {
            this.policy = (ParallelPolicy)args[0];
        }

        public override IEnumerator<NodeResult> NodeTask()
        {
            List<IEnumerator<NodeResult>> tasks = new List<IEnumerator<NodeResult>>();
            Dictionary<IEnumerator<NodeResult>, Node> childMap = new Dictionary<IEnumerator<NodeResult>, Node>();
            foreach (Node c in children)
            {
                if (!c.enabled)
                    continue;
                IEnumerator<NodeResult> t = c.GetNodeTask();
                childMap[t] = c;
                tasks.Add(t);
            }

            while (true)
            {
                if (tasks.Count == 0)
                {
                    if (policy == ParallelPolicy.SucceedIfOneSucceeds)
                    {
                        yield return NodeResult.Failure;
                    }
                    if (policy == ParallelPolicy.FailIfOneFails)
                    {
                        yield return NodeResult.Success;
                    }
                }
                List<int> toKill = new List<int>();
                for (int i = 0; i<tasks.Count; i++)
                {
                    IEnumerator<NodeResult> task = tasks[i];
                    if (task.MoveNext())
                    {
                        if (policy == ParallelPolicy.SucceedIfOneSucceeds)
                        {
                            if (task.Current != NodeResult.Continue)
                            {
                                if (task.Current == NodeResult.Success)
                                {
                                    foreach (IEnumerator<NodeResult> c in childMap.Keys)
                                    {
                                        if (c != task)
                                            childMap[c].Abort();
                                    }
                                    yield return NodeResult.Success;
                                }
                                else if (task.Current == NodeResult.Failure)
                                {
                                    toKill.Add(i);
                                }
                            }
                        }
                        if (policy == ParallelPolicy.FailIfOneFails)
                        {
                            if (task.Current != NodeResult.Continue)
                            {
                                if (task.Current == NodeResult.Failure)
                                {
                                    foreach (IEnumerator<NodeResult> c in childMap.Keys)
                                    {
                                        if (c != task)
                                            childMap[c].Abort();
                                    }
                                    yield return NodeResult.Failure;
                                }
                                else if (task.Current == NodeResult.Success)
                                    toKill.Add(i);
                            }
                        }
                        if (task.Current == NodeResult.Continue)
                        {
                        }
                    }
                    else
                    {
                        toKill.Add(i);
                    }
                }

                toKill.Reverse();
                foreach (int i in toKill)
                {
                    childMap.Remove(tasks[i]);
                    tasks.RemoveAt(i);
                }
                toKill.Clear();

                yield return NodeResult.Continue;
            }
        }
    }
}