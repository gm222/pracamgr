using System;
using System.Collections.Generic;

namespace BAD
{

    /// <summary>
    /// The root node which must sit at the top of the tree.
    /// </summary>
    /// 
    [Serializable]
    public class Root : Branch
    {
        public override IEnumerator<NodeResult> NodeTask()
        {
            Node[] children = this.children.ToArray();

            while (true)
            {
                bool childrenExist = false;
                for (int i = 0; i<children.Length; i++)
                {

                    Node child = children[i];
                    if (!child.enabled)
                        continue;
                    childrenExist = true;
                    IEnumerator<NodeResult> task = child.GetNodeTask();
                    while (task.MoveNext())
                    {
                        if (task.Current == NodeResult.Continue)
                            yield return NodeResult.Continue;
                        else
                            break;
                    }
                }
                if (!childrenExist)
                    yield return NodeResult.Continue;
            }
        }
    }
}