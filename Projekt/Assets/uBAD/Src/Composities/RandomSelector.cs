using System;
using System.Collections.Generic;

namespace BAD
{

    /// <summary>
    /// A selector which randomly shuffles it's children before executing.
    /// </summary>
    public class RandomSelector : Selector
    {

        public override IEnumerator<NodeResult> NodeTask()
        {
            Random rng = new System.Random();
            int n = children.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                Node value = children[k];
                children[k] = children[n];
                children[n] = value;
            }

            IEnumerator<NodeResult> task = base.NodeTask();
            while (task.MoveNext())
            {
                yield return task.Current;
            }
        }
    }
}