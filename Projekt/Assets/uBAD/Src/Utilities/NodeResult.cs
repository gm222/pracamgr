namespace BAD {

    public enum NodeResult {
        Failure,
        Success,
        Continue
    }
}