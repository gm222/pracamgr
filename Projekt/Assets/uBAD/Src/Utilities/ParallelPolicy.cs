namespace BAD {

    public enum ParallelPolicy {
        FailIfOneFails,
        SucceedIfOneSucceeds
    }
}