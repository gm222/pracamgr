namespace BAD {

    public enum MutationPolicy {
        MoveToTop,
        MoveToBottom
    }
}