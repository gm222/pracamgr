using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace BAD
{
    public class BADViewerWindow : EditorWindow
    {
        // static BADViewerWindow window;
        public BADReactor reactor;
        // Vector2 scrollPosition;
        Rect cursor;
        List<Connector> connectors = new List<Connector>();

        void DrawNode(Node node)
        {
            GUI.color = node.running ? Color.yellow : Color.white;
            if (node.state != null)
            {
                Rect icon = cursor;
                icon.x -= 14;
                icon.width = 14;
                Color color = GUI.color;
                GUI.color = node.state == NodeResult.Success ? Color.green : node.state == NodeResult.Continue ? Color.yellow : Color.red;
                GUI.Label(icon, "", EditorStyles.radioButton);
                GUI.color = color;

            }
            string text = string.Format("{0}", node);
            cursor.width = GUI.skin.button.CalcSize(new GUIContent(text)).x;
            GUI.Label(cursor, text, "button");
            GUILayout.Label("", GUILayout.Width(0));

        }

        void DrawDecoratorNode(Node node)
        {
            DrawNode(node);
        }

        void DrawBranchNode(Node node)
        {
            DrawNode(node);
        }

        void DrawGraph(Node node, Vector2 parentPosition)
        {
            float midY = Mathf.Lerp(cursor.yMax, cursor.yMin, 0.5f);
            float x = parentPosition.x;
            Vector2 A = new Vector2(cursor.xMin, midY);
            Vector2 B = new Vector2(x, midY);
            Vector2 C = parentPosition;
            if (node.running)
            {
                connectors.Add(new Connector(A, B, C));
            }
            else
            {
                Handles.color = Color.white;
                Handles.DrawPolyLine(A, B, C);
            }


            if (node is Decorator)
            {
                Decorator decorator = node as Decorator;
                DrawDecoratorNode(node);
                parentPosition = new Vector2(cursor.xMax, Mathf.Lerp(cursor.yMin, cursor.yMax, 0.5f));
                float indent = cursor.width + 16;
                cursor.x += indent;
                foreach (Node child in decorator.children)
                {
                    DrawGraph(child, parentPosition);
                }
                cursor.x -= indent;
            }
            else if (node is Branch)
            {
                Branch branch = node as Branch;
                DrawBranchNode(node);
                parentPosition = new Vector2(Mathf.Lerp(cursor.xMin, cursor.xMax, 0.25f), cursor.yMax);
                float indent = (cursor.width / 4) + 16;
                cursor.x += indent;
                cursor.y += cursor.height + 4;
                foreach (Node child in branch.children)
                {
                    DrawGraph(child, parentPosition);
                }
                cursor.x -= indent;
            }
            else
            {
                DrawNode(node);
                cursor.y += cursor.height + 4;
            }

        }

        void OnDrawGUI()
        {
            cursor = new Rect(20, 20, 160, 18);
            connectors.Clear();
            if (reactor != null)
            {
                foreach (Node node in reactor.runningGraphs)
                {
                    DrawGraph(node, new Vector2(20, 20));
                }
            }
            foreach (Connector c in connectors)
            {
                Handles.color = Color.green;
                Handles.DrawPolyLine(c.A, c.B, c.C);
            }
        }
        Vector2 _scrollView = Vector2.zero;
        void OnGUI()
        {

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            EditorGUILayout.ObjectField(reactor, typeof(BADReactor), true);
            GUILayout.EndHorizontal();
            _scrollView = GUI.BeginScrollView(new Rect(0, 0, position.width-20f, position.height-20f), _scrollView, new Rect(0, 0, cursor.width, cursor.y));
            GUILayout.BeginVertical();
            OnDrawGUI();
            GUILayout.EndVertical();
            GUI.EndScrollView();







        }

        void Update()
        {
            if (reactor != null && Application.isPlaying)
            {
                Repaint();
            }
        }

        class Connector
        {
            public Vector2 A, B, C;

            public Connector(Vector2 A, Vector2 B, Vector2 C)
            {
                this.A = A;
                this.B = B;
                this.C = C;
            }
        }

    }

}