using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace BAD
{
    [CustomEditor(typeof(BADReactor))]
    public class BADReactorEditor : Editor
    {
        BADReactor activeReactor;

        public void OnEnable()
        {
            activeReactor = target as BADReactor;
        }



        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            base.OnInspectorGUI();
            //EditorGUILayout.PropertyField(serializedObject.FindProperty("runningGraphs"));
            //EditorGUILayout.PropertyField(serializedObject.FindProperty("blackboard"));
            if (Application.isPlaying)
            {
                GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                BADReactor reactor = target as BADReactor;
                if (GUILayout.Button(reactor.pause ? "Resume" : "Pause", GUILayout.MinWidth(64)))
                {
                    reactor.pause = !reactor.pause;
                }
                if (GUILayout.Button("Step", GUILayout.MinWidth(64)))
                {
                    reactor.step = true;
                }
                if (GUILayout.Button("Fast", GUILayout.MinWidth(64)))
                {
                    reactor.fastforward = true;
                }
                if (GUILayout.Button("Watch", GUILayout.MinWidth(64)))
                {
                    BADViewerWindow window = (BADViewerWindow)EditorWindow.GetWindow(typeof(BADViewerWindow));
                    if (activeReactor != null)
                        activeReactor.debug = false;
                    reactor.debug = true;
                    activeReactor = reactor;
                    window.reactor = reactor;
                    window.Show();
                }
                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();

                foreach (KeyValuePair<string, float> i in reactor.blackboard.Items)
                {
                    GUILayout.BeginHorizontal();
                    EditorGUILayout.PrefixLabel(i.Key);
                    GUILayout.Label(i.Value.ToString(), "box", GUILayout.Width(128));
                    GUILayout.EndHorizontal();
                }
            }
        }
    }
}