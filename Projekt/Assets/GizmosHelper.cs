﻿using UnityEngine;

public class GizmosHelper : MonoBehaviour {
    [SerializeField]
    private Transform _bulletPoint;

    [SerializeField]
    private Transform _rotator;

    [SerializeField]
    private Transform _armPoint;

    [SerializeField, Range(0f, 200f)]
    private float _rayDistance = 50f;




    public void OnDrawGizmos() {

        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, transform.forward * _rayDistance);

        if(_bulletPoint != null) {
            Gizmos.color = Color.blue;
            Gizmos.DrawRay(_bulletPoint.position, _bulletPoint.forward * _rayDistance);
        }

        if(_rotator != null) {
            Gizmos.color = Color.green;
            Gizmos.DrawRay(_rotator.position, _rotator.forward * _rayDistance);
        }

        if(_armPoint != null) {
            Gizmos.color = Color.magenta;
            Gizmos.DrawRay(_armPoint.position, _armPoint.forward * _rayDistance);
        }

        //Gizmos.color = Color.magenta;
        // Gizmos.DrawRay(transform.position, transform.forward * _rayDistance);
    }
}
