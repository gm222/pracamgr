﻿using UnityEngine;

public class SeeAngles : MonoBehaviour
{
    public void Update()
    {
        Vector3 posToCheck = transform.position;
        Vector3 forward = transform.forward;
        Debug.DrawRay(posToCheck, forward * 50f);
    }

    [SerializeField, Range(0f, 200f)]
    private float _rayDistance = 50f;

    [SerializeField, Range(0f, 360f)]
    private float _angle = 45f;

    [SerializeField, Range(1, 100)]
    int _stepDivider = 10;

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        Vector3 rayPosition = transform.position;
        Vector3 leftRayRotation = Quaternion.AngleAxis(-_angle, transform.up) * transform.forward;
        Vector3 rightRayRotation = Quaternion.AngleAxis(_angle, transform.up) * transform.forward;

        ////Constructing rays
        //Ray rayCenter = new Ray(rayPosition, transform.forward * _rayDistance);
        //Ray rayLeft = new Ray(rayPosition, leftRayRotation * _rayDistance);
        //Ray rayRight = new Ray(rayPosition, rightRayRotation *_rayDistance);

        //Gizmos.color = Color.red;
        //Gizmos.DrawRay(rayPosition, transform.forward * _rayDistance);
        //Gizmos.color = Color.blue;
        //Gizmos.DrawRay(rayPosition, leftRayRotation * _rayDistance);
        //Gizmos.color = Color.green;
        //Gizmos.DrawRay(rayPosition, rightRayRotation * _rayDistance);

        Gizmos.color = Color.magenta;

        _stepDivider = (int)Mathf.Clamp((int)(_angle+1) / 2, 15, _rayDistance * 2);

        if (_stepDivider == 0) _stepDivider = 1;
        int step = (int)_angle*2/_stepDivider;
        for (int i = -(int)_angle; i <= _angle; i+= step)
        {
            Vector3 angled = Quaternion.AngleAxis(i, transform.up) * transform.forward;
            Gizmos.DrawRay(rayPosition, angled *_rayDistance);
            RaycastHit hit;
            if (Physics.Raycast(rayPosition, angled, out hit, _rayDistance))
            {
                if (hit.transform.tag.Equals("Enemy"))
                {
                    Debug.Log("Enemy Detect");
                    break;
                }
                else Debug.Log("clear");
            }
        }




    }
}
