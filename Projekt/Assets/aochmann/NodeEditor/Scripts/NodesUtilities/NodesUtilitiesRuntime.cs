﻿using Assets.Test.Scripts.Data;
using System;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Assets.aochmann.NodeEditor.Scripts.NodesUtilities
{
    public static class NodesUtilitiesRuntime
    {
        private const string DATABASE_PATH = @"Assets/aochmann/NodeEditor/Resources/Database";
        public static void LoadGraph(ref NodeGraph currentGraph)
        {

            string path = Application.dataPath + @"/aochmann/NodeEditor/Resources/Database/" + currentGraph.GraphName + ".asset";
            if (string.IsNullOrEmpty(path))
                return;
            int appPathLen = Application.dataPath.Length;
            string finalPath = path.Substring(appPathLen - 6);
            AssetDatabase.Refresh();

            NodeGraph currentGraphTMP = AssetDatabase.LoadAssetAtPath<NodeGraph>(finalPath);

            currentGraph = currentGraphTMP;

            if (currentGraph == null)
            {
                currentGraph = CreatNodeGraph();
            }

        }

        public static NodeGraph CreatNodeGraph()
        {
            string graphName = "Graph" + DateTime.Now.ToString("HHmmssfff");

            NodeGraph currentGraph = new NodeGraph(graphName);
            if (currentGraph != null)
            {
                currentGraph.Initialize();
                AssetDatabase.CreateAsset(currentGraph, DATABASE_PATH + "/" + graphName + ".asset");
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();

                return currentGraph;
            }

            return null;
        }


        public static bool LoadGraph(ref EnemyLogicContainer container)
        {
            bool result = false;

            if (container != null)
            {
                if (container.FileName.Length == 0)
                {
                    container.FileName = "Enemy" + DateTime.Now.ToString("HHmmssfff");
                }

                string path = string.Format(@"{0}\{1}", DATABASE_PATH, container.FileName);
                NodeGraph[] tmp = LoadAllAssetsAtPath(path);
                if (tmp == null || tmp.Length == 0)
                {
                    container.EnemyLogics.Clear();
                    result = CreatEnemyStructure(container);
                }
                else
                {
                    container.EnemyLogics = new System.Collections.Generic.List<NodeGraph>(tmp);
                    result = true;
                }
            }



            return result;
        }

        private static NodeGraph[] LoadAllAssetsAtPath(string path)
        {
            if (path.EndsWith("/"))
            {
                path = path.TrimEnd('/');
            }
            string[] GUIDs = AssetDatabase.FindAssets("", new string[] { path }).Distinct<string>().ToArray();
            NodeGraph[] objectList = new NodeGraph[GUIDs.Length];
            for (int index = 0; index < GUIDs.Length; index++)
            {
                string guid = GUIDs[index];
                string assetPath = AssetDatabase.GUIDToAssetPath(guid);
                NodeGraph asset = AssetDatabase.LoadAssetAtPath<NodeGraph>(assetPath);
                objectList[index] = asset;
            }

            return objectList;
        }

        private static bool CreatEnemyStructure(EnemyLogicContainer container)
        {
            AssetDatabase.CreateFolder(DATABASE_PATH, container.FileName);
            //AssetDatabase.CreateAsset(container.EnemyGraph, string.Format(@"{0}\{1}.asset", DATABASE_PATH, container.EnemyGraph.FileName));
            AddGraphToLogic(container);
            Save();
            return true;
        }

        public static void AddGraphToLogic(EnemyLogicContainer logic)
        {
            NodeGraph graph = new NodeGraph(DateTime.Now.ToString("HHmmssfff"));
            logic.EnemyLogics.Add(graph);
            logic.SelectedIndex = logic.EnemyLogics.Count - 1;
            AssetDatabase.CreateAsset(graph, string.Format(@"{0}\{1}\{2}{3}.asset", DATABASE_PATH, logic.FileName, logic.FileName, graph.GraphName));
            AssetDatabase.SaveAssets();
        }

        public static void Save()
        {
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
        }
    }
}
