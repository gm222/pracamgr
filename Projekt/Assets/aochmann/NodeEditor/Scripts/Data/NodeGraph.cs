﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.aochmann.NodeEditor.Scripts.NodesGUI.Nodes;

#if UNITY_EDITOR

using UnityEditor;

#endif

using UnityEngine;

namespace Assets.Test.Scripts.Data
{

    [Serializable]
    public class NodeGraph : ScriptableObject
    {

        [SerializeField]
        private string _graphName = "New Graph";
        [SerializeField]
        private List<NodeBase> _nodes;
        [SerializeField]
        private NodeBase _selectedNode;
        [SerializeField]
        private bool _wantsConnection;
        [SerializeField]
        private NodeBase _connectionNode;
        [SerializeField]
        private bool _showProperties;
        [SerializeField]
        private NodeBase _startupNode;


        public string GraphName
        {
            get { return _graphName; }
        }

        public List<NodeBase> Nodes
        {
            get { return _nodes; }
            set { _nodes=value; }
        }

        public NodeBase SelectedNode
        {
            get { return _selectedNode; }
            set { _selectedNode=value; }
        }

        public bool WantsConnection
        {
            get { return _wantsConnection; }
            set { _wantsConnection=value; }
        }

        public NodeBase ConnectionNode
        {
            get { return _connectionNode; }
            set { _connectionNode=value; }
        }

        public bool ShowProperties
        {
            get { return _showProperties; }
            set { _showProperties=value; }
        }

        public NodeBase StartupNode
        {
            get { return _startupNode; }
        }


        public NodeGraph() : this("")
        {

        }

        public NodeGraph(string graphName)
        {
            _graphName = graphName;
        }

        private void OnEnable()
        {
            if (Nodes == null)
            {
                Nodes = new List<NodeBase>();
            }
        }

        public void Initialize()
        {
            if (Nodes.Any())
            {
                foreach (NodeBase node in Nodes)
                {
                    node.Initialize();
                }
            }
        }

        public void UpdateGraph(Event e)
        {
            if (Nodes.Any())
            {
                foreach (NodeBase node in Nodes)
                {
                    node.UpdateNode(e);
                    node.Id = -1;
                }

                if (_startupNode != null)
                {
                    int tmpID = -1;
                    SetID(_startupNode, tmpID);
                }
            }
        }

        private void SetID(NodeBase node, int id)
        {
            node.Id = ++id;

            Queue<NodeBase> Q = new Queue<NodeBase>();
            Q.Enqueue(node);

            while (Q.Count > 0)
            {
                NodeBase n = Q.Dequeue();
                foreach (NodeOutput output in n.Outputs)
                {
                    foreach (NodeBase child in output.ChildNodes)
                    {
                        child.Id = ++id;
                        Q.Enqueue(child);
                    }
                }
            }


        }

#if UNITY_EDITOR

        public void UpdateGraphGUI(Event e, Rect viewRect)
        {
            if (Nodes.Any())
            {
                HandleEvents(e, viewRect);

                foreach (NodeBase window in Nodes)
                    window.UpdateNodeGUI(e, viewRect);
            }

            if (WantsConnection)
                if (ConnectionNode != null)
                    DrawConnectionToMouse(e.mousePosition);
        }

        private void HandleEvents(Event e, Rect viewRect)
        {
            if (viewRect.Contains(e.mousePosition))
            {
                if (e.button == 0)
                {
                    if (e.type == EventType.MouseDown)
                    {
                        DeselectAllNodes();

                        ShowProperties = false;
                        bool setNode = false;
                        SelectedNode   = null;

                        foreach (NodeBase node in Nodes)
                        {
                            if (node.NodeBoundry.Contains(e.mousePosition))
                            {
                                SelectedNode = node;
                                setNode = true;
                                node.IsSelected = true;
                                break;
                            }
                        }
                        if (!setNode)
                        {
                            DeselectAllNodes();

                            if (WantsConnection)
                            {
                                WantsConnection = false;

                                if (ConnectionNode != null)
                                {
                                    if (ConnectionNode.ActiveOutput.ChildNodes.Any())
                                        ConnectionNode.ActiveOutput.IsOccupied = false;

                                    ConnectionNode.ActiveOutput = null;
                                    ConnectionNode.IsSelected = true;
                                    SelectedNode = ConnectionNode;
                                    ConnectionNode = null;
                                }
                            }
                        }
                    }
                }
            }
            if (e.keyCode == KeyCode.Delete && SelectedNode != null)
            {
                DeleteNode(SelectedNode);
                SelectedNode = null;
            }
        }

        private void DrawConnectionToMouse(Vector2 position)
        {
            Vector3 start = ConnectionNode.ActiveOutput.Possition;
            Vector3 end = position;

            Vector2 startTangent;
            Vector2 endTangent;

            Color color = Color.red;
            float offset = Math.Abs(start.x - end.x) / 1.75f;

            if (position.x < start.x)
            {
                startTangent = new Vector2(start.x - offset, start.y);
                endTangent   = new Vector2(end.x + offset, end.y);
            }
            else
            {
                startTangent = new Vector2(start.x + offset, start.y);
                endTangent   = new Vector2(end.x - offset, end.y);
            }

            Handles.BeginGUI();
            {
                Handles.color = Color.white;
                Handles.DrawBezier(start, end, startTangent, endTangent, color, null, 2);
            }
            Handles.EndGUI();
        }

        public void ChangeStartupNode(RootGUI node)
        {
            _startupNode = node;
        }

        public void DeleteNode(NodeBase node)
        {
            if (_startupNode == node)
                _startupNode = null;

            if (node.Outputs != null && node.Outputs.Any())
                foreach (NodeOutput output in node.Outputs)
                    output.BreakOutputConnection();

            if (node.Inputs != null && node.Inputs.Any())
                foreach (NodeInput input in node.Inputs)
                    input.BreakInputConnection(node);

            DestroyImmediate(node, true);
            Nodes.Remove(node);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        public void DeselectAllNodes()
        {
            foreach (NodeBase node in Nodes)
                node.IsSelected = false;

            SelectedNode = null;
        }

#endif
    }
}