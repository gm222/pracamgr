﻿using System;
using System.Linq;
using UnityEngine;

namespace Assets.Test.Scripts.Data {

    [Serializable]
    public class NodeInput {

        #region Serialized Fields

        [SerializeField]
        private bool _isOccupied = false;

        [SerializeField]
        private NodeBase _parentNode;

        [SerializeField]
        private int _connectedOutputID = -1;

        #endregion Serialized Fields


        public Vector2 Possition;

        #region Properties

        public bool IsOccupied {
            get { return _isOccupied; }
            set { _isOccupied=value; }
        }

        public NodeBase ParentNode {
            get { return _parentNode; }
            set { _parentNode=value; }
        }

        public int ConnectedOutputID {
            get { return _connectedOutputID; }
            set { _connectedOutputID=value; }
        }

        #endregion Properties

        public void Connect(NodeBase nodeinfo) {
            if(ConnectedOutputID != -1) {
                ParentNode.Outputs[ConnectedOutputID].ChildNodes.Remove(nodeinfo);
                ParentNode.Outputs[ConnectedOutputID].IsOccupied = false;

                if(ParentNode.Outputs[ConnectedOutputID].ChildNodes.Count == 0 || !ParentNode.Outputs[ConnectedOutputID].CanHaveMoreChilds) {
                    ParentNode.Outputs[ConnectedOutputID].ConnectedInputID = -1;
                }
            }

            ParentNode = nodeinfo.ParentGraph.ConnectionNode;
            IsOccupied = ParentNode != null;

            NodeOutput outputNode = ParentNode.ActiveOutput;

            if(!outputNode.CanHaveMoreChilds) {
                outputNode.BreakOutputConnection();
                outputNode.ChildNodes.Add(nodeinfo);
            } else
                outputNode.ChildNodes.Add(nodeinfo);

            ConnectedOutputID                    = ParentNode.Outputs.IndexOf(outputNode);
            outputNode.ConnectedInputID          = 1;
            outputNode.IsOccupied                = true;
            nodeinfo.ParentGraph.WantsConnection = false;
            nodeinfo.ParentGraph.ConnectionNode  = null;
        }

        public void BreakInputConnection(NodeBase childNode) {
            if(ConnectedOutputID != -1) {
                NodeOutput outputNode = ParentNode.Outputs[ConnectedOutputID];
                outputNode.IsOccupied = false;

                outputNode.ChildNodes.RemoveAt(outputNode.ChildNodes.IndexOf(childNode));

                if(!outputNode.ChildNodes.Any())
                    outputNode.ConnectedInputID = -1;
            }

            ParentNode = null;
            IsOccupied = false;
            ConnectedOutputID = -1;
        }
    }
}