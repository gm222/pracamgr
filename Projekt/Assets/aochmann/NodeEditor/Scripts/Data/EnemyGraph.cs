﻿using System;
using UnityEngine;

namespace Assets.aochmann.NodeEditor.Scripts.Data {
    [Serializable]
    public class EnemyGraph : ScriptableObject {
        [SerializeField]
        private string _fileName;

        public string FileName {
            get {
                return _fileName;
            }

            set {
                _fileName=value;
            }
        }

        public EnemyGraph() : this("") {

        }

        public EnemyGraph(string graphName) {
            _fileName = graphName;
        }
    }
}
