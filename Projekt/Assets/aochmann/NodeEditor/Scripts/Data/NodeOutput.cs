﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Test.Scripts.Data {

    [Serializable]
    public class NodeOutput {
        public bool IsOccupied = false;
        public bool CanHaveMoreChilds = true;
        public List<NodeBase> ChildNodes = new List<NodeBase>();
        public int ConnectedInputID = -1;
        public Vector2 Possition;


        public void BreakOutputConnection() {

            if(ChildNodes.Any()) {

                for(int i = 0; i < ChildNodes.Count; i++) {
                    NodeBase item = ChildNodes[i];
                    NodeInput connectedInput = item.Inputs[0];

                    connectedInput.ParentNode = null;
                    connectedInput.IsOccupied = false;
                    connectedInput.ConnectedOutputID = -1;
                }
            }

            IsOccupied = false;
            ConnectedInputID = -1;
            ChildNodes.Clear();
        }
    }
}