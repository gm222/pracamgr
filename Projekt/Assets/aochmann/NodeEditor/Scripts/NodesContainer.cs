﻿using Assets.aochmann.NodeEditor.Scripts.NodesGUI;
using Assets.aochmann.NodeEditor.Scripts.NodesUtilities;
using Assets.Test.Scripts.Data;
using BAD;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.aochmann.NodeEditor.Scripts
{
    [Serializable, RequireComponent(typeof(BADReactor))]
    public class NodesContainer : MonoBehaviour
    {
        [SerializeField]
        public NodeGraph GUIGraph;

        private string[] nodesTypes;

        public void Initialize()
        {
            if (GUIGraph == null) return;

            NodesUtilitiesRuntime.LoadGraph(ref GUIGraph);

            if (GUIGraph == null || GUIGraph.StartupNode == null) return;

            BADReactor reactor = GetComponent<BADReactor>();
            if (reactor == null)
            {
                Debug.LogWarning("Missing BadReactor");
                return;
            }

            if (GUIGraph.StartupNode != null)
            {
                _checkCondition = true;
                nodesTypes = Enum.GetNames(typeof(NodeType));
                Root root = new Root();
                root.NodeType = Array.IndexOf(nodesTypes, NodeType.Root.ToString());
                Parser2(root, GUIGraph.StartupNode, reactor);
                reactor.runningGraphs.Clear();

                if (_checkCondition)
                    reactor.runningGraphs.Add(root);

                if (reactor.runningGraphs.Any())
                {
                    int nodesCount = 0;
                    foreach (Node node in reactor.runningGraphs)
                    {
                        SetID(node, -1, out nodesCount);
                        //int id = 0;
                        //node.ID = id++;
                        //Queue<Node> Q = new Queue<Node>();
                        //Q.Enqueue(node);
                        //while(Q.Count > 0) {
                        //    var n = Q.Dequeue();
                        //    var branch = n as Branch;
                        //    if(branch != null) {
                        //        foreach(var child in branch.children) {
                        //            child.ID = ++id;
                        //            Q.Enqueue(child);
                        //        }
                        //    }
                        //}
                    }

                }
            }

        }

        public void Start()
        {
            //if(GUIGraph == null) return;

            //NodesUtilitiesRuntime.LoadGraph(ref GUIGraph);

            //if(GUIGraph == null || GUIGraph.StartupNode == null) return;

            //BADReactor reactor = GetComponent<BADReactor>();
            //if(reactor == null) {
            //    Debug.LogWarning("Missing BadReactor");
            //    return;
            //}

            //if(GUIGraph.StartupNode != null) {
            //    _checkCondition = true;
            //    nodesTypes = Enum.GetNames(typeof(NodeType));
            //    Root root = new Root();
            //    Parser2(root, GUIGraph.StartupNode, reactor);

            //    if(_checkCondition)
            //        reactor.runningGraphs.Add(root);

            //    if(reactor.runningGraphs.Any()) {
            //        int nodesCount = 0;
            //        foreach(var node in reactor.runningGraphs) {
            //            SetID(node, -1, out nodesCount);
            //            //int id = 0;
            //            //node.ID = id++;
            //            //Queue<Node> Q = new Queue<Node>();
            //            //Q.Enqueue(node);
            //            //while(Q.Count > 0) {
            //            //    var n = Q.Dequeue();
            //            //    var branch = n as Branch;
            //            //    if(branch != null) {
            //            //        foreach(var child in branch.children) {
            //            //            child.ID = ++id;
            //            //            Q.Enqueue(child);
            //            //        }
            //            //    }
            //            //}
            //        }
            //        int[] nodeInputs = new int[nodesCount];
            //        SetNodeTypesArray(nodeInputs, root);
            //    }
            //}


        }

        public void SetID(Node node, int id, out int nodesCount)
        {
            node.ID = ++id;
            nodesCount = 1;
            Queue<Node> Q = new Queue<Node>();
            Q.Enqueue(node);

            while (Q.Count > 0)
            {
                nodesCount++;
                Node n = Q.Dequeue();
                Branch branch = n as Branch;

                if (branch != null)
                {
                    foreach (Node child in branch.children)
                    {
                        child.ID = ++id;
                        Q.Enqueue(child);
                    }
                }
            }
        }



        private bool _checkCondition = true;

        private void Parser2(Node instance, NodeBase instanceBase, BADReactor reactor)
        {

            Queue<Node> Q = new Queue<Node>();
            Queue<NodeBase> Q2 = new Queue<NodeBase>();
            Q.Enqueue(instance);
            Q2.Enqueue(instanceBase);
            List<object> arguments = new List<object>();
            while (Q2.Count > 0)
            {
                Node tmpInstance = Q.Dequeue();
                NodeBase tmpBase = Q2.Dequeue();
                Branch branch = tmpInstance as Branch;

                tmpInstance.reactor = reactor;

                if (branch != null)
                    foreach (NodeOutput outputsSlots in tmpBase.Outputs)
                        foreach (NodeBase outputNode in outputsSlots.ChildNodes)
                        {
                            Node instanceTMP = ScriptableObject.CreateInstance(outputNode.NodeType.ToString()) as Node;
                            if (instanceTMP != null)
                            {
                                instanceTMP.ID = outputNode.Id;
                                int indexType = Array.IndexOf(nodesTypes, outputNode.NodeType.ToString());
                                instanceTMP.NodeType = indexType;
                                branch.Add(instanceTMP);
                                Q.Enqueue(instanceTMP);
                                Q2.Enqueue(outputNode);
                            }
                        }

                object tmpArguments = tmpBase.GetArguments();
                if (tmpArguments != null)
                    if (tmpArguments is string[])
                    {
                        ComponentMethodLookup c = new ComponentMethodLookup(tmpArguments as string[]);
                        c.Resolve(gameObject);
                        arguments.Add(c);
                    }
                    else
                        arguments.Add(tmpArguments);

                if (arguments.Count > 0)
                    tmpInstance.Apply(arguments.ToArray());
                arguments.Clear();
            }
        }

        //private void Parser(Node instance, NodeBase nodeBase, BADReactor reactor) {
        //    instance.reactor = reactor;

        //    foreach(var outputsSlots in nodeBase.Outputs) {
        //        Branch branch = instance as Branch;
        //        if(branch != null) {
        //            foreach(var outputNode in outputsSlots.ChildNodes) {
        //                instance = ScriptableObject.CreateInstance(outputNode.NodeType.ToString()) as Node;

        //                if(instance != null)
        //                    branch.Add(instance);

        //                Parser(instance, outputNode, reactor);

        //            }
        //        }

        //    }

        //    List<object> arguments = new List<object>();

        //    object tmpArguments = nodeBase.GetArguments();
        //    if(tmpArguments != null)
        //        if(tmpArguments is string[]) {
        //            var c = new ComponentMethodLookup(tmpArguments as string[]);
        //            c.Resolve(gameObject);
        //            arguments.Add(c);
        //        } else
        //            arguments.Add(tmpArguments);

        //    if(arguments.Count > 0)
        //        instance.Apply(arguments.ToArray());


        //}
    }
}
