﻿using Assets.aochmann.NodeEditor.Scripts.NodesUtilities;
using Assets.Test.Scripts.Data;
using BAD;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.aochmann.NodeEditor.Scripts
{
    [RequireComponent(typeof(BAD.BADReactor)), Serializable]
    public class EnemyLogicContainer : MonoBehaviour
    {
        [SerializeField]
        private List<NodeGraph> _enemyLogics = new List<NodeGraph>();

        [SerializeField]
        private int _selectedIndex = -1;



        public List<NodeGraph> EnemyLogics
        {
            get
            {
                return _enemyLogics;
            }

            set
            {
                _enemyLogics=value;
            }
        }

        [SerializeField]
        private string _fileName = string.Empty;

        public string FileName
        {
            get
            {
                return _fileName;
            }

            set
            {
                _fileName=value;
            }
        }


        public int SelectedIndex { get { return _selectedIndex; } set { _selectedIndex = value; } }

        private void Awake()
        {
            EnemyLogicContainer obj = this;
            NodesUtilitiesRuntime.LoadGraph(ref obj);
        }


        public Node Parser(Node instance, NodeBase instanceBase, BADReactor reactor)
        {

            Queue<Node> Q = new Queue<Node>();
            Queue<NodeBase> Q2 = new Queue<NodeBase>();
            Q.Enqueue(instance);
            Q2.Enqueue(instanceBase);
            List<object> arguments = new List<object>();
            while (Q2.Count > 0)
            {
                Node tmpInstance = Q.Dequeue();
                NodeBase tmpBase = Q2.Dequeue();
                Branch branch = tmpInstance as Branch;

                tmpInstance.reactor = reactor;

                if (branch != null)
                    foreach (NodeOutput outputsSlots in tmpBase.Outputs)
                        foreach (NodeBase outputNode in outputsSlots.ChildNodes)
                        {
                            Node instanceTMP = ScriptableObject.CreateInstance(outputNode.NodeType.ToString()) as Node;
                            if (instanceTMP != null)
                            {
                                instanceTMP.ID = outputNode.Id;
                                branch.Add(instanceTMP);
                                Q.Enqueue(instanceTMP);
                                Q2.Enqueue(outputNode);
                            }
                        }

                object tmpArguments = tmpBase.GetArguments();
                if (tmpArguments != null)
                    if (tmpArguments is string[])
                    {
                        ComponentMethodLookup c = new ComponentMethodLookup(tmpArguments as string[]);
                        c.Resolve(gameObject);
                        arguments.Add(c);
                    }
                    else
                        arguments.Add(tmpArguments);

                if (arguments.Count > 0)
                    tmpInstance.Apply(arguments.ToArray());
                arguments.Clear();
            }

            return instance;
        }

    }
}
