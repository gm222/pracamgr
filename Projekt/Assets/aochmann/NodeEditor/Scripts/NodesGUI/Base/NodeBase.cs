﻿using Assets.aochmann.NodeEditor.Scripts.NodesGUI;
using System;
using System.Collections.Generic;
using System.Linq;

#if UNITY_EDITOR

using UnityEditor;

#endif

using UnityEngine;

namespace Assets.Test.Scripts.Data
{

    [Serializable]
    public class NodeBase : ScriptableObject
    {

        #region Serialized Fields
        [SerializeField]
        private string _name;

        [SerializeField]
        public Rect NodeRect;

        [SerializeField]
        private NodeGraph _parentGraph;

        [SerializeField]
        private NodeType _nodeType;

        [SerializeField]
        private bool _isSelected = false;

        [SerializeField]
        private NodeOutput _activeOutput;

        [SerializeField]
        private NodeInput _activeInput;

        [SerializeField]
        private List<NodeOutput> _outputs = new List<NodeOutput>();

        [SerializeField]
        private List<NodeInput> _inputs = new List<NodeInput>();

        [SerializeField]
        private NodeType _baseType = NodeType.Null;

        [SerializeField]
        private bool _nodeOption = false;

        [SerializeField]
        private Vector2 _largeOpitonRect;


        [SerializeField]
        private bool _haveCustomProperties = false;
        #endregion

        #region Properties
        public NodeType NodeType
        {
            get { return _nodeType; }
            set { _nodeType=value; }
        }

        public List<NodeInput> Inputs
        {
            get { return _inputs; }
            set { _inputs=value; }
        }

        public List<NodeOutput> Outputs
        {
            get { return _outputs; }
            set { _outputs=value; }
        }

        public Rect NodeBoundry
        {
            get { return _nodeBoundry; }
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected=value; }
        }

        public NodeOutput ActiveOutput
        {
            get { return _activeOutput; }
            set { _activeOutput=value; }
        }

        public NodeInput ActiveInput
        {
            get { return _activeInput; }
            set { _activeInput=value; }
        }

        public NodeGraph ParentGraph
        {
            get { return _parentGraph; }
            set { _parentGraph=value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name=value; }
        }

        public int Id
        {
            get { return _id; }
            set { _id=value; }
        }
        #endregion

        #region Non-Serialized Fields
        private object _connectionButton;
        private Rect _nodeBoundry;
        private int _id = 0;
        private Vector2 _dragPositon = Vector2.zero;
        private readonly Vector2 _smallOpitonRect = new Vector2(180f, 100f);


        #endregion

        #region Constructors

        public NodeBase()
        {
            Inputs.Add(new NodeInput());
        }

        public NodeBase(string title) : this()
        {
            Name = title;
        }


        public NodeBase(string title, Rect rect) : this(title)
        {
            NodeRect = rect;
        }



        public virtual void Initialize()
        {
            NodeRect = new Rect(0f, 0f, _smallOpitonRect.x, _smallOpitonRect.y);
            _largeOpitonRect = new Vector2(285f, 120f);

#if UNITY_EDITOR

            hideFlags = HideFlags.DontUnloadUnusedAsset;

#endif
        }

        #endregion Constructors

        #region Not used
        public virtual void UpdateNode(Event e)
        {
        }



        #endregion

#if UNITY_EDITOR

        #region Public Methods

        public void SetDragPosition(Vector2 dragPosition)
        {
            _dragPositon = dragPosition;
        }

        public void SetBaseType(Type t)
        {
            if (t == typeof(BranchGUI))
                _baseType = NodeType.Branch;
        }

        public void SetLargeRect(Vector2 large)
        {
            _largeOpitonRect = large;
        }

        public void Collapse()
        {
            _nodeOption = false;
        }

        public void ExpandAll()
        {
            _nodeOption = true;
        }

        public void ExpandLeafsNodes()
        {
            if (GetType().BaseType == typeof(LeafGUI))
                _nodeOption = true;
        }

        public void SetPropertiesShow()
        {
            _haveCustomProperties = true;
        }

        Vector2 _scrollView = Vector2.zero;
        public virtual void UpdateNodeGUI(Event e, Rect viewRect)
        {
            CheckSelect(e);

            if (_haveCustomProperties)
            {
                if (_nodeOption == true)
                {
                    NodeRect.width  = _largeOpitonRect.x;
                    NodeRect.height = _largeOpitonRect.y;
                }
                else
                {
                    NodeRect.width  = _smallOpitonRect.x;
                    NodeRect.height = _smallOpitonRect.y;
                }
            }

            GUIStyle centeredTitleStyle = new GUIStyle();

            centeredTitleStyle.fontSize = 16;
            if (IsSelected)
                centeredTitleStyle.fontStyle = FontStyle.Bold;
            else
                centeredTitleStyle.fontStyle = FontStyle.Normal;
            var tmpRect = NodeRect;
            tmpRect.position += _dragPositon;

            GUI.Box(tmpRect, "", GUI.skin.window);
            Rect t = new Rect(tmpRect);
            t.x += 1f;
            t.width -= 2f;
            t.height -= 2f;
            t.y += 1f;
            GUILayout.BeginArea(t);
            {
                GUILayout.BeginVertical();
                {
                    GUILayout.BeginHorizontal(EditorStyles.toolbar);
                    {
                        GUILayout.FlexibleSpace();
                        GUILayout.Label(Name, centeredTitleStyle);
                        GUILayout.FlexibleSpace();
                    }
                    GUILayout.EndHorizontal();

                    if (_haveCustomProperties)
                    {
                        GUILayout.BeginHorizontal(EditorStyles.toolbar);
                        {
                            GUILayout.FlexibleSpace();
                            _nodeOption = GUILayout.Toggle(_nodeOption, !_nodeOption ? "Show Properties" : "Hide Properties", EditorStyles.toolbarButton);
                            GUILayout.FlexibleSpace();
                        }
                        GUILayout.EndHorizontal();
                        GUILayout.Space(2f);
                    }

                    _scrollView = GUILayout.BeginScrollView(_scrollView);
                    {
                        if (_haveCustomProperties && _nodeOption)
                        {
                            GUILayout.BeginVertical();
                            {
                                DrawNodeProperties();
                            }
                            GUILayout.EndVertical();
                            GUILayout.Space(5f);
                        }

                        bool parentNodeTest = this.Inputs[0] != null && Inputs[0].ParentNode != null;
                        int childID = parentNodeTest && Inputs[0].ParentNode.Outputs != null && Inputs[0].ParentNode.Outputs.Count != 0 ? Inputs[0].ParentNode.Outputs[0].ChildNodes.IndexOf(this) : -1;
                        ShowPriorityInfosProperties();
                        GUILayout.BeginHorizontal();
                        {
                            GUILayout.Label("Parrent Node:", GUILayout.Width(96f));
                            GUILayout.Label(parentNodeTest ? Inputs[0].ParentNode.Name : "Not set");
                        }
                        GUILayout.EndHorizontal();

                        if (parentNodeTest)
                        {
                            GUILayout.BeginHorizontal();
                            {
                                GUILayout.Label("Child ID:", GUILayout.Width(96f));
                                GUILayout.Label(childID == -1 ? "Not set" : childID.ToString());
                            }
                            GUILayout.EndHorizontal();
                        }

                        if (parentNodeTest && Inputs[0].ParentNode.Outputs[0].ChildNodes.Count > 1)
                        {
                            GUILayout.BeginHorizontal();
                            {
                                int maxChilds = Inputs[0].ParentNode.Outputs[0].ChildNodes.Count -1;
                                GUILayout.Label("Set child ID:", GUILayout.Width(96f));
                                int tmpChildID = (int)GUILayout.HorizontalSlider(childID, 0, maxChilds);
                                if (tmpChildID != childID)
                                {
                                    Inputs[0].ParentNode.Outputs[0].ChildNodes.Remove(this);
                                    Inputs[0].ParentNode.Outputs[0].ChildNodes.Insert(tmpChildID, this);
                                }
                            }
                            GUILayout.EndHorizontal();
                        }

                        if (_baseType == NodeType.Branch)
                        {
                            GUILayout.BeginHorizontal();
                            {
                                GUILayout.Label("Children Count:", GUILayout.Width(96f));
                                GUILayout.Label(Outputs[0].ChildNodes.Count().ToString());
                            }
                            GUILayout.EndHorizontal();
                        }


                    }
                    GUILayout.EndScrollView();
                }
                GUILayout.EndVertical();
            }
            GUILayout.EndArea();

            _nodeBoundry = tmpRect;
            if (Outputs.Any())
                _nodeBoundry.height += 18f;
            if (Inputs.Any())
            {
                _nodeBoundry.y -= 18f;
                _nodeBoundry.height += 18;
            }

            DrawConnections();

            #region Out Connections

            if (Outputs.Any())
            {
                if (Outputs.Count > 1)
                {
                    for (int i = 0; i < Outputs.Count; i++)
                    {
                        Outputs[i].Possition = new Vector2(i * (NodeRect.width - 46) / (Outputs.Count - 1) + tmpRect.x + 22, tmpRect.y + NodeRect.height + 8);
                        if (GUI.Button(new Rect(Outputs[i].Possition.x - 13, Outputs[i].Possition.y - 7, 26, 16), ""))
                        {
                            if (e.button == 0)
                            {
                                Outputs[i].IsOccupied = true;
                                HandleConnection(Outputs[i]);
                            }
                            else if (e.button == 1)
                            {
                                _connectionButton = Outputs[i];
                                ProcessContextMenu();
                            }
                        }
                    }
                }
                else
                {
                    Outputs[0].Possition = new Vector2(tmpRect.x + NodeRect.width / 2, tmpRect.y + NodeRect.height + 8);
                    if (GUI.Button(new Rect(Outputs[0].Possition.x - 13, Outputs[0].Possition.y - 7, 26, 16), ""))
                    {
                        if (e.button == 0)
                        {
                            Outputs[0].IsOccupied = true;
                            HandleConnection(Outputs[0]);
                        }
                        else if (e.button == 1)
                        {
                            _connectionButton = Outputs[0];
                            ProcessContextMenu();
                        }
                    }
                }
            }

            #endregion Out Connections

            #region In Connections

            if (Inputs.Any())
            {
                //GUI.Box(new Rect(NodeRect.x + 6, NodeRect.y - 18, 24, 18), "");
                //GUI.Box(new Rect(NodeRect.x + NodeRect.width - 30, NodeRect.y - 18, 24, 18), "");
                //GUI.Box(new Rect(NodeRect.x + 30, NodeRect.y - 18, NodeRect.width - 60, 18), "");

                for (int i = 0; i < Inputs.Count; i++)
                {
                    string result = string.Format("{0}", Id == -1 ? "" : Id.ToString());

                    //if(Inputs[i].ParentNode != null) {
                    //    var outputChilds = Inputs[i].ParentNode.Outputs[Inputs[i].ConnectedOutputID].ChildNodes;
                    //    result = string.Format("{0}", outputChilds.IndexOf(this) + 1);
                    //}

                    int count = Inputs.Count - 1 == 0 ? 2 : Inputs.Count-1;
                    float math = NodeRect.width / count;
                    float corrnerOffset = 0f;

                    if (i > 0)
                        corrnerOffset = i * 26f - i * 13f;

                    if (Inputs.Count == 1)
                        Inputs[i].Possition = new Vector2(tmpRect.x + math - 13f, tmpRect.y - 8);
                    else
                        Inputs[i].Possition = new Vector2(tmpRect.x + i * math - corrnerOffset, tmpRect.y - 8);
                    if (GUI.Button(new Rect(Inputs[i].Possition.x, Inputs[i].Possition.y - 7, 26f, 16f), result))
                    {
                        if (e.button == 0)
                        {
                            Inputs[i].IsOccupied = true;
                            FinalizeConnection(Inputs[i]);

                        }
                        else if (e.button == 1)
                        {
                            _connectionButton = Inputs[i];
                            ProcessContextMenu();
                        }
                    }
                }
            }

            #endregion In Connections

            HandleEvents(e, viewRect);

            if (GUI.changed)
                EditorUtility.SetDirty(this);
        }


        public virtual void HandleEvents(Event e, Rect viewRect)
        {
            if (IsSelected)
                //if(viewRect.Contains(e.mousePosition))
                if (e.type == EventType.MouseDrag)
                {
                    Vector2 delta = e.delta;

                    if (e.button == 0)
                    {
                        NodeRect.x += delta.x;
                        NodeRect.y += delta.y;
                    }

                    if (_baseType == NodeType.Branch && Outputs != null && Outputs[0].ChildNodes != null && e.button == 1)
                        MoveAllChillds(this, delta);
                }

        }
        #endregion

        #region Private Methods

        private void MoveAllChillds(NodeBase node, Vector2 delta)
        {
            node.NodeRect.x += delta.x;
            node.NodeRect.y += delta.y;

            if (node._baseType == NodeType.Branch)
                foreach (NodeBase child in node.Outputs[0].ChildNodes)
                    MoveAllChillds(child, delta);

        }

        private void DrawConnections()
        {
            if (Inputs == null || !Inputs.Any())
                return;

            foreach (NodeInput input in Inputs)
            {
                if (input.ParentNode == null || input.ConnectedOutputID == -1)
                    continue;
                Vector3 start = input.ParentNode.Outputs[input.ConnectedOutputID].Possition;
                Vector3 end = input.Possition;

                Vector2 startTangent;
                Vector2 endTangent;

                Color color = Color.gray;

                if (IsSelected || input.ParentNode.IsSelected)
                    color = new Color(1, 0.5f, 0);
                float offset = Math.Abs(start.x - end.x) / 1.75f;
                if (input.Possition.x < start.x)
                {
                    startTangent = new Vector2(start.x - offset, start.y);
                    endTangent = new Vector2(end.x + offset, end.y);
                }
                else
                {
                    startTangent = new Vector2(start.x + offset, start.y);
                    endTangent = new Vector2(end.x - offset, end.y);
                }

                Handles.BeginGUI();
                {
                    Handles.color = Color.white;
                    Handles.DrawBezier(start, end, startTangent, endTangent, color, null, 2);
                    //GUI.Label(new Rect(end.x, end.y - 40f, 25f, 25f), "a");

                }
                Handles.EndGUI();
            }
        }

        private void CheckSelect(Event e)
        {
            if (e.button == 0 && e.type == EventType.Used && NodeRect.Contains(e.mousePosition))
            {
                ParentGraph.DeselectAllNodes();
                ParentGraph.SelectedNode = this;
                IsSelected = true;
            }
        }

        private void ProcessContextMenu()
        {
            if (ParentGraph == null)
                return;

            GenericMenu menu = new GenericMenu();
            menu.AddItem(new GUIContent("Break Connection"), false, ContextCallback, _connectionButton);
            menu.ShowAsContext();
        }

        private void ContextCallback(object obj)
        {
            if (obj is NodeInput)
            {
                NodeInput input = obj as NodeInput;
                input.BreakInputConnection(this);
                //BreakInputConnection(input);
            }
            else if (obj is NodeOutput)
            {
                NodeOutput output = obj as NodeOutput;
                output.BreakOutputConnection();
                //BreakOutputConnection(output);
            }
        }

        private void HandleConnection(NodeOutput output)
        {
            if (ParentGraph == null)
                return;
            ParentGraph.WantsConnection = true;
            ActiveOutput = output;
            ParentGraph.ConnectionNode = this;
        }

        private void FinalizeConnection(NodeInput input)
        {
            if (ParentGraph == null || ParentGraph.ConnectionNode == null)
            {
                input.IsOccupied = false;
                return;
            }

            if (this.ParentGraph.ConnectionNode == this)
            {
                this.ParentGraph.WantsConnection = false;
                this.ParentGraph.ConnectionNode = null;
                input.IsOccupied = false;
                return;
            }

            input.Connect(this);
        }

        #endregion Private Methods

        #region Not-Implemented Virtual

        public virtual void ShowPriorityInfosProperties() { }
        public virtual void DrawNodeProperties()
        {
        }

        public virtual void DrawNodeHelp()
        {
        }

        public virtual void DrawNodeStatus()
        {
        }

        public virtual object GetArguments()
        {
            return null;
        }

        #endregion Not-Implemented Virtual


#endif
    }
}