﻿namespace Assets.aochmann.NodeEditor.Scripts.NodesGUI {

    public class DecoratorGUI : BranchGUI {

        public override void Initialize() {
            base.Initialize();
            Outputs[0].CanHaveMoreChilds = false;
        }
    }
}
