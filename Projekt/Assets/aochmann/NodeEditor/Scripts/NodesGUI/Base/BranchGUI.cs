﻿using Assets.Test.Scripts.Data;

namespace Assets.aochmann.NodeEditor.Scripts.NodesGUI {

    public class BranchGUI : NodeBase {

        public override void Initialize() {
            base.Initialize();
            base.SetBaseType(typeof(BranchGUI));
            Outputs.Add(new NodeOutput());
        }
    }
}
