﻿using System;
using Assets.aochmann.NodeEditor.Scripts.NodesGUI;
using UnityEditor;
using UnityEngine;

namespace Assets.aochmann.NodeEditor.Scripts.Data.Nodes {
    [Serializable]
    public class LogGUI : LeafGUI {

        [SerializeField]
        private string _logMessage = "";


        public override void Initialize() {
            base.Initialize();
            base.SetPropertiesShow();
            base.SetLargeRect(new Vector2(340f, 120f));
            NodeRect = new Rect(0, 0, 100f, 120f);
        }

        public string LogMessage {
            get { return _logMessage; }
        }

        private readonly GUILayoutOption LabelWidth  =  GUILayout.Width(70f);

        public override void DrawNodeProperties() {
            base.DrawNodeProperties();
            GUILayout.BeginHorizontal();
            {
                GUILayout.Label("Message:", LabelWidth);
                _logMessage = GUILayout.TextField(_logMessage, 50);
            }
            GUILayout.EndHorizontal();

            if(GUI.changed)
                EditorUtility.SetDirty(this);
        }

        public override object GetArguments() {
            return _logMessage;
        }

    }
}
