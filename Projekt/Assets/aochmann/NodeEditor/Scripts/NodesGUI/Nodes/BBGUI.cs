﻿using Assets.Test.Scripts.Data;
using UnityEditor;
using UnityEngine;

namespace Assets.aochmann.NodeEditor.Scripts.Data.Nodes {
    class BBGUI : NodeBase {

        public override void Initialize() {
            base.Initialize();
            base.SetPropertiesShow();
            base.SetLargeRect(new Vector2(285f, 120f));
        }

        [SerializeField]
        private int _selectedIndex = -1;

        private string[] _bbAction = new string[] { "set", "get",  "inc", "dec", "mul", "div" };
        private string _bbValue = string.Empty;
        private string _bbSymbol = string.Empty;


        private readonly GUILayoutOption LabelWidth =  GUILayout.Width(70f);
        private readonly GUILayoutOption OptionWidth =  GUILayout.Width(60f);


        public override void DrawNodeProperties() {
            base.DrawNodeProperties();
            GUILayout.BeginVertical();
            {
                GUILayout.Label(this.Inputs[0] != null && Inputs[0].ParentNode != null ? Inputs[0].ParentNode.Name : "Not set");
                GUILayout.BeginHorizontal();
                {
                    GUILayout.Label("BB action:", LabelWidth);
                    _selectedIndex =  EditorGUILayout.Popup(_selectedIndex, _bbAction, OptionWidth);
                    _bbSymbol = GUILayout.TextField(_bbSymbol, OptionWidth);
                    _bbValue = GUILayout.TextField(_bbValue, OptionWidth);
                }
                GUILayout.EndHorizontal();
            }
            GUILayout.EndVertical();
        }

        public override object GetArguments() {
            if(_selectedIndex != -1 && _bbSymbol.Length > 0) {
                object[] array = new object[3];
                array[0] = _bbAction[_selectedIndex];
                array[1] = _bbSymbol;

                if(_bbAction[_selectedIndex].Equals("inc") || _bbAction[_selectedIndex].Equals("dec") || _bbAction[_selectedIndex].Equals("mul") || _bbAction[_selectedIndex].Equals("div")) {
                    float value = 0f;
                    float.TryParse(_bbValue, out value);
                    array[2] = value;
                } else
                    array[2] = _bbValue;

                return array;
            }

            return null;
        }
    }
}
