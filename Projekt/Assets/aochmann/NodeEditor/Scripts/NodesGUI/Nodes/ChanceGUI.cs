﻿using Assets.aochmann.NodeEditor.Scripts.NodesGUI;
using UnityEditor;
using UnityEngine;

namespace Assets.aochmann.NodeEditor.Scripts.Data.Nodes {
    class ChanceGUI : DecoratorGUI {
        public override void Initialize() {
            base.Initialize();
            base.SetPropertiesShow();
            base.SetLargeRect(new Vector2(285f, 120f));
        }

        [SerializeField]
        private float _probability = 0.5f;

        private readonly GUILayoutOption LabelWidth  =  GUILayout.Width(80f);

        public override void DrawNodeProperties() {
            base.DrawNodeProperties();
            GUILayout.BeginHorizontal();
            {
                GUILayout.Label("Probability:", LabelWidth);
                _probability = EditorGUILayout.FloatField(_probability);
                if(_probability > 1f) _probability = 1f;
                if(_probability < 0f) _probability = 0f;
            }
            GUILayout.EndHorizontal();
        }

        public override object GetArguments() {
            return _probability;
        }
    }
}
