﻿using System;
using System.Linq;
using System.Reflection;
using Assets.aochmann.NodeEditor.Scripts.NodesGUI;
using UnityEditor;
using UnityEngine;

namespace Assets.aochmann.NodeEditor.Scripts.Data.Nodes {

    public class IfGUI : DecoratorGUI {

        #region Serialized-Fields

        [SerializeField]
        private string _componentName = string.Empty;

        [SerializeField]
        private int _seletedIndex = -1;

        [SerializeField]
        private string[] _methodsName;

        #endregion Serialized-Fields

        #region Non-Serialized Fields

        private MonoBehaviour _monoScript;

        #endregion Non-Serialized Fields

        #region Readonly and consts

        private readonly Type CustomTaskType = typeof(bool);
        private const BindingFlags FLAGS = BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly;
        private readonly GUILayoutOption LabelWidth =  GUILayout.Width(125f);
        private readonly GUILayoutOption OptionWidth =  GUILayout.Width(140f);

        #endregion Readonly and consts

        #region Properties

        public string ComponentName {
            get {
                return _componentName;
            }
        }

        public string SelectedMethod {
            get {
                string result = string.Empty;
                if(_seletedIndex == -1 || _methodsName == null || _methodsName.Length == 0 || _seletedIndex > _methodsName.Length) { } else
                    result = _methodsName[_seletedIndex];
                return result;
            }
        }

        #endregion Properties

        //Methods

        #region Public Methods

        public override void Initialize() {
            base.Initialize();
            base.SetPropertiesShow();
            base.SetLargeRect(new Vector2(300f, 140f));
        }


        public override void DrawNodeProperties() {
            base.DrawNodeProperties();
            GUILayout.Label(this.Inputs[0] != null && Inputs[0].ParentNode != null ? Inputs[0].ParentNode.Name : "Not set");
            MonoBehaviour tmpScript = null;

            GUILayout.BeginHorizontal();
            {
                GUILayout.Label("Custom action script:", LabelWidth);
#if UNITY_EDITOR
                tmpScript = EditorGUILayout.ObjectField(_monoScript, typeof(MonoBehaviour), true, OptionWidth) as MonoBehaviour;
#endif
            }
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            {
                GUILayout.Label("Target name:", LabelWidth);
                GUILayout.Label(ComponentName, OptionWidth);
            }
            GUILayout.EndHorizontal();

            if(tmpScript != _monoScript) {
                if(tmpScript != null) {
                    MethodInfo[] allMethods = tmpScript.GetType().GetMethods(FLAGS).Where(method => method.ReturnType == CustomTaskType).ToArray();

                    if(allMethods != null && allMethods.Length > 0) {
                        _methodsName = new string[allMethods.Length];

                        for(int i = 0; i < allMethods.Length; i++) {
                            _methodsName[i] = allMethods[i].Name;
                        }

                        _monoScript = tmpScript;
                        _componentName = tmpScript.GetType().Name;
                    }
                } else {
                    _componentName = "Not selected";
                    _monoScript    = null;
                    _methodsName   = null;
                }

                _seletedIndex = -1;
            }

            GUILayout.BeginHorizontal();
            {
                GUILayout.Label("Method:", LabelWidth);

                string methodName = "Not Selected";

                if(_methodsName != null) {
                    if(_seletedIndex == -1 || _seletedIndex > _methodsName.Length) { } else {
                        methodName = _methodsName[_seletedIndex];
                    }
                }

                if(GUILayout.Button(methodName, EditorStyles.popup, OptionWidth)) {
                    if(_methodsName == null || _methodsName.Length == 0) {
                    } else {
                        GenericMenu menu = new GenericMenu();
                        for(int i = 0; i < _methodsName.Length; i++) {
                            menu.AddItem(new GUIContent(_methodsName[i]), false, ContextCallback, i);
                        }

                        menu.ShowAsContext();
                    }
                }
            }
            GUILayout.EndHorizontal();
        }

        public override object GetArguments() {
            return new string[] { ComponentName, _methodsName == null ? "" : _methodsName[_seletedIndex] };
        }

        #endregion Public Methods

        #region Private Methods

        private void ContextCallback(object obj) {
            _seletedIndex = (int)obj;
        }

        #endregion Private Methods
    }
}
