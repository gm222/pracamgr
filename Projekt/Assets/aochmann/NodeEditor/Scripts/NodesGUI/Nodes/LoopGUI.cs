﻿using Assets.aochmann.NodeEditor.Scripts.NodesGUI;
using UnityEditor;
using UnityEngine;

namespace Assets.aochmann.NodeEditor.Scripts.Data.Nodes {
    class LoopGUI : DecoratorGUI {
        public override void Initialize() {
            base.Initialize();
            base.SetPropertiesShow();
            base.SetLargeRect(new Vector2(300f, 140f));
        }

        [SerializeField]
        private int _loops = 1;

        private readonly GUILayoutOption LabelWidth  =  GUILayout.Width(90f);

        public override void DrawNodeProperties() {
            base.DrawNodeProperties();
            GUILayout.BeginHorizontal();
            {
                GUILayout.Label("Loops:", LabelWidth);
                _loops = EditorGUILayout.IntField(_loops);

                if(_loops < 0) _loops = 0;

            }
            GUILayout.EndHorizontal();
        }

        public override object GetArguments() {
            return _loops;
        }
    }
}
