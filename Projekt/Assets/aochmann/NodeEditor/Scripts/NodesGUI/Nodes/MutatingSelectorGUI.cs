﻿using System;
using System.Linq;
using Assets.aochmann.NodeEditor.Scripts.NodesGUI;
using BAD;
using UnityEditor;
using UnityEngine;

namespace Assets.aochmann.NodeEditor.Scripts.Data.Nodes {
    class MutatingSelectorGUI : BranchGUI {
        public override void Initialize() {
            base.Initialize();
            base.SetPropertiesShow();
            base.SetLargeRect(new Vector2(300f, 140f));
        }

        [SerializeField]
        private string[] _policyArray = new string[] { MutationPolicy.MoveToBottom.ToString(), MutationPolicy.MoveToTop.ToString() };

        [SerializeField]
        private int _selectedIndex = 0;


        private readonly GUILayoutOption LabelWidth =  GUILayout.Width(70f);
        private readonly GUILayoutOption OptionWidth =  GUILayout.Width(60f);


        public override void DrawNodeProperties() {
            base.DrawNodeProperties();
            GUILayout.Label(this.Inputs[0] != null && Inputs[0].ParentNode != null ? Inputs[0].ParentNode.Name : "Not set");

            GUILayout.BeginHorizontal();
            {
                GUILayout.Label("Policy:", LabelWidth);
                _selectedIndex =  EditorGUILayout.Popup(_selectedIndex, _policyArray, OptionWidth);
            }
            GUILayout.EndHorizontal();
        }
        public override object GetArguments() {
            return Enum.GetValues(typeof(MutationPolicy)).Cast<MutationPolicy>().ToList()[_selectedIndex];
        }
    }
}
