﻿using Assets.aochmann.NodeEditor.Scripts.NodesGUI;
using UnityEditor;
using UnityEngine;

namespace Assets.aochmann.NodeEditor.Scripts.Data.Nodes {
    class CooldownGUI : DecoratorGUI {

        public override void Initialize() {
            base.Initialize();
            base.SetPropertiesShow();
            base.SetLargeRect(new Vector2(300f, 140f));
        }

        [SerializeField]
        private int _seconds = 1;

        private readonly GUILayoutOption LabelWidth  =  GUILayout.Width(110f);

        public override void DrawNodeProperties() {
            base.DrawNodeProperties();
            GUILayout.BeginHorizontal();
            {
                GUILayout.Label("Cooldown [sec]:", LabelWidth);
                _seconds = EditorGUILayout.IntField(_seconds);
                if(_seconds < 0) _seconds = 0;

            }
            GUILayout.EndHorizontal();
        }

        public override object GetArguments() {
            return _seconds;
        }
    }
}
