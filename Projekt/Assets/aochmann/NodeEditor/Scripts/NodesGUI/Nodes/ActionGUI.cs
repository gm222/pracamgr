﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Assets.aochmann.NodeEditor.Scripts.NodesGUI;
using UnityEditor;
using UnityEngine;

namespace Assets.aochmann.NodeEditor.Scripts.Data.Nodes {
    public class ActionGUI : LeafGUI {

        #region Private Fields

        private const BindingFlags FLAGS = BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly;

        private readonly Type CustomTaskType = typeof(IEnumerator<BAD.NodeResult>);

        private readonly GUILayoutOption LabelWidth =  GUILayout.Width(125f);

        private readonly GUILayoutOption OptionWidth =  GUILayout.Width(140f);

        private readonly Type ScriptType = typeof(ActionBase);

        [SerializeField]
        private string _componentName = string.Empty;

        [SerializeField]
        private string[] _methodsName;

        [SerializeField]
        private MonoScript _monoScript;

        [SerializeField]
        private string _selectedMethodName = string.Empty;

        [SerializeField]
        private int _seletedIndex = -1;

        #endregion Private Fields

        #region Public Properties

        public string ComponentName {
            get {
                return _componentName;
            }
        }

        //public MonoScript script;
        public string SelectedMethod {
            get {
                string result = string.Empty;
                if(_seletedIndex == -1 || _methodsName == null || _methodsName.Length == 0 || _seletedIndex > _methodsName.Length) { } else
                    result = _methodsName[_seletedIndex];
                return result;

            }
        }

        #endregion Public Properties

        #region Public Methods

        public override void DrawNodeProperties() {
            base.DrawNodeProperties();
            MonoScript tmpScript = null;

            GUILayout.BeginHorizontal();
            {
                GUILayout.Label("Custom action script:", LabelWidth);
#if UNITY_EDITOR
                tmpScript = EditorGUILayout.ObjectField(_monoScript, typeof(MonoScript), true, OptionWidth) as MonoScript;
#endif
            }
            GUILayout.EndHorizontal();
            //script = EditorGUILayout.ObjectField("Script:", script, typeof(MonoScript), false) as MonoScript;
            GUILayout.BeginHorizontal();
            {
                GUILayout.Label("Target name:", LabelWidth);
                GUILayout.Label(ComponentName, OptionWidth);
            }
            GUILayout.EndHorizontal();

            if(tmpScript != _monoScript) {
                bool result = true;
                if(tmpScript != null) {

                    Type classtmp = tmpScript.GetClass();

                    if(classtmp.BaseType == ScriptType) {

                        MethodInfo[] methodsInfo = classtmp.GetMethods(FLAGS).Where(method => method.ReturnType == CustomTaskType).ToArray();
                        _methodsName   = null;
                        _monoScript = tmpScript;
                        _componentName = classtmp.Name;
                        if(methodsInfo != null && methodsInfo.Length > 0) {
                            _methodsName = new string[methodsInfo.Length];

                            for(int i = 0; i < methodsInfo.Length; i++)
                                _methodsName[i] = methodsInfo[i].Name;
                        }
                    } else result = false;
                } else result = false;

                if(result == false) {
                    _componentName = "Not selected";
                    _monoScript    = null;
                    _methodsName   = null;
                }
                _selectedMethodName = string.Empty;
                _seletedIndex = -1;
            }

            GUILayout.BeginHorizontal();
            {
                GUILayout.Label("Method:", LabelWidth);
                string methodName = "Not Selected";

                if(_methodsName != null) {
                    if(_seletedIndex == -1 || _seletedIndex > _methodsName.Length) { } else {
                        _selectedMethodName = methodName = _methodsName[_seletedIndex];
                    }
                }

                if(GUILayout.Button(methodName, EditorStyles.popup, OptionWidth)) {
                    if(_methodsName == null || _methodsName.Length == 0) {
                    } else {
                        GenericMenu menu = new GenericMenu();

                        for(int i = 0; i < _methodsName.Length; i++) {
                            menu.AddItem(new GUIContent(_methodsName[i]), false, ContextCallback, i);
                        }
                        menu.ShowAsContext();
                    }

                }
            }
            GUILayout.EndHorizontal();
        }

        public override object GetArguments() {
            string method = string.Empty;
            if(_methodsName != null) {
                if(_seletedIndex < 0) { } else if(_seletedIndex >= _methodsName.Length) { } else method = _methodsName[_seletedIndex];
            }

            return new string[] { ComponentName, method };
        }

        public override void Initialize() {
            base.Initialize();
            base.SetPropertiesShow();
            base.SetLargeRect(new Vector2(300f, 140f));
        }

        #endregion Public Methods

        #region Private Methods

        private void ContextCallback(object obj) {
            _seletedIndex = (int)obj;
        }
#if UNITY_EDITOR
        private void OnValidate() {
            if(_monoScript != null) {
                Type classtmp = _monoScript.GetClass();

                MethodInfo[] methodsInfo = classtmp.GetMethods(FLAGS).Where(method => method.ReturnType == CustomTaskType).ToArray();

                if(methodsInfo != null && methodsInfo.Length > 0) {
                    _methodsName = new string[methodsInfo.Length];

                    for(int i = 0; i < methodsInfo.Length; i++)
                        _methodsName[i] = methodsInfo[i].Name;

                    if(_selectedMethodName.Length != 0) {
                        int tmpIndex = Array.IndexOf<string>(_methodsName, _selectedMethodName);
                        _seletedIndex = tmpIndex;
                    }

                    if(_seletedIndex >= methodsInfo.Length || _seletedIndex < 0) {
                        _seletedIndex = -1;
                        _selectedMethodName = string.Empty;
                    }
                }
            } else {
                _seletedIndex = -1;
                _selectedMethodName = string.Empty;
                _componentName = "Not selected";
                _monoScript    = null;
                _methodsName   = null;
            }
        }


#endif
        #endregion Private Methods
    }
}
