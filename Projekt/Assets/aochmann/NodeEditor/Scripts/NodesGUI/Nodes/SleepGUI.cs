﻿using Assets.aochmann.NodeEditor.Scripts.NodesGUI;
using UnityEditor;
using UnityEngine;

namespace Assets.aochmann.NodeEditor.Scripts.Data.Nodes {
    public class SleepGUI : LeafGUI {

        [SerializeField]
        [Range(0f, 180f)]
        private float _sleepTime = 1f;

        public float SleepTime {
            get { return _sleepTime; }
        }

        public override void Initialize() {
            base.Initialize();
            base.SetPropertiesShow();
            base.SetLargeRect(new Vector2(300f, 140f));
        }

        public override void DrawNodeProperties() {
            base.DrawNodeProperties();

            GUILayout.BeginHorizontal();
            {
                GUILayout.Label("Sleep Time:");
                _sleepTime = EditorGUILayout.FloatField(_sleepTime);
                if(_sleepTime < 0f) _sleepTime = 0f;
            }
            GUILayout.EndHorizontal();
        }

        public override object GetArguments() {
            return _sleepTime;
        }
    }
}
