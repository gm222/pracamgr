﻿using System;
using System.Linq;
using System.Reflection;
using Assets.aochmann.NodeEditor.Scripts.NodesGUI;
using UnityEditor;
using UnityEngine;

namespace Assets.aochmann.NodeEditor.Scripts.Data.Nodes {

    [Serializable]
    public class ConditionGUI : LeafGUI {
        //Fields
        #region Serialized-Fields
        [SerializeField]
        private string _componentName = string.Empty;

        [SerializeField]
        private int _seletedIndex = -1;

        [SerializeField]
        private string[] _methodsName;

        [SerializeField]
        private MonoScript _monoScript;
        [SerializeField]
        private string _selectedMethodName = string.Empty;


        #endregion
        #region Non-Serialized Fields


        #endregion
        #region Readonly and consts
        private readonly Type CustomTaskType = typeof(bool);
        private readonly Type ScriptType = typeof(ConditionBase);
        private const BindingFlags FLAGS = BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly;
        private readonly GUILayoutOption LabelWidth =  GUILayout.Width(125f);
        private readonly GUILayoutOption OptionWidth =  GUILayout.Width(140f);

        #endregion
        #region Properties
        public string ComponentName {
            get {
                return _componentName;
            }
        }

        public string SelectedMethod {
            get {
                string result = string.Empty;
                if(_seletedIndex == -1 || _methodsName == null || _methodsName.Length == 0 || _seletedIndex > _methodsName.Length) { } else
                    result = _methodsName[_seletedIndex];
                return result;
            }
        }

        #endregion

        //Methods
        #region Public Methods
        public override void Initialize() {
            base.Initialize();
            base.SetPropertiesShow();
            base.SetLargeRect(new Vector2(300f, 140f));
        }

        public override void DrawNodeProperties() {
            base.DrawNodeProperties();
            MonoScript tmpScript = null;

            GUILayout.BeginHorizontal();
            {
                GUILayout.Label("Custom action script:", LabelWidth);
#if UNITY_EDITOR
                tmpScript = EditorGUILayout.ObjectField(_monoScript, typeof(MonoScript), true, OptionWidth) as MonoScript;
#endif
            }
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            {
                GUILayout.Label("Target name:", LabelWidth);
                GUILayout.Label(ComponentName, OptionWidth);
            }
            GUILayout.EndHorizontal();

            if(tmpScript != _monoScript) {
                bool result = true;
                if(tmpScript != null) {

                    Type classtmp = tmpScript.GetClass();

                    if(classtmp.BaseType == ScriptType) {

                        MethodInfo[] methodsInfo = classtmp.GetMethods(FLAGS).Where(method => method.ReturnType == CustomTaskType).ToArray();
                        _methodsName   = null;
                        _monoScript = tmpScript;
                        _componentName = classtmp.Name;

                        if(methodsInfo != null && methodsInfo.Length > 0) {
                            _methodsName = new string[methodsInfo.Length];

                            for(int i = 0; i < methodsInfo.Length; i++)
                                _methodsName[i] = methodsInfo[i].Name;
                        }
                    } else result = false;
                } else result = false;

                if(result == false) {
                    _componentName = "Not selected";
                    _monoScript    = null;
                    _methodsName   = null;
                }

                _selectedMethodName = string.Empty;
                _seletedIndex = -1;
            }

            GUILayout.BeginHorizontal();
            {
                GUILayout.Label("Method:", LabelWidth);

                string methodName = "Not Selected";

                if(_methodsName != null) {
                    if(_seletedIndex == -1 || _seletedIndex > _methodsName.Length) { } else {
                        _selectedMethodName = methodName = _methodsName[_seletedIndex];
                    }
                }

                if(GUILayout.Button(methodName, EditorStyles.popup, OptionWidth)) {
                    if(_methodsName == null || _methodsName.Length == 0) {
                    } else {
                        GenericMenu menu = new GenericMenu();
                        for(int i = 0; i < _methodsName.Length; i++) {
                            menu.AddItem(new GUIContent(_methodsName[i]), false, ContextCallback, i);
                        }

                        menu.ShowAsContext();
                    }
                }
            }
            GUILayout.EndHorizontal();

        }
        public override object GetArguments() {


            string method = string.Empty;
            if(_methodsName != null) {
                if(_seletedIndex < 0) { _componentName = string.Empty; } else if(_seletedIndex >= _methodsName.Length) { } else method = _methodsName[_seletedIndex];
            }

            return new string[] { ComponentName, method };
        }
        #endregion
        #region Private Methods
        private void ContextCallback(object obj) {
            _seletedIndex = (int)obj;
        }

        #endregion

#if UNITY_EDITOR
        private void OnValidate() {
            if(_monoScript != null) {
                Type classtmp = _monoScript.GetClass();
                _componentName = classtmp.Name;
                MethodInfo[] methodsInfo = classtmp.GetMethods(FLAGS).Where(method => method.ReturnType == CustomTaskType).ToArray();

                if(methodsInfo != null && methodsInfo.Length > 0) {
                    _methodsName = new string[methodsInfo.Length];

                    for(int i = 0; i < methodsInfo.Length; i++)
                        _methodsName[i] = methodsInfo[i].Name;

                    if(_selectedMethodName.Length != 0) {
                        int tmpIndex = Array.IndexOf<string>(_methodsName, _selectedMethodName);
                        _seletedIndex = tmpIndex;
                    }

                    if(_seletedIndex >= methodsInfo.Length || _seletedIndex < 0) {
                        _seletedIndex = -1;
                        _selectedMethodName = string.Empty;
                    }
                }
            } else {
                _seletedIndex = -1;
                _selectedMethodName = string.Empty;
                _componentName = "Not selected";
                _monoScript    = null;
                _methodsName   = null;
            }
        }
#endif
    }
}
