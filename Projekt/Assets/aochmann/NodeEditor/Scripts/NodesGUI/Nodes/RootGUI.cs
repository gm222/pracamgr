﻿using UnityEngine;

namespace Assets.aochmann.NodeEditor.Scripts.NodesGUI.Nodes {
    public class RootGUI : BranchGUI {
        public override void Initialize() {
            base.Initialize();
            base.SetPropertiesShow();
            base.SetLargeRect(new Vector2(120f, 100f));
        }

        private readonly GUILayoutOption LabelWidth =  GUILayout.Width(70f);
        private readonly GUILayoutOption OptionWidth =  GUILayout.Width(60f);

        public bool IsStartUP {
            get { return ParentGraph.StartupNode == this; }
            set {
                if(value == true)
                    ParentGraph.ChangeStartupNode(this);
                if(IsStartUP == this && value == false)
                    ParentGraph.ChangeStartupNode(null);
            }
        }

        public override void ShowPriorityInfosProperties() {
            base.ShowPriorityInfosProperties();
            GUILayout.BeginHorizontal();
            {
                GUILayout.Label("Is startup: ", LabelWidth);
                IsStartUP = GUILayout.Toggle(IsStartUP, "", OptionWidth);
            }
            GUILayout.EndHorizontal();
        }
    }
}
