﻿using Assets.aochmann.NodeEditor.Editor.Views.EnemyEditor;
using Assets.aochmann.NodeEditor.Scripts;
using Assets.Test.Editor.Utilities;
using Assets.Test.Scripts.Data;
using BAD;
using UnityEditor;
using UnityEngine;

namespace Assets.aochmann.NodeEditor.Editor.Windows {
    public class EnemyEditorWindow : EditorWindow {
        public static EnemyEditorWindow Instance;

        #region SERIALIZED FIELDS
        #region Views
        [SerializeField]
        private EnemyWorkView       WorkView;
        [SerializeField]
        private EnemyToolbarView    ToolsView;
        [SerializeField]
        private EnemyPropertyView PropertiesView;
        [SerializeField]
        private EnemyHelpView       HelpView;
        [SerializeField]
        private EnemyStagesView StagesView;

        [SerializeField]
        private EnemyStatusView StatusView;
        #endregion
        #region Others
        [SerializeField]
        private BADReactor _nodeReactor;
        [SerializeField]
        private EnemyLogicContainer _nodeContainer;
        #endregion
        #endregion
        #region NON-SERIALIZED FIELDS
        private float _viewVerticalPercetage   = 0.7f;

        private GameObject _selectedGameObject = null;

        private float _zoom = 1f;
        private float _minZoom = 0.5f;
        private float _maxZoom = 2.0f;

        #endregion
        #region Properties
        public float Zoom {
            get { return _zoom; }
            set {
                _zoom = Mathf.Clamp(value, _minZoom, _maxZoom);
            }
        }

        #endregion


        public static void Init() {
            Instance                 = GetWindow<EnemyEditorWindow>();
            Instance.titleContent    = new GUIContent("Enemy BT Editor");
            Instance.maxSize         = new Vector2(Screen.width, Screen.height);
            Instance.minSize         = new Vector2(1000f, 800f);
            Instance.WorkView        = new EnemyWorkView();
            Instance.ToolsView       = new EnemyToolbarView();
            Instance.PropertiesView  = new EnemyPropertyView();
            Instance.HelpView        = new EnemyHelpView();
            Instance.StagesView      = new EnemyStagesView();
            Instance.StatusView      = new EnemyStatusView();

            CreatViews();
        }

        private static void CreatViews() {
            if(Instance != null) {
                Instance.Focus();
                Instance.Show();
            } else
                Init();
        }

        private NodeGraph _graph;

        public void Update() {
            if(_selectedGameObject == null || _selectedGameObject != Selection.activeGameObject)
                CheckObjectChannge();

            if(_nodeContainer != null) {
                if(_nodeContainer.EnemyLogics != null && _nodeContainer.EnemyLogics.Count > 0 && _nodeContainer.SelectedIndex != -1 && _nodeContainer.SelectedIndex < _nodeContainer.EnemyLogics.Count) {
                    _graph = _nodeContainer.EnemyLogics[_nodeContainer.SelectedIndex];
                    if(_graph != null)
                        _graph.UpdateGraph(Event.current);
                } else
                    _graph = null;
            } else
                _graph = null;


        }

        public void OnGUI() {
            if(WorkView == null || ToolsView == null || PropertiesView == null || HelpView == null || StatusView == null) {
                CreatViews();
                return;
            }

            Event e = Event.current;

            #region Views Draw Area
            float leftWidth, rightWidth;
            leftWidth = rightWidth = 0.15f;
            float rightPosition = 1 - rightWidth;

            Rect windowRect                = new Rect(position.width, position.height, position.width, position.height);
            Rect stagesViewPercentage      = new Rect(0f, 0f, leftWidth, 1 - 0.05f);
            Rect workViewPercentage        = new Rect(leftWidth, 0.05f, 1 - leftWidth - rightWidth, 0.9f);
            Rect propertiesViewPercentage  = new Rect(rightPosition, 0f, rightWidth, _viewVerticalPercetage + 0.15f);
            Rect helpViewPercentage        = new Rect(rightPosition, 1- 0.15f, rightWidth, 0.1f);
            Rect toolsViewPercentage       = new Rect(leftWidth, 0, 1f - leftWidth - rightWidth, 0.05f);
            Rect statusViewPercentage      = new Rect(0, 1 - 0.05f, 1f, 0.05f);

            #endregion
            #region Views Settings
            bool playMode                   = Application.isPlaying;
            #endregion
            #region Views Display
            StagesView.UpdateView(windowRect, stagesViewPercentage, e, _graph, _nodeContainer, playMode);
            PropertiesView.UpdateView(windowRect, propertiesViewPercentage, e, _graph, _nodeContainer, playMode);
            WorkView.UpdateView(windowRect, workViewPercentage, e, _graph, playMode);
            HelpView.UpdateView(windowRect, helpViewPercentage, e, _graph);
            ToolsView.UpdateView(windowRect, toolsViewPercentage, e, _graph, playMode);
            StatusView.UpdateView(windowRect, statusViewPercentage, e, _graph);
            #endregion

            HandleEvents(e);

            if(e.type == EventType.Repaint) {
                Repaint();
            }
        }

        #region Private Methods
        private void CheckObjectChannge() {
            _nodeContainer = null;
            _graph         = null;
            _nodeReactor   = null;

            _selectedGameObject = Selection.activeGameObject;

            if(_selectedGameObject != null) {
                _nodeReactor = _selectedGameObject.GetComponent<BADReactor>();

                if(_nodeReactor == null) {
                    _selectedGameObject = null;
                    return;
                }

                _nodeContainer = _selectedGameObject.GetComponent<EnemyLogicContainer>();

                if(_nodeContainer == null) {
                    Reset();
                    return;
                }

                NodeUtilities.LoadGraph(ref _nodeContainer);

            }
        }

        private void Reset() {
            _selectedGameObject = null;
            _nodeContainer      = null;
            _graph              = null;
            _nodeReactor        = null;
        }


        /// <summary>
        /// This is for manipulating bars width/height
        /// </summary>
        private void HandleEvents(Event e) {

        }
        #endregion
    }
}
