﻿using UnityEditor;
using UnityEngine;

namespace Assets.Test.Editor.Windows {

    public class NodePopupWindow : EditorWindow {
        private static NodePopupWindow Instance;
        private string GraphName = "Enter a name...";

        public static void Init() {
            Instance = GetWindow<NodePopupWindow>();
            Instance.titleContent = new UnityEngine.GUIContent("Graph name");
            Instance.maxSize = new UnityEngine.Vector2(300, 80);
            Instance.minSize = Instance.maxSize;
        }

        public void OnGUI() {
            GUILayout.Space(20);
            GUILayout.BeginHorizontal();
            {
                GUILayout.Space(20);
                EditorGUILayout.LabelField("New Graph", EditorStyles.boldLabel, GUILayout.Width(80));
                GraphName = EditorGUILayout.TextField(GraphName);
                GUILayout.Space(20);
            }
            GUILayout.EndHorizontal();
            GUILayout.Space(6);
            GUILayout.BeginHorizontal();
            {
                GUILayout.Space(20);
                if(GUILayout.Button("Create")) {
                    if(!string.IsNullOrEmpty(GraphName) && !GraphName.Equals("Enter a name...")) {
                        //NodeUtilities.CreatNodeGraph(GraphName);
                        Instance.Close();
                    } else {
                        EditorUtility.DisplayDialog("Error", "Enter a valid name for the graph", "OK");
                    }
                }

                GUILayout.Space(10);
                if(GUILayout.Button("Cancel")) {
                    Instance.Close();
                }
                GUILayout.Space(20);
            }
            GUILayout.EndHorizontal();
            GUILayout.Space(20);
            Repaint();
        }
    }
}