﻿using Assets.aochmann.NodeEditor.Scripts;
using Assets.Test.Editor.Utilities;
using Assets.Test.Editor.Views;
using Assets.Test.Scripts.Data;
using UnityEditor;
using UnityEngine;

namespace Assets.Test.Editor.Windows
{

    public class NodeEditorWindow : EditorWindow
    {
        public static NodeEditorWindow Instance;

        #region SERIALIZED FIELDS
        #region Views
        [SerializeField]
        private NodeWorkView WorkView;
        [SerializeField]
        private NodeToolbarView ToolsView;
        [SerializeField]
        private NodePropertiesView PropertiesView;
        [SerializeField]
        private NodeHelpView HelpView;
        [SerializeField]
        private NodeStatusView StatusView;
        #endregion
        #region Others
        #endregion
        #endregion
        #region NON-SERIALIZED FIELDS
        private float _viewHorizontalPercetage = 0.75f;
        private float _viewVerticalPercetage = 0.7f;

        private Object _selectedGameObject = null;

        private float _zoom = 1f;
        private float _minZoom = 0.01f;
        private float _maxZoom = 2.0f;

        #endregion
        #region Properties
        public float Zoom
        {
            get { return _zoom; }
            set
            {
                _zoom = Mathf.Clamp(value, _minZoom, _maxZoom);
            }
        }

        #endregion


        public static void Init()
        {
            Instance = GetWindow<NodeEditorWindow>();
            Instance.titleContent = new GUIContent("Behaviour Tree Editor");
            Instance.maxSize = new Vector2(Screen.width, Screen.height);
            Instance.minSize = new Vector2(600f, 800f);

            Instance.WorkView = new NodeWorkView();
            Instance.ToolsView = new NodeToolbarView(Instance.WorkView);
            Instance.PropertiesView = new NodePropertiesView();
            Instance.HelpView = new NodeHelpView();
            Instance.StatusView = new NodeStatusView();

            CreatViews();
        }

        private static void CreatViews()
        {
            if (Instance != null)
            {
                Instance.Focus();
                Instance.Show();
            }
            else
                Init();
        }

        private NodeGraph _graph;

        public void Update()
        {
            if (_selectedGameObject == null || _selectedGameObject != Selection.activeGameObject)
                CheckObjectChannge();

            if (_graph != null)
                _graph.UpdateGraph(Event.current);


        }

        public void OnGUI()
        {
            if (WorkView == null || ToolsView == null || PropertiesView == null || HelpView == null || StatusView == null)
            {
                CreatViews();
                return;
            }




            #region Views Draw Area
            Rect windowRect = new Rect(position.width, position.height, position.width, position.height);
            Rect workViewPercentage = new Rect(0, 0.05f, _viewHorizontalPercetage, 0.9f);
            Rect propertiesViewPercentage = new Rect(_viewHorizontalPercetage, 0.05f, 1 - _viewHorizontalPercetage, _viewVerticalPercetage + 0.15f);



            Rect helpViewPercentage = new Rect(_viewHorizontalPercetage, 1- 0.15f, 1 - _viewHorizontalPercetage, 0.1f);
            Rect statusViewPercentage = new Rect(0, 1 - 0.05f, 1, 0.05f);
            Rect toolsViewPercentage = new Rect(0, 0, 1, 0.05f);
            #endregion
            #region Views Settings
            bool playMode = Application.isPlaying;
            #endregion
            Event e = Event.current;

            #region Views Display

            PropertiesView.UpdateView(windowRect, propertiesViewPercentage, e, _graph, playMode);
            WorkView.UpdateView(windowRect, workViewPercentage, e, _graph, playMode);
            HelpView.UpdateView(windowRect, helpViewPercentage, e, _graph);
            StatusView.UpdateView(windowRect, statusViewPercentage, e, _graph, playMode);
            ToolsView.UpdateView(windowRect, toolsViewPercentage, e, _graph);
            #endregion


            HandleEvents(e);

            if (e.type == EventType.Repaint)
            {

                Repaint();
            }
        }

        #region Private Methods
        private void CheckObjectChannge()
        {
            _graph         = null;

            _selectedGameObject = Selection.activeObject;

            if (_selectedGameObject != null)
            {

                if (_selectedGameObject is GameObject)
                {
                    var nodeContainer = (_selectedGameObject as GameObject).GetComponent<NodesContainer>();
                    if (nodeContainer == null)
                    {
                        _selectedGameObject = null;
                        return;
                    }

                    if (nodeContainer.GUIGraph == null)
                        nodeContainer.GUIGraph = NodeUtilities.CreatNodeGraph();
                    else
                        NodeUtilities.LoadGraph(ref nodeContainer.GUIGraph);
                    _graph = nodeContainer.GUIGraph;
                }
                else
                {
                    NodeGraph graph = _selectedGameObject as NodeGraph;
                    if (graph != null)
                    {
                        _graph = graph;
                    }
                }
            }
        }


        /// <summary>
        /// This is for manipulating bars width/height
        /// </summary>
        private void HandleEvents(Event e)
        {
            if (e.type == EventType.ScrollWheel && Instance != null)
            {
                Vector2 delta = e.delta;
                float zoomDelta = -delta.y / 150f;
                Instance.Zoom += zoomDelta;
                return;
            }

            if (e.type == EventType.KeyDown)
            {
                switch (e.keyCode)
                {
                    case KeyCode.LeftArrow:
                        _viewHorizontalPercetage -= .01f;
                        break;

                    case KeyCode.RightArrow:
                        _viewHorizontalPercetage += .01f;
                        break;

                    case KeyCode.UpArrow:
                        _viewVerticalPercetage -= .01f;
                        break;

                    case KeyCode.DownArrow:
                        _viewVerticalPercetage += .01f;
                        break;
                }
                if (_viewHorizontalPercetage < 0)
                    _viewHorizontalPercetage = 0f;
                if (_viewHorizontalPercetage > .81f)
                    _viewHorizontalPercetage = .81f;

                if (_viewVerticalPercetage < .003f)
                    _viewVerticalPercetage = .003f;
                if (_viewVerticalPercetage > .82f)
                    _viewVerticalPercetage = .82f;
                e.Use();
            }


        }
        #endregion
    }
}