﻿using Assets.Test.Editor.Windows;
using Assets.Test.Scripts.Data;
using UnityEngine;

namespace Assets.Test.Editor.Views {

    public class NodeHelpView : ViewBase {


        public NodeHelpView() : base("Zoom View") {
        }

        #region Overrides of ViewBase

        public override void UpdateView(Rect editorRect, Rect percentageRect, Event e, NodeGraph graph, bool playMode = false) {
            base.UpdateView(editorRect, percentageRect, e, graph);


            HandleEvents(e, graph);

            GUILayout.BeginArea(ViewRect);
            {
                GUILayout.BeginVertical();
                {
                    GUILayout.FlexibleSpace();
                    GUILayout.BeginHorizontal();
                    {
                        GUILayout.FlexibleSpace();
                        GUILayout.FlexibleSpace();
                        NodeEditorWindow.Instance.Zoom = GUILayout.HorizontalSlider(NodeEditorWindow.Instance.Zoom, 0.01f, 2f, GUILayout.Width(120f));
                        GUILayout.Label(string.Format("{0:0.###}", NodeEditorWindow.Instance.Zoom), GUILayout.Width(40));
                        GUILayout.FlexibleSpace();
                    }
                    GUILayout.EndHorizontal();
                    GUILayout.BeginHorizontal();
                    {
                        GUILayout.FlexibleSpace();
                        if(GUILayout.Button("Set default", GUILayout.Width(80f))) {
                            NodeEditorWindow.Instance.Zoom = 1f;
                        }
                        GUILayout.FlexibleSpace();
                    }
                    GUILayout.EndHorizontal();
                    GUILayout.FlexibleSpace();
                }
                GUILayout.EndVertical();
            }
            GUILayout.EndArea();


        }

        private void HandleEvents(Event e, NodeGraph graph) {
            if(ViewRect.Contains(e.mousePosition) && e.button == 0 && e.type == EventType.MouseDown && graph != null) {
                graph.DeselectAllNodes();
            }

        }

        #endregion Overrides of ViewBase

        private void PrecessContextMenu(Event e) {
        }

        private void ContextCalback(object obj) {
        }
    }
}