﻿using Assets.aochmann.NodeEditor.Scripts.NodesUtilities;
using Assets.Test.Scripts.Data;
using UnityEngine;

namespace Assets.Test.Editor.Views
{

    public class NodeWorkView : ViewBase
    {
        public float toolbarHeight;
        public Vector2 _dragPosition = Vector2.zero;


        public NodeWorkView() : base("Work View")
        {
        }

        #region Overrides of ViewBase

        public override void UpdateView(Rect editorRect, Rect percentageRect, Event e, NodeGraph graph, bool playMode = false)
        {
            base.UpdateView(editorRect, percentageRect, e, graph, playMode);

            Rect newArea = EditorZoomArea.Begin(Windows.NodeEditorWindow.Instance.Zoom, ViewRect);
            {
                GUILayout.BeginArea(newArea);
                {
                    GUI.enabled = !playMode;

                    if (graph != null)
                        graph.UpdateGraphGUI(e, newArea);

                    GUI.enabled = true;
                }
                GUILayout.EndArea();
            }
            EditorZoomArea.End();

            HandleEvents(e);
        }

        public void ResetPosition()
        {
            _dragPosition = Vector2.zero;
        }

        private void HandleEvents(Event e)
        {
            //TODO: In the future make zoom to the mouse position
            if (_currentGraph == null)
                return;

            //foreach(var node in _currentGraph.Nodes)
            //    if(node.NodeBoundry.Contains(e.mousePosition - new Vector2(0, toolbarHeight)))
            //        return;

            //if(e.type == EventType.ScrollWheel && Windows.NodeEditorWindow.Instance != null) {
            //    Vector2 delta   = e.delta;
            //    float zoomDelta = -delta.y / 150f;
            //    Windows.NodeEditorWindow.Instance.Zoom += zoomDelta;
            //    return;
            //} else 
            if (e.type == EventType.MouseDrag && ((Event.current.button == 0 && Event.current.modifiers == EventModifiers.Alt) || Event.current.button == 2))
            {
                float x = e.delta.x / Windows.NodeEditorWindow.Instance.Zoom;
                float y = e.delta.y / Windows.NodeEditorWindow.Instance.Zoom;

                _dragPosition.x += x;
                _dragPosition.y += y;

                foreach (NodeBase node in _currentGraph.Nodes)
                {
                    node.SetDragPosition(_dragPosition);
                    //node.NodeRect.x += x;
                    //node.NodeRect.y += y;
                }
            }
        }

        #endregion Overrides of ViewBase
    }
}