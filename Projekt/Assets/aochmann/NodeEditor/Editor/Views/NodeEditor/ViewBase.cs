﻿using System;
using Assets.Test.Scripts.Data;
using UnityEngine;

namespace Assets.Test.Editor.Views {

    [Serializable]
    public class ViewBase : ScriptableObject {
        [SerializeField]
        private string _viewTitle;

        [SerializeField]
        private Rect _viewRect;

        public string ViewTitle {
            get { return _viewTitle; }
            set { _viewTitle = value; }
        }

        public Rect ViewRect {
            get {
                return _viewRect;
            }
        }

        protected NodeGraph _currentGraph;

        public ViewBase() {
            _viewTitle = string.Empty;
            _viewRect = new Rect();
        }

        public ViewBase(string viewTitle) : this() {
            _viewTitle = viewTitle;
        }

        public ViewBase(string viewTitle, Rect viewRect) : this(viewTitle) {
            _viewRect = viewRect;
        }

        public virtual void UpdateView(Rect editorRect, Rect percentageRect, Event e, NodeGraph graph, bool playMode = false) {

            _viewRect = new Rect(editorRect.x * percentageRect.x,
                editorRect.y * percentageRect.y,
                editorRect.width * percentageRect.width,
                editorRect.height * percentageRect.height);

            _currentGraph = graph;

            GUI.Box(ViewRect, ViewTitle);
        }

        public virtual void ProcessEvents(Event e) {
        }

    }
}