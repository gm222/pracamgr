﻿using Assets.Test.Scripts.Data;
using UnityEngine;

namespace Assets.Test.Editor.Views
{

    public class NodeToolbarView : ViewBase
    {
        private readonly NodeWorkView _workView;
        //private GUIContent newGraphContent;
        // private GUIContent saveGraphContent;
        //private GUIContent loadGraphContent;
        //private GUIContent separatorContent;

        public NodeToolbarView(NodeWorkView workView) : base("Toolbar View")
        {
            _workView = workView;
            //newGraphContent = new GUIContent("New Graph", null, "New Graph");
            //saveGraphContent = new GUIContent("Save Graph", null, "Save Graph");
            //loadGraphContent = new GUIContent("Load Graph", null, "Load Graph");
            //separatorContent = new GUIContent("", null, "");
        }

        private const float HeightOffset = 10f;
        #region Overrides of ViewBase

        public override void UpdateView(Rect editorRect, Rect percentageRect, Event e, NodeGraph graph, bool playMode = false)
        {
            base.UpdateView(editorRect, percentageRect, e, graph);

            ProcessEvents(e);


            GUI.Box(ViewRect, ViewTitle);

            GUILayout.BeginArea(ViewRect);
            {
                GUI.enabled = !playMode;
                GUILayout.BeginHorizontal();
                {
                    if (GUILayout.Button("Collapse all nodes") && graph != null)
                        foreach (NodeBase node in graph.Nodes)
                            node.Collapse();

                    if (GUILayout.Button("Expand all nodes") && graph != null)
                        foreach (NodeBase node in graph.Nodes)
                            node.ExpandAll();

                    if (GUILayout.Button("Expand all leafs nodes") && graph != null)
                        foreach (NodeBase node in graph.Nodes)
                            node.ExpandLeafsNodes();

                    if (GUILayout.Button("Reset position") && graph != null)
                    {
                        _workView.ResetPosition();
                        foreach (NodeBase node in graph.Nodes)
                            node.SetDragPosition(Vector2.zero);
                    }

                    //    GUILayout.Box(separatorContent, GUI.skin.box, GUILayout.Height(ViewRect.height - HeightOffset), GUILayout.Width(2f));

                    //    if(GUILayout.Button(newGraphContent, GUILayout.Width(80f), GUILayout.Height(ViewRect.height - HeightOffset))) {
                    //        NodePopupWindow.Init();
                    //    }

                    //    GUILayout.Box(separatorContent, GUI.skin.box, GUILayout.Height(ViewRect.height - HeightOffset), GUILayout.Width(2f));

                    //    if(GUILayout.Button(saveGraphContent, GUILayout.Width(80f), GUILayout.Height(ViewRect.height - HeightOffset)) && graph != null) {
                    //        Utilities.NodeUtilities.SaveGraphToFile(graph);
                    //    }
                }

                GUILayout.EndHorizontal();
                GUI.enabled = true;
            }
            GUILayout.EndArea();

        }

        public override void ProcessEvents(Event e)
        {
            base.ProcessEvents(e);
        }

        #endregion Overrides of ViewBase
    }
}