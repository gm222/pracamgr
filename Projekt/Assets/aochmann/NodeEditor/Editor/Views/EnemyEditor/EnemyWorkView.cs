﻿using Assets.Test.Editor.Views;
using Assets.Test.Scripts.Data;
using UnityEngine;

namespace Assets.aochmann.NodeEditor.Editor.Views.EnemyEditor
{
    public class EnemyWorkView : ViewBase
    {
        public float toolbarHeight;

        public EnemyWorkView() : base("Work View")
        {
        }

        #region Overrides of ViewBase

        public override void UpdateView(Rect editorRect, Rect percentageRect, Event e, NodeGraph graph, bool playMode = false)
        {
            base.UpdateView(editorRect, percentageRect, e, graph, playMode);

            //var newArea = EditorZoomArea.Begin(Windows.EnemyEditorWindow.Instance.Zoom, ViewRect);
            //{
            GUILayout.BeginArea(ViewRect);
            {
                GUI.enabled = !playMode;
                if (graph != null)
                {
                    graph.UpdateGraphGUI(e, ViewRect);
                }
                GUI.enabled = true;
            }
            GUILayout.EndArea();
            //}
            //EditorZoomArea.End();

            HandleEvents(e);
        }

        private void HandleEvents(Event e)
        {
            //TODO: In the future make zoom to the mouse position
            if (_currentGraph == null)
                return;

            foreach (NodeBase node in _currentGraph.Nodes)
                if (node.NodeBoundry.Contains(e.mousePosition - new Vector2(0, toolbarHeight)))
                    return;

            if (e.type == EventType.ScrollWheel && Windows.EnemyEditorWindow.Instance != null)
            {
                Vector2 delta = e.delta;
                float zoomDelta = -delta.y / 150f;
                Windows.EnemyEditorWindow.Instance.Zoom += zoomDelta;
                return;
            }
            else if (e.type == EventType.MouseDrag && ((Event.current.button == 0 && Event.current.modifiers == EventModifiers.Alt) || Event.current.button == 2))
            {
                float x = e.delta.x / Windows.EnemyEditorWindow.Instance.Zoom;
                float y = e.delta.y / Windows.EnemyEditorWindow.Instance.Zoom;

                foreach (NodeBase node in _currentGraph.Nodes)
                {
                    node.NodeRect.x += x;
                    node.NodeRect.y += y;
                }
            }
        }

        #endregion Overrides of ViewBase
    }
}
