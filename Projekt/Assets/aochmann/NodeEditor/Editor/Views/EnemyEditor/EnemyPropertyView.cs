﻿using Assets.aochmann.NodeEditor.Scripts;
using Assets.aochmann.NodeEditor.Scripts.NodesGUI;
using Assets.Test.Editor.Utilities;
using Assets.Test.Editor.Views;
using Assets.Test.Scripts.Data;
using UnityEditor;
using UnityEngine;

namespace Assets.aochmann.NodeEditor.Editor.Views.EnemyEditor {
    class EnemyPropertyView : ViewBase {

        public EnemyPropertyView() : base("Properties View") {

            _headerStyle = EditorStyles.toolbarButton;
            _headerStyle.font = EditorStyles.boldFont;

        }

        GUIStyle _headerStyle;

        NodeType[] _leafs     = new NodeType[] {
            NodeType.Action,
            NodeType.Condition,
            NodeType.BB,
            NodeType.Log,
            NodeType.Sleep
        };
        NodeType[] _composities = new NodeType[] {
            NodeType.Root,
            NodeType.Selector,
            NodeType.Sequence,
            NodeType.MutatingSelector,
            NodeType.RandomSelector,
            NodeType.Parallel,
        };
        NodeType[] _decorators  = new NodeType[] {
            NodeType.Chance,
            NodeType.Cooldown,
            NodeType.FlipFlop,
            NodeType.If,
            NodeType.Invert,
            NodeType.Loop,
            NodeType.Once,
            NodeType.TimeLimit,
            NodeType.UntilFailure,
            NodeType.UntilSuccess,
            NodeType.WaitFor,
            NodeType.WhileBoth
        };

        public Vector2 _scrollPosition;

        public void UpdateView(Rect editorRect, Rect percentageRect, Event e, NodeGraph graph, EnemyLogicContainer container, bool playMode = false) {
            base.UpdateView(editorRect, percentageRect, e, graph, playMode);

            GUILayout.BeginArea(ViewRect);
            {
                _scrollPosition = GUILayout.BeginScrollView(_scrollPosition);
                {
                    GUILayout.BeginVertical();
                    {
                        GUILayout.Label("Leafs", _headerStyle);
                        GUILayout.BeginVertical(EditorStyles.textArea);
                        {
                            for(int i = 0; i < _leafs.Length; i++) {
                                GUILayout.BeginHorizontal();
                                {
                                    GUILayout.FlexibleSpace();
                                    if(GUILayout.Button(_leafs[i].ToString(), GUILayout.Width(120f)))
                                        CreatNode(_leafs[i], graph, container, NodeType.Null, playMode);
                                    GUILayout.FlexibleSpace();
                                }
                                GUILayout.EndHorizontal();
                            }
                        }
                        GUILayout.EndVertical();

                        GUILayout.Space(5f);

                        GUILayout.Label("Composities", EditorStyles.toolbarButton);
                        GUILayout.BeginVertical(EditorStyles.textArea);
                        {
                            for(int i = 0; i < _composities.Length; i++) {
                                GUILayout.BeginHorizontal();
                                {
                                    GUILayout.FlexibleSpace();
                                    if(GUILayout.Button(_composities[i].ToString(), GUILayout.Width(120f)))
                                        CreatNode(_composities[i], graph, container, NodeType.Branch, playMode);
                                    GUILayout.FlexibleSpace();
                                }
                                GUILayout.EndHorizontal();
                            }
                        }
                        GUILayout.EndVertical();

                        GUILayout.Space(5f);

                        GUILayout.Label("Decorators", EditorStyles.toolbarButton);
                        GUILayout.BeginVertical(EditorStyles.textArea);
                        {
                            for(int i = 0; i < _decorators.Length; i++) {
                                GUILayout.BeginHorizontal();
                                {
                                    GUILayout.FlexibleSpace();
                                    if(GUILayout.Button(_decorators[i].ToString(), GUILayout.Width(120f)))
                                        CreatNode(_decorators[i], graph, container, NodeType.Decorator, playMode);
                                    GUILayout.FlexibleSpace();
                                }
                                GUILayout.EndHorizontal();
                            }
                        }
                        GUILayout.EndVertical();
                    }
                    GUILayout.EndVertical();
                }

                GUILayout.EndScrollView();
            }
            GUILayout.EndArea();

        }

        private void CreatNode(NodeType guiType, NodeGraph graph, EnemyLogicContainer container, NodeType baseType = NodeType.Null, bool playMode = false) {
            if(!playMode)
                NodeUtilities.CreatNode2(container, graph, guiType, baseType);
        }
    }
}
