﻿using Assets.Test.Editor.Views;
using Assets.Test.Scripts.Data;
using UnityEngine;

namespace Assets.aochmann.NodeEditor.Editor.Views.EnemyEditor {
    class EnemyToolbarView : ViewBase {
        public EnemyToolbarView() : base("Toolbar View") {
        }

        private const float HeightOffset = 10f;
        #region Overrides of ViewBase

        public override void UpdateView(Rect editorRect, Rect percentageRect, Event e, NodeGraph graph, bool playMode = false) {
            base.UpdateView(editorRect, percentageRect, e, graph, playMode);

            ProcessEvents(e);
            GUI.enabled = !playMode;
            GUILayout.BeginArea(ViewRect);
            {
                //GUILayout.BeginVertical();
                //{
                //    GUILayout.FlexibleSpace();
                //    GUILayout.BeginHorizontal();
                //    {
                //        GUILayout.FlexibleSpace();
                //        if(GUILayout.Button("Clear Stage")) {

                //        }

                //        if(GUILayout.Button("Creat Next Stage")) {

                //        }
                //        GUILayout.FlexibleSpace();
                //    }
                //    GUILayout.EndHorizontal();
                //    GUILayout.FlexibleSpace();
                //}
                //GUILayout.EndVertical();
            }
            GUILayout.EndArea();
            GUI.enabled = true;

        }

        public override void ProcessEvents(Event e) {
            base.ProcessEvents(e);
        }

        #endregion Overrides of ViewBase
    }
}