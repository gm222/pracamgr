﻿using Assets.Test.Editor.Views;
using Assets.Test.Scripts.Data;
using UnityEngine;

namespace Assets.aochmann.NodeEditor.Editor.Views.EnemyEditor {
    public class EnemyStatusView : ViewBase {
        public EnemyStatusView() : base("Status View") {
        }

        #region Overrides of ViewBase

        public override void UpdateView(Rect editorRect, Rect percentageRect, Event e, NodeGraph graph, bool playMode = false) {
            base.UpdateView(editorRect, percentageRect, e, graph);


        }

        public override void ProcessEvents(Event e) {
            base.ProcessEvents(e);
        }

        #endregion Overrides of ViewBase

        private void PrecessContextMenu(Event e) {
        }

        private void ContextCalback(object obj) {
        }
    }
}