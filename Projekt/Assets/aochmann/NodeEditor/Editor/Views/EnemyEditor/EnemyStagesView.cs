﻿using Assets.aochmann.NodeEditor.Scripts;
using Assets.Test.Editor.Utilities;
using Assets.Test.Editor.Views;
using Assets.Test.Scripts.Data;
using UnityEditor;
using UnityEngine;

namespace Assets.aochmann.NodeEditor.Editor.Views.EnemyEditor
{
    class EnemyStagesView : ViewBase
    {

        public EnemyStagesView() : base("Methods View")
        {
        }

        public Vector2 _scrollPosition;

        public void UpdateView(Rect editorRect, Rect percentageRect, Event e, NodeGraph graph, EnemyLogicContainer _nodeContainer, bool playMode = false)
        {
            base.UpdateView(editorRect, percentageRect, e, graph);

            //EnemyDBLogic logic = null;

            //if(_nodeContainer != null)
            //    logic = _nodeContainer.Logic;


            GUILayout.BeginArea(ViewRect);
            {
                GUILayout.BeginVertical();
                {
                    GUILayout.Label("Stages", EditorStyles.toolbarButton);

                    GUILayout.Space(60f);

                    if (_nodeContainer != null)
                    {

                        GUILayout.BeginHorizontal();
                        {

                            if (GUILayout.Button("Add Scenario") && _nodeContainer != null && !playMode)
                            {
                                NodeUtilities.AddGraphToLogic(_nodeContainer);
                                if (GUI.changed)
                                    EditorUtility.SetDirty(this);
                            }

                        }
                        GUILayout.EndHorizontal();

                        _scrollPosition = GUILayout.BeginScrollView(_scrollPosition);
                        {
                            GUILayout.BeginVertical();
                            {
                                for (int i = 0; i<_nodeContainer.EnemyLogics.Count; i++)
                                {
                                    GUILayout.BeginHorizontal();
                                    {
                                        GUIStyle style = EditorStyles.label;
                                        if (i == _nodeContainer.SelectedIndex)
                                            style = EditorStyles.boldLabel;

                                        if (GUILayout.Button(string.Format("Scenario {0}", i), style, GUILayout.Width(90f)))
                                            _nodeContainer.SelectedIndex = i;
                                        if (GUILayout.Button("X", GUILayout.Width(25f)) && !playMode)
                                        {
                                            NodeUtilities.RemoveGraphFromLogic(_nodeContainer, i);
                                            i--;
                                            GUILayout.EndHorizontal();
                                            continue;
                                        }
                                    }
                                    GUILayout.EndHorizontal();
                                }
                            }
                            GUILayout.EndVertical();
                        }

                        GUILayout.EndScrollView();

                    }
                }
                GUILayout.EndVertical();
            }
            GUILayout.EndArea();

        }
    }
}
