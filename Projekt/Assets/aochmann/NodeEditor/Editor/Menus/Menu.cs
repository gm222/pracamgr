﻿using Assets.aochmann.NodeEditor.Editor.Windows;
using Assets.Test.Editor.Windows;
using UnityEditor;

public partial class Menu {

    [MenuItem("Master Thesis/Behavior Editor %t")]
    public static void InitNodeEditor() {
        NodeEditorWindow.Init();
    }

    [MenuItem("Master Thesis/Enemy Node Editor %e")]
    public static void InitEnemyNodeEditor() {
        EnemyEditorWindow.Init();
    }
}