﻿using Assets.aochmann.NodeEditor.Scripts.NodesGUI;
using Assets.Test.Scripts.Data;
using System;
using Assets.aochmann.NodeEditor.Scripts;
using System.Linq;

#if UNITY_EDITOR

using UnityEditor;

#endif

using UnityEngine;

namespace Assets.Test.Editor.Utilities
{

    public static class NodeUtilities
    {
        private const string DATABASE_PATH = @"Assets/aochmann/NodeEditor/Resources/Database";

#if UNITY_EDITOR

        public static NodeGraph CreatNodeGraph()
        {
            string graphName = "Graph" + DateTime.Now.ToString("HHmmssfff");

            NodeGraph currentGraph = new NodeGraph(graphName);
            if (currentGraph != null)
            {
                currentGraph.Initialize();
                AssetDatabase.CreateAsset(currentGraph, DATABASE_PATH + "/" + graphName + ".asset");
                Save();

                return currentGraph;
            }

            return null;
        }

        private static bool CreatEnemyStructure(EnemyLogicContainer container)
        {
            AssetDatabase.CreateFolder(DATABASE_PATH, container.FileName);
            //AssetDatabase.CreateAsset(container.EnemyGraph, string.Format(@"{0}\{1}.asset", DATABASE_PATH, container.EnemyGraph.FileName));
            AddGraphToLogic(container);
            Save();
            return true;
        }

        public static void AddGraphToLogic(EnemyLogicContainer logic)
        {
            NodeGraph graph = new NodeGraph(DateTime.Now.ToString("HHmmssfff"));
            logic.EnemyLogics.Add(graph);
            logic.SelectedIndex = logic.EnemyLogics.Count - 1;
            AssetDatabase.CreateAsset(graph, string.Format(@"{0}\{1}\{2}{3}.asset", DATABASE_PATH, logic.FileName, logic.FileName, graph.GraphName));
            AssetDatabase.SaveAssets();
        }

        public static void LoadGraph(ref NodeGraph currentGraph)
        {

            string path = Application.dataPath + @"/aochmann/NodeEditor/Resources/Database/" + currentGraph.GraphName + ".asset";
            if (string.IsNullOrEmpty(path))
                return;
            int appPathLen = Application.dataPath.Length;
            string finalPath = path.Substring(appPathLen - 6);
            AssetDatabase.Refresh();

            NodeGraph currentGraphTMP = AssetDatabase.LoadAssetAtPath<NodeGraph>(finalPath);

            currentGraph = currentGraphTMP;
            if (currentGraph == null)
                ErrorMessage("The selected asset !");
        }

        public static void RemoveGraphFromLogic(EnemyLogicContainer logic, int i)
        {
            NodeGraph name = logic.EnemyLogics[i];
            string path = AssetDatabase.GetAssetPath(name);
            logic.EnemyLogics.RemoveAt(i);
            logic.SelectedIndex = -1;
            AssetDatabase.DeleteAsset(path);
            NodeUtilities.Save();
        }

        public static bool LoadGraph(ref EnemyLogicContainer container)
        {
            bool result = false;

            if (container != null)
            {
                if (container.FileName.Length == 0)
                {
                    container.FileName = "Enemy" + DateTime.Now.ToString("HHmmssfff");
                }

                string path = string.Format(@"{0}\{1}", DATABASE_PATH, container.FileName);
                NodeGraph[] tmp = LoadAllAssetsAtPath(path);
                if (tmp == null || tmp.Length == 0)
                {
                    container.EnemyLogics.Clear();
                    result = CreatEnemyStructure(container);
                }
                else
                {
                    container.EnemyLogics = new System.Collections.Generic.List<NodeGraph>(tmp);
                    result = true;
                }
            }



            return result;



            //if(container == null || container.LogicFileName.Length == 0) {
            //    if(container != null)
            //        container.EnemyLogics = null;
            //    result = CreatEnemyStructure(container);
            //} else {
            //    string path = string.Format(@"{0}\{1}", DATABASE_PATH, container.LogicFileName);
            //    var tmp = LoadAllAssetsAtPath(path);
            //    if(tmp == null || tmp.Length == 0) {
            //        container.EnemyLogics = null;
            //        result = CreatEnemyStructure(container);
            //    } else {
            //        container.EnemyLogics = new System.Collections.Generic.List<NodeGraph>(tmp);
            //        result = true;
            //    }
            //}
            //return result;
        }

        public static NodeGraph[] LoadAllAssetsAtPath(string path)
        {
            if (path.EndsWith("/"))
            {
                path = path.TrimEnd('/');
            }
            string[] GUIDs = AssetDatabase.FindAssets("", new string[] { path }).Distinct<string>().ToArray();
            NodeGraph[] objectList = new NodeGraph[GUIDs.Length];
            for (int index = 0; index < GUIDs.Length; index++)
            {
                string guid = GUIDs[index];
                string assetPath = AssetDatabase.GUIDToAssetPath(guid);
                NodeGraph asset = AssetDatabase.LoadAssetAtPath<NodeGraph>(assetPath);
                objectList[index] = asset;
            }

            return objectList;
        }

        public static void DeleteGraph(string graphName)
        {
            if (EditorUtility.DisplayDialog("Delete Graph", "Are you sure you want to remove current graph ?", "Yes", "No"))
            {
                AssetDatabase.DeleteAsset(@"Assets/aochmann/NodeEditor/Resources/Database/" + graphName + ".asset");
                Save();
            }
        }

        public static void CreatNode(NodeGraph graph, NodeType nodeType, NodeType baseType = NodeType.Null)
        {
            if (graph == null || nodeType == NodeType.Null)
                return;

            NodeBase currentNode = ScriptableObject.CreateInstance(nodeType.ToString() +"GUI") as NodeBase;

            if (currentNode == null)
            {
                if (baseType != NodeType.Null)
                    currentNode = ScriptableObject.CreateInstance(baseType.ToString() + "GUI") as NodeBase;

                if (currentNode == null)
                {
                    Debug.LogError(string.Format("Can not create instance of type {0}", nodeType));
                    return;
                }
            }

            currentNode.Initialize();
            currentNode.NodeType = nodeType;

            currentNode.name = currentNode.Name = nodeType.ToString();

            currentNode.ParentGraph = graph;
            graph.Nodes.Add(currentNode);

            if (graph.SelectedNode != null)
                graph.SelectedNode.IsSelected = false;

            currentNode.IsSelected = true;
            graph.SelectedNode = currentNode;

            AssetDatabase.AddObjectToAsset(currentNode, graph);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

#endif

        private static void ErrorMessage(string body)
        {
            EditorUtility.DisplayDialog("Error", body, "OK");
        }

        public static void Save()
        {
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
        }


        public static void CreatNode2(EnemyLogicContainer container, NodeGraph graph, NodeType nodeType, NodeType baseType)
        {
            if (graph == null && nodeType == NodeType.Null || container == null)
                return;

            NodeBase currentNode = ScriptableObject.CreateInstance(nodeType.ToString() +"GUI") as NodeBase;

            if (currentNode == null)
            {
                if (baseType != NodeType.Null)
                    currentNode = ScriptableObject.CreateInstance(baseType.ToString() + "GUI") as NodeBase;

                if (currentNode == null)
                {
                    Debug.LogError(string.Format("Can not create instance of type {0}", nodeType));
                    return;
                }
            }

            currentNode.Initialize();
            currentNode.NodeType = nodeType;

            currentNode.name = currentNode.Name = nodeType.ToString();

            currentNode.ParentGraph = graph;
            graph.Nodes.Add(currentNode);

            if (graph.SelectedNode != null)
                graph.SelectedNode.IsSelected = false;

            currentNode.IsSelected = true;
            graph.SelectedNode = currentNode;


            AssetDatabase.AddObjectToAsset(currentNode, graph);
            Save();
        }
    }
}