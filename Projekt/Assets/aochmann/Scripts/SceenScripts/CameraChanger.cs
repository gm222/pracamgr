﻿using UnityEngine;

public class CameraChanger : MonoBehaviour {

    private int _currentCameraIndex = 0;

    private Camera[] _cameras;
    public void Start() {
        _cameras = Camera.allCameras;

        for(int i = 0; i < _cameras.Length; i++)
            _cameras[i].enabled = false;

        _cameras[_currentCameraIndex].enabled = true;
    }



    // Update is called once per frame
    void Update() {
        if(Input.GetKeyDown(KeyCode.Keypad5)) {
            _cameras[_currentCameraIndex].enabled = false;

            _currentCameraIndex++;

            if(_currentCameraIndex >= _cameras.Length) _currentCameraIndex = 0;

            _cameras[_currentCameraIndex].enabled = true;
        }

    }
}
