﻿using System.Collections;
using UnityEngine;

public class DoorsAction : MonoBehaviour {
    private bool _isDoorOpen = false;

    [SerializeField, Range(0.1f, 10f)]
    private float _speed = 3f;

    [SerializeField]
    private Transform _openPoint;

    [SerializeField]
    private Transform _closePoint;


    public IEnumerator OpenDoor() {
        if(!_isDoorOpen) {
            yield return GoToPoint(_openPoint.position);
            _isDoorOpen = !_isDoorOpen;
        }
    }

    public IEnumerator CloseDoor() {
        if(_isDoorOpen) {
            yield return GoToPoint(_closePoint.position);
            _isDoorOpen = !_isDoorOpen;
        }
    }

    void Update() {
        if(Input.GetKeyDown(KeyCode.Space)) {
            StartCoroutine(OpenDoor());
        }
        if(Input.GetKeyDown(KeyCode.KeypadEnter)) {
            StartCoroutine(CloseDoor());
        }
    }

    IEnumerator GoToPoint(Vector3 point) {
        float distance = 0f;

        do {
            Vector3 characterPosition = transform.position;
            Vector3 position          = point - characterPosition;
            Vector3 direction         = position.normalized;

            direction.y = 0f;
            position.y  = 0;

            distance = position.magnitude;
            transform.position = Vector3.Lerp(characterPosition, characterPosition + direction, Time.fixedDeltaTime * _speed);

            yield return null;
        }
        while(distance >= 0.08f);
    }

}
