﻿using Assets.aochmann.NodeEditor.Scripts;
using Assets.aochmann.NodeEditor.Scripts.NodesUtilities;
using Assets.aochmann.Scripts.Other;
using Assets.aochmann.Scripts.Utilities;
using Assets.Test.Scripts.Data;
using BAD;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class GameManager : MonoSingleton<GameManager>
{
    #region Serialized Fields
    [SerializeField]
    [Range(0f, 100f)]
    private float _tickDuration = 0.1f;


    [SerializeField, Range(1, 100)]
    private int _enemiesCount;

    [SerializeField, Range(1, 100)]
    private int _maxSimulationTime = 30;

    [SerializeField, Range(0, 10)]
    private int _bunkerAmmunition = 2;

    [SerializeField, Range(0, 100)]
    private int _minigunAmmuniton = 40;

    [SerializeField]
    private bool _minigunEnabled = false;

    [SerializeField]
    private Transform _door;

    [SerializeField]
    private Transform _dorsPointOpen;

    [SerializeField]
    private Transform _dorsPointClose;

    [SerializeField]
    private Transform _insideDoor;

    [SerializeField]
    private Transform _outsideDoor;


    [SerializeField, Range(0f, 50f)]
    private float _doorsSpeed = 10f;

    [SerializeField]
    private GameObject _bullet;

    [SerializeField]
    private bool _doorsClosed = true;

    [SerializeField, Range(0, 100000)]
    private int _SimulationCount = 200;

    [SerializeField]
    private Transform _AIRespawnPoint;

    [SerializeField]
    private GameObject _AIObject;

    [SerializeField]
    private GameObject _enemiesPrefab;

    [SerializeField]
    private Transform _enemiesRespawnPoint;

    #endregion
    #region Non Serialized Fields
    private int _currentBunkerAmmunition;
    private bool _isBreakSet = false;
    private List<GameObject> _enemiesList = new List<GameObject>();
    private int _currentMinigunAmmuniton;

    #endregion

    #region Properties
    public bool IsBreak
    {
        get { return _isBreakSet; }
        set { _isBreakSet = value; }
    }

    public float TickDuration
    {
        get { return _tickDuration; }
    }

    public List<GameObject> EnemiesList
    {
        get { return _enemiesList; }
    }

    public int BunkerAmmunition
    {
        get { return _bunkerAmmunition; }
    }

    public int CurrentBunkerAmmunition
    {
        get { return _currentBunkerAmmunition; }
        set { _currentBunkerAmmunition=value; }
    }

    public int CurrentMinigunAmmuniton
    {
        get { return _currentMinigunAmmuniton; }
        set { _currentMinigunAmmuniton=value; }
    }

    public int MinigunAmmuniton
    {
        get { return _minigunAmmuniton; }
    }

    public bool MinigunEnabled
    {
        get { return _minigunEnabled; }
        set { _minigunEnabled=value; }
    }

    public GameObject Bullet
    {
        get { return _bullet; }
    }

    public Transform Door
    {
        get { return _door; }
    }

    public bool DoorsClosed
    {
        get { return _doorsClosed; }
        set { _doorsClosed=value; }
    }

    public Transform InsideDoor
    {
        get { return _insideDoor; }
    }

    public Transform OutsideDoor
    {
        get { return _outsideDoor; }
    }

    public GameObject[] Enemies
    {
        get { return _enemies; }
    }

    #endregion
    #region Private Methods
    private void OnValidate()
    {
        //Time.fixedDeltaTime = 0.02f * Time.timeScale;
        Time.timeScale      = _tickDuration;
    }

    IEnumerator GoToPoint(Vector3 point)
    {
        float distance = 0f;

        do
        {
            Vector3 characterPosition = _door.position;
            Vector3 position = point - characterPosition;
            Vector3 direction = position.normalized;

            direction.y = 0f;
            position.y  = 0;

            distance = position.magnitude;
            _door.position = Vector3.Lerp(characterPosition, characterPosition + direction, Time.fixedDeltaTime * _doorsSpeed);

            yield return null;
        }
        while (distance >= 0.1f);
    }

    private void Start()
    {
        CurrentBunkerAmmunition = BunkerAmmunition;
        _currentMinigunAmmuniton = MinigunAmmuniton;


        StartCoroutine(GameLogicManager());
    }

    enum Simulation
    {
        Playing,
        Stopped,
        Initialize
    }

    public int CurrentSimulation = 0;

    GameObject _aiObject;
    BAD.BADReactor _aiBTReactor;
    AIControllerSetup _aiControllerSetup;
    GameObject[] _enemies;
    BADReactor[] _enemiesReactor;
    AIControllerSetup[] _enemiesControllerSetup;

    private int _enemiesCountInitialized = 0;
    private int _enemiesFinished = 0;


    private int GetNodesCount(Node node)
    {
        int nodesCount = 0;
        Queue<Node> Q = new Queue<Node>();
        Q.Enqueue(node);

        while (Q.Count > 0)
        {
            nodesCount++;
            Branch branch = Q.Dequeue() as Branch;
            if (branch != null)
                foreach (Node child in branch.children)
                    Q.Enqueue(child);
        }

        return nodesCount;
    }

    private void SetNodeArrayStructure(Node[] array, Node node)
    {
        array[node.ID] = node;
        Branch branch = node as Branch;
        if (branch != null)
            foreach (Node child in branch.children)
                SetNodeArrayStructure(array, child);
    }

    private void SetNodeTypesArray(int[] array, Node node)
    {
        array[node.ID] = node.NodeType;
        Branch branch = node as Branch;

        if (branch != null)
            foreach (Node child in branch.children)
                SetNodeTypesArray(array, child);
    }

    private void SetBTStructure(object[] aiStructure, Node node)
    {
        object[] list = new object[2];
        int parrent = node.parent == null ? node.ID : node.parent.ID;
        List<int> childrenNodeList = new List<int>();

        list[0] = parrent;
        list[1] = childrenNodeList;

        aiStructure[node.ID] = list;

        Branch branch = node as Branch;
        if (branch != null)
            foreach (Node child in branch.children)
            {
                childrenNodeList.Add(child.ID);
                SetBTStructure(aiStructure, child);
            }
    }

    System.Diagnostics.Process mutationProgram = null;

    private IEnumerator GameLogicManager()
    {
        //first init
        string x = Application.dataPath;
        mutationProgram = System.Diagnostics.Process.Start(@"E:\Adi Repositories\PracaMGR\PracaMGR2\BTree\BTree\bin\Debug\BTree.exe");
        //
        long simulaionCount = 0;
        _aiObject                 = _AIObject; //GameObject.Instantiate<GameObject>(_AIObject);
        _aiBTReactor              = _aiObject.GetComponent<BAD.BADReactor>();
        _aiControllerSetup        = _aiObject.GetComponent<AIControllerSetup>();
        NodesContainer aiNC = _aiObject.GetComponent<NodesContainer>();

        _enemiesCountInitialized  = _enemiesCount;
        Simulation state = Simulation.Initialize;
        CurrentSimulation         = 0;
        float startTime = Time.realtimeSinceStartup;
        float simulationStartTime = Time.realtimeSinceStartup;

        _enemies                  = new GameObject[_enemiesCount];
        _enemiesReactor           = new BADReactor[_enemiesCount];
        _enemiesControllerSetup   = new AIControllerSetup[_enemiesCount];
        Node[] enemiesStageNodes = new Node[_enemiesCount];

        aiNC.Initialize();

        for (int i = 0; i < _enemiesCountInitialized; i++)
        {
            Enemies[i]                 = UnityEngine.Object.Instantiate<GameObject>(_enemiesPrefab);
            EnemyLogicContainer logic = Enemies[i].GetComponent<EnemyLogicContainer>();
            _enemiesReactor[i]         = Enemies[i].GetComponent<BADReactor>();
            _enemiesControllerSetup[i] = Enemies[i].GetComponent<AIControllerSetup>();
            AIPointData test = Enemies[i].GetComponent<AIPointData>();

            NodesUtilitiesRuntime.LoadGraph(ref logic);

            NodeGraph guiGraph = logic.EnemyLogics[i%logic.EnemyLogics.Count];
            Node root = logic.Parser(new Root(), guiGraph.StartupNode, _enemiesReactor[i]);
            enemiesStageNodes[i] = root;

            _enemiesReactor[i].ClearRuningGraphs();
            _enemiesReactor[i].runningGraphs.Add(enemiesStageNodes[i]);
        }

        Node aiNodeRoot = _aiBTReactor.runningGraphs[0];
        int aiNodesCount = GetNodesCount(_aiBTReactor.runningGraphs[0]);

        //inicialization pipe connection

        Node[] aiNodeMapStructure = null;

        bool firstInitialization = true;
        using (NamedPipeClientStream _namedPipeClient = new NamedPipeClientStream(".", "test-pipe", PipeDirection.InOut))
        {
            while (!_namedPipeClient.IsConnected)
            {
                _namedPipeClient.Connect(60);
                yield return null;
            }
            Debug.Log("Client Connected");
            _namedPipeClient.ReadMode = PipeTransmissionMode.Message;
            yield return null;

            if (firstInitialization)
            {
                int[] aiNodeTypes = new int[aiNodesCount];
                SetNodeTypesArray(aiNodeTypes, aiNodeRoot);

                object[] aiStructure = new object[aiNodesCount];

                SetBTStructure(aiStructure, aiNodeRoot);

                aiNodeMapStructure = new Node[aiNodesCount];
                SetNodeArrayStructure(aiNodeMapStructure, aiNodeRoot);

                //############################# WYSYŁANIE ##############################
                if (_namedPipeClient.IsConnected)
                {
                    object sendObject = new object[] { aiNodeTypes, aiStructure };
                    HelpFunctions.PipeWrite(_namedPipeClient, sendObject);
                    firstInitialization = !firstInitialization;
                }
                //######################################################################
            }
            bool endConnection = false;
            while (_namedPipeClient.IsConnected && !endConnection)
            {
                bool canSimulate = false;
                //############################# ODBIERANIE #############################
                if (_namedPipeClient.IsConnected)
                {
                    try
                    {
                        object[] serverResponse = HelpFunctions.PipeRead(_namedPipeClient) as object[];

                        if (serverResponse == null || serverResponse[0] is bool)
                            endConnection = true;

                        object[] serverTree = serverResponse[0] as object[];

                        if (serverTree == null || serverTree.Length == 0)
                            continue;


                        //resetowanie danych rodzica
                        for (int i = 0; i < aiNodeMapStructure.Length; i++)
                            aiNodeMapStructure[i].parent = null;

                        //budowanie drzewa
                        for (int i = 0; i < serverTree.Length; i++)
                        {
                            List<int> childsID = serverTree[i] as List<int>;

                            Branch nodeBranch = aiNodeMapStructure[i] as Branch;
                            if (nodeBranch != null)
                                nodeBranch.children.Clear();

                            if (childsID.Count > 0)
                                if (nodeBranch != null)
                                    for (int j = 0; j < childsID.Count; j++)
                                    {
                                        Node tmpNode = aiNodeMapStructure[childsID[j]];
                                        tmpNode.parent = nodeBranch;
                                        nodeBranch.children.Add(tmpNode);
                                    }
                        }

                        canSimulate = true;
                    }
                    catch (Exception) { canSimulate = false; }
                }
                //######################################################################
                float cost = float.MaxValue;
                yield return null;
                if (!canSimulate) continue;
                if (canSimulate)
                {
                    CurrentSimulation++;
                    Debug.Log("Simulation " + CurrentSimulation.ToString());
                    IsBreak          = false;
                    _enemiesFinished = 0;

                    SetPosition(_aiObject, _AIRespawnPoint);
                    ResetGMFields();
                    _aiControllerSetup.ResetData();


                    Node aiNode = aiNodeMapStructure[0];

                    for (int i = 0; i < aiNodeMapStructure.Length; i++)
                    {
                        aiNodeMapStructure[i].running = false;
                        aiNodeMapStructure[i].state = null;
                    }

                    _aiBTReactor.ClearRuningGraphs();
                    _aiBTReactor.runningGraphs.Add(aiNode);

                    for (int i = 0; i < _enemiesCountInitialized; i++)
                    {
                        _enemiesReactor[i].ClearRuningGraphs();
                        _enemiesReactor[i].runningGraphs.Add(enemiesStageNodes[i]);
                    }

                    _aiBTReactor.StartLogic();

                    for (int i = 0; i < _enemiesCountInitialized; i++)
                    {
                        _enemiesControllerSetup[i].ResetData();
                        SetPosition(Enemies[i], _enemiesRespawnPoint);
                        Enemies[i].SetActive(true);
                        //Enemies[i].transform.position += new Vector3(i*2, 0, 0);
                        _enemies[i].transform.position += new Vector3(UnityEngine.Random.Range(-10, 10)+i*0.5f, 0, 0);
                        _enemiesReactor[i].Reset(_enemiesReactor[i].runningGraphs[0]);
                        _enemiesReactor[i].StartLogic();
                    }

                    simulationStartTime = Time.realtimeSinceStartup;

                    float simulationStart = Time.realtimeSinceStartup;

                    while (!
                        (IsBreak
                        ||
                        _enemiesFinished >= _enemiesCountInitialized
                        ||
                        (Time.realtimeSinceStartup - simulationStart > ((0.1f * _maxSimulationTime)/_tickDuration))))
                    {
                        yield return null;
                    }

                    //yield return new WaitUntil(() => );

                    IsBreak = true;

                    float stopTime = Time.realtimeSinceStartup;
                    float diff = stopTime - simulationStartTime;

                    StopObjectAction(_aiObject);

                    cost        = CalcCost(simulationStartTime, stopTime, _aiObject, Enemies);
                }

                //############################# WYSYŁANIE ##############################
                if (_namedPipeClient.IsConnected)
                {
                    object sendObject = new object[] { cost };
                    HelpFunctions.PipeWrite(_namedPipeClient, sendObject);
                }
                //######################################################################
            }
            yield return null;
        }
        yield return new WaitForSeconds(5);
        using (NamedPipeClientStream _namedPipeClient = new NamedPipeClientStream(".", "test-pipe", PipeDirection.InOut))
        {
            _namedPipeClient.Connect();
            _namedPipeClient.ReadMode = PipeTransmissionMode.Message;
            yield return null;

            if (_namedPipeClient.IsConnected)
            {
                try
                {
                    object[] serverResponse = HelpFunctions.PipeRead(_namedPipeClient) as object[];
                    object[] serverTree = serverResponse[0] as object[];

                    //resetowanie danych rodzica
                    for (int i = 0; i < aiNodeMapStructure.Length; i++)
                        aiNodeMapStructure[i].parent = null;

                    //budowanie drzewa
                    for (int i = 0; i < serverTree.Length; i++)
                    {
                        List<int> childsID = serverTree[i] as List<int>;

                        Branch nodeBranch = aiNodeMapStructure[i] as Branch;
                        if (nodeBranch != null)
                            nodeBranch.children.Clear();

                        if (childsID.Count > 0)
                        {

                            if (nodeBranch != null)
                                for (int j = 0; j < childsID.Count; j++)
                                {
                                    Node tmpNode = aiNodeMapStructure[childsID[j]];
                                    tmpNode.parent = nodeBranch;
                                    nodeBranch.children.Add(tmpNode);
                                }
                        }

                    }
                }
                catch (Exception) { }
            }
            //######################################################################

            while (true)
            {
                switch (state)
                {
                    case Simulation.Playing:
                        if (IsBreak || _enemiesFinished >= _enemiesCountInitialized)
                        {
                            state = Simulation.Stopped;
                            yield return null;
                        }
                        break;
                    case Simulation.Stopped:
                        {
                            float stopTime = Time.realtimeSinceStartup;
                            float diff = stopTime - simulationStartTime;

                            StopObjectAction(_aiObject);
                            float cost = CalcCost(simulationStartTime, stopTime, _aiObject, Enemies);
                            state = Simulation.Initialize;
                            yield return new WaitForSeconds(0.5f);
                            Debug.Log("Next ^^");
                        }
                        break;
                    case Simulation.Initialize:
                        {
                            IsBreak          = false;
                            _enemiesFinished = 0;

                            SetPosition(_aiObject, _AIRespawnPoint);
                            ResetGMFields();
                            _aiControllerSetup.ResetData();


                            Node tmpAINode = aiNodeMapStructure[0];

                            for (int i = 0; i < aiNodeMapStructure.Length; i++)
                            {
                                aiNodeMapStructure[i].running = false;
                                aiNodeMapStructure[i].state = null;
                            }

                            _aiBTReactor.ClearRuningGraphs();
                            _aiBTReactor.runningGraphs.Add(tmpAINode);

                            for (int i = 0; i < _enemiesCountInitialized; i++)
                            {
                                _enemiesReactor[i].ClearRuningGraphs();
                                _enemiesReactor[i].runningGraphs.Add(enemiesStageNodes[i]);
                            }
                            _aiBTReactor.StartLogic();
                            for (int i = 0; i < _enemiesCountInitialized; i++)
                            {
                                _enemiesControllerSetup[i].ResetData();
                                Enemies[i].SetActive(true);
                                SetPosition(Enemies[i], _enemiesRespawnPoint);
                                Enemies[i].transform.position += new Vector3(i*2, 0, i*3);
                                _enemiesReactor[i].Reset(_enemiesReactor[i].runningGraphs[0]);
                                _enemiesReactor[i].StartLogic();
                            }

                            simulationStartTime = Time.realtimeSinceStartup;
                            state = Simulation.Playing;

                        }

                        break;
                    default:
                        break;
                }

                yield return null; //stop one frame
            }
        }
        Debug.Log("Simulation Ended");


        ////////////////////////

        EditorUtility.DisplayDialog("Question", "Simulation ended", "OK");
    }

    private float CalcCost(float startTime, float stopTime, GameObject aiObject, GameObject[] enemies)
    {
        Vector3 aiPosition = aiObject.transform.position;
        float distance = 22222222222f;


        Transform[] enemyPositions = _shotedOffEnemiesList.Where(enemy => enemy.activeSelf).Select(enemy => enemy.transform).ToArray();

        if (enemyPositions == null || enemyPositions.Length == 0)
            enemyPositions = enemies.Select(enemy => enemy.transform).ToArray();

        if (enemyPositions == null || enemyPositions.Length == 0) { }
        else
        {
            Vector3 nearestEnemyPosition = ScriptsHelper.SelectNearestPoint(aiObject.transform.position, enemyPositions);
            distance                     = Vector3.Distance(aiPosition, nearestEnemyPosition);
        }

        float amunitionData = _aiControllerSetup.CurrentAmmunitionCount * _aiControllerSetup.CurrentAmunitionWithMe;
        int enemyDiedCount = enemies.Select(enemy => enemy.activeInHierarchy).Count();
        if (enemyDiedCount == 0) enemyDiedCount = 1;
        float playTime = stopTime - startTime;

        return ((amunitionData / distance) / enemyDiedCount) / (playTime * 60);
    }


    private void SetPosition(GameObject obj, Transform wantedPosition)
    {
        obj.transform.position = wantedPosition.position;
    }



    private void ResetGMFields()
    {
        _currentBunkerAmmunition = _bunkerAmmunition;
        _currentMinigunAmmuniton = _minigunAmmuniton;
        _minigunEnabled          = false;
        _isBreakSet              = false;
        _doorsClosed             = true;
        _shotedOffEnemiesList.Clear();
    }

    private void OnApplicationQuit()
    {
        //try {
        //    _namedPipeClient.Flush();
        //    _namedPipeClient.Close();
        //} catch(Exception) { }
        if (mutationProgram != null)
        {
            //mutationProgram.Close();
            mutationProgram.CloseMainWindow();
        }
    }

    #endregion

    #region Public Methods

    List<GameObject> _shotedOffEnemiesList = new List<GameObject>();

    public void StopObjectAction(GameObject obj, bool wasShuttedDown = false)
    {
        if (obj == _aiObject)
        {
            IsBreak = true;
            _aiBTReactor.StopLocal();

            for (int i = 0; i < _enemiesCountInitialized; i++)
                _enemiesReactor[i].StopLocal();

        }
        else
        {
            int index = Array.IndexOf(Enemies, obj);
            if (index < 0) return;
            _enemiesReactor[index].StopLocal();
            if (wasShuttedDown)
                _shotedOffEnemiesList.Add(obj);

            Enemies[index].SetActive(false);
            _enemiesFinished++;
        }
    }

    public void DoorsManipulationAction(bool state)
    {
        StopAllCoroutines();
        Vector3 tmpPoint = state == true ? _dorsPointOpen.position : _dorsPointClose.position;
        StartCoroutine(GoToPoint(tmpPoint));
        DoorsClosed = !state;
    }
    #endregion

}
