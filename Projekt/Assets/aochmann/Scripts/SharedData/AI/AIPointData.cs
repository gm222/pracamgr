﻿using UnityEngine;

public class AIPointData : MonoBehaviour {
    #region Serialized Fields
    [SerializeField]
    private Transform[] _safePoints;

    [SerializeField]
    private Transform _bunkerPosition;

    [SerializeField]
    private Transform _minigunPosition;

    [SerializeField]
    private Transform _map;

    [SerializeField]
    private Transform _patrolPoints;

    #endregion
    #region Non Serialized Fields
    private float _TerrainLeft, _TerrainRight, _TerrainTop, _TerrainBottom, _TerrainWidth, _TerrainLength, _TerrainHeight;
    #endregion
    #region Properties
    public Transform[] SafePoints {
        get { return _safePoints; }
    }

    public Transform BunkerPosition {
        get { return _bunkerPosition; }
    }

    public Transform MinigunPosition {
        get { return _minigunPosition; }
    }

    public Transform PatrolPoints {
        get { return _patrolPoints; }
    }
    #endregion

    public void Start() {
        //_TerrainLeft = _map.position.x + 100f;
        //_TerrainBottom = _map.position.z + 120f;

        //_TerrainWidth = _map..terrainData.size.x - 100f;
        //_TerrainLength = _Terrain.terrainData.size.z - 120f;
        //_TerrainHeight = _Terrain.terrainData.size.y;

        //_TerrainRight = _TerrainLeft + _TerrainWidth;
        //_TerrainTop = _TerrainBottom + _TerrainLength;
    }
}
