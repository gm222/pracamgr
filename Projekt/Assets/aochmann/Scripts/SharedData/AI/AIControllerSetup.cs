﻿using UnityEngine;

public class AIControllerSetup : MonoBehaviour {
    #region Serialized Fields
    [SerializeField, Range(0.1f, 140f), Header("Object Speed")]
    private float _moveDamping = 3f;

    [Space(10f)]

    [SerializeField, Range(0.8f, 20f)]
    private float _distanceToObject = 4f;

    [SerializeField, Range(0f, 100f)]
    private float _maxDistance = 4f;

    [SerializeField, Range(0f, 500f)]
    private float _enemyRangeDetection = 2f;

    [SerializeField, Range(0, 360)]
    private int _enemyAngledRangeDetection = 45;

    [SerializeField, Range(0f, 500f)]
    private float _minigunRangeDetection = 15f;

    [SerializeField, Range(0, 32)]
    private int _bulletsMagazineCount = 25;

    [SerializeField, Range(0, 100)]
    private int _amunitionWithMeCount = 2;

    [SerializeField, Range(0,360)]
    private int _fieldOfViewAngle = 75;

    [SerializeField]
    private int _currentAmunitionCount = 0;
    [SerializeField]
    private int _currentAmunitionWithMe = 0;

    #endregion
    #region Non Serialized Fields

    #endregion
    #region Properties
    public float MoveDamping {
        get { return _moveDamping; }
    }

    public float DistanceToObject {
        get { return _distanceToObject; }
    }

    public float MaxDistance {
        get { return _maxDistance; }
    }

    public float EnemyRangeDetection {
        get { return _enemyRangeDetection; }
    }

    public float MinigunRangeDetection {
        get { return _minigunRangeDetection; }
    }

    public int BulletsMagazineCount {
        get { return _bulletsMagazineCount; }
    }

    public int CurrentAmmunitionCount {
        get { return _currentAmunitionCount; }
        set { _currentAmunitionCount=value; }
    }

    public int CurrentAmunitionWithMe {
        get { return _currentAmunitionWithMe; }
        set { _currentAmunitionWithMe=value; }
    }

    public int FieldOfViewAngle {
        get { return _fieldOfViewAngle; }
    }

    public int EnemyAngledRangeDetection {
        get { return _enemyAngledRangeDetection; }
    }

    #endregion

    #region Private Methods
    private void Start() {
        ResetData();
    }

    #endregion
    #region Public Methods
    public void ResetData() {
        CurrentAmmunitionCount = _bulletsMagazineCount;
        CurrentAmunitionWithMe = _amunitionWithMeCount;
    }


    #endregion


}
