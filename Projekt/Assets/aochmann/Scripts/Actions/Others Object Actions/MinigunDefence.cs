﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MinigunDefence : ActionBase {
    private Transform _bulletSpawnPoint;
    private Transform _rotatorZ;
    private Transform _head;
    private GameObject _bullet;

    #region Private Methods
    // Use this for initialization
    private void Start() {
        Transform[] childTransforms = gameObject.GetComponentsInChildren<Transform>(false);
        _bulletSpawnPoint = childTransforms.FirstOrDefault(child => child.CompareTag("Respawn") == true);
        _bullet = GameManager.Instance.Bullet;
        _rotatorZ = childTransforms.FirstOrDefault(child => child.name.Equals("RotatorZ") == true);
        _head = childTransforms.FirstOrDefault(child => child.name.Equals("Head") == true);
        bool result = CheckSetups();
    }

    private bool CheckSetups() {
        bool result = true;

        if(_bulletSpawnPoint == null)
            throw new System.NullReferenceException(string.Format("Missing bullet spawn poin."));

        if(_bullet == null)
            throw new System.NullReferenceException("Bullet prefab not initialized");

        return result;
    }


    private void Shot() {
        Instantiate(_bullet, _bulletSpawnPoint.position, _bulletSpawnPoint.rotation);
    }

    #endregion
    #region Public Methods
    public IEnumerator<BAD.NodeResult> ShotToEnemy() {
        if(!CheckSetups()) yield return BAD.NodeResult.Failure;

        //Collider enemyCollider = Physics.OverlapSphere(transform.position, 20f).FirstOrDefault(enemy => enemy.CompareTag("Enemy"));

        //if(enemyCollider == null) yield return BAD.NodeResult.Failure;

        //Vector3 enemyPosition  = enemyCollider.transform.position;
        //Vector3 headPosition   = _head.position;
        //Vector3 headForward    = _head.transform.forward;
        //Vector3 bulletPosition = _bulletSpawnPoint.transform.position;
        //Vector3 bulletForward  = _bulletSpawnPoint.transform.forward;


        //Vector3 characterDirectionToTarget = enemyPosition - headPosition;
        //characterDirectionToTarget.y = 0f;
        //characterDirectionToTarget = characterDirectionToTarget.normalized;

        //float angleY = Vector3.Angle (characterDirectionToTarget, headForward);

        //Vector3 weaponDirectionToTarget = enemyPosition - bulletPosition;
        //weaponDirectionToTarget.z = 0f;
        //weaponDirectionToTarget = weaponDirectionToTarget.normalized;
        //weaponDirectionToTarget.z = 0f;

        //float angleZ = Vector3.Angle (weaponDirectionToTarget, bulletForward);
        //var t = new Vector3(0f, 180f - angleY, 0f);
        //transform.rotation = Quaternion.Euler(t);
        ////_rotatorZ.rotation = Quaternion.Euler(0f, 0f, angleZ);

        ////_rotatorZ.Rotate(0f, 0f, angleZ);




        ////_sentryGunPitch.rotation = Quaternion.AngleAxis(angle, Vector3.up);



        ////Vector3 myPosition = _rotatorZ.position;
        ////Vector3 rayDirection = _sentryGunPitch.forward;
        ////int rayLengthMeters=3;
        ////RaycastHit hitInfo;

        ////if(Physics.Raycast(myPosition, rayDirection, out hitInfo, rayLengthMeters))
        ////    if(hitInfo.collider.gameObject.tag=="Walls")
        ////        yield return BAD.NodeResult.Failure;

        ////Shot();
        ////GameManager.Instance.CurrentMinigunAmmuniton--;
        yield return BAD.NodeResult.Success;
    }

    #endregion

}
