﻿using System;
using System.Collections.Generic;

public class AIAnotherActions : ActionBase {

    public IEnumerator<BAD.NodeResult> OpenDoors() {
        if(GameManager.Instance == null || !GameManager.Instance.DoorsClosed || GameManager.Instance.Door == null) yield return BAD.NodeResult.Failure;

        DateTime dt = DateTime.Now + TimeSpan.FromSeconds(4f);
        GameManager.Instance.DoorsManipulationAction(true);
        do { yield return BAD.NodeResult.Continue; } while(DateTime.Now < dt);
        yield return BAD.NodeResult.Success;
    }

    public IEnumerator<BAD.NodeResult> CloseDoors() {
        if(GameManager.Instance == null || GameManager.Instance.DoorsClosed) yield return BAD.NodeResult.Failure;

        DateTime dt = DateTime.Now + TimeSpan.FromSeconds(4f);
        GameManager.Instance.DoorsManipulationAction(false);
        do { yield return BAD.NodeResult.Continue; } while(DateTime.Now < dt);
        yield return BAD.NodeResult.Success;
    }

    public IEnumerator<BAD.NodeResult> Finsh() {
        GameManager.Instance.StopObjectAction(gameObject);
        yield return BAD.NodeResult.Success;
    }

}
