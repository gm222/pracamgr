﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(AIWeaponConditions))]
public class AIDefenceActions : ActionBase {
    private AIPointData       _safePoints;
    private AIControllerSetup _controllerSetups;
    private Vector3           _selectedNearesPoint = Vector3.zero;
    private Transform _bulletSpawnPoint;
    private Transform _rotator;
    private Transform _rotationArmCenter;



    #region Private Methods
    // Use this for initialization
    private void Start() {
        _safePoints = GetComponent<AIPointData>();
        _controllerSetups = GetComponent<AIControllerSetup>();
        Transform[] childTransforms = gameObject.GetComponentsInChildren<Transform>(false);
        _bulletSpawnPoint = childTransforms.FirstOrDefault(child => child.CompareTag("Respawn") == true);

        _rotator = childTransforms.FirstOrDefault(child => child.CompareTag("ArmRotator") == true);
        _rotationArmCenter = childTransforms.FirstOrDefault(child => child.CompareTag("ArmPoint") == true);


        bool result = CheckSetups();
    }

    private bool CheckSetups() {
        bool result = true;

        if(_safePoints == null || _controllerSetups == null || _bulletSpawnPoint == null) result = false;
        if(_safePoints == null)
            throw new System.NullReferenceException(string.Format("Missing AISafePoint component with some data"));
        if(_controllerSetups == null)
            throw new System.NullReferenceException(string.Format("Missing AIControllerSetup component with some data"));
        if(_bulletSpawnPoint == null)
            throw new System.NullReferenceException(string.Format("Missing bullet spawn poin."));

        return result;
    }


    private void Shot() {
        if(GameManager.Instance != null && GameManager.Instance.Bullet != null)
            Instantiate(GameManager.Instance.Bullet, _bulletSpawnPoint.position, _bulletSpawnPoint.rotation);
    }

    #endregion
    #region Public Methods
    public IEnumerator<BAD.NodeResult> ShotToEnemy() {
        if(!CheckSetups() || _rotationArmCenter == null || _rotator == null) yield return BAD.NodeResult.Failure;

        //Collider enemyObject = Physics.OverlapSphere(transform.position, _controllerSetups.EnemyRangeDetection).FirstOrDefault(enemy => enemy.CompareTag("Enemy"));

        // if(enemyObject == null) yield return BAD.NodeResult.Failure;


        int angle      = _controllerSetups.EnemyAngledRangeDetection;
        float distance = _controllerSetups.EnemyRangeDetection;

        Vector3 rayPosition = transform.position;

        int stepDivider = (int) Mathf.Clamp((angle+1) / 2, 15, distance * 2);

        if(stepDivider == 0) stepDivider = 1;
        int step = (int)angle*2/stepDivider;
        RaycastHit hit;

        GameObject enemy = null;

        for(int i = -angle; i <= angle; i+= step) {
            Vector3 angled = Quaternion.AngleAxis(i, transform.up) * transform.forward;
            Debug.DrawRay(rayPosition, angled * distance);
            //Gizmos.DrawRay(rayPosition, angled *distance);
            if(Physics.Raycast(rayPosition, angled, out hit, distance))
                if(hit.transform.tag.Equals("Enemy"))
                    enemy = hit.transform.gameObject;

        }

        if(enemy == null) yield return BAD.NodeResult.Failure;





        Vector3 enemyPosition = enemy.transform.position;
        Vector3 armPosition  = _rotationArmCenter.position;
        Vector3 direction     = (enemyPosition - armPosition).normalized;

        Vector2 directionY = new Vector2(direction.x, direction.z);
        Vector3 directionX = new Vector3(direction.x, 0, direction.z);

        float angleY = Mathf.Atan2(directionY.y, directionY.x) * -Mathf.Rad2Deg + 90f;
        float angleX  = Vector3.Angle(direction, directionX);

        if(direction.y > 0)
            angleX *= -1;
        Quaternion rotationY = Quaternion.AngleAxis(angleY, Vector3.up);

        Vector3 rotatorX = Vector3.right * angleX;


        while(Quaternion.Angle(transform.rotation, rotationY) > 0.2f) {
            transform.rotation = Quaternion.Slerp(transform.rotation, rotationY, Time.deltaTime * _controllerSetups.MoveDamping);
            yield return BAD.NodeResult.Continue;
        }

        _rotator.localEulerAngles = rotatorX;

        Shot();
        _controllerSetups.CurrentAmmunitionCount--;

        yield return BAD.NodeResult.Success;

    }
    public IEnumerator<BAD.NodeResult> ReloadAmmunition() {

        DateTime dt = DateTime.Now + TimeSpan.FromSeconds(2f);

        do { yield return BAD.NodeResult.Continue; } while(DateTime.Now < dt);
        _controllerSetups.CurrentAmmunitionCount = _controllerSetups.BulletsMagazineCount;
        _controllerSetups.CurrentAmunitionWithMe--;

        yield return BAD.NodeResult.Success;
    }

    #endregion
}
