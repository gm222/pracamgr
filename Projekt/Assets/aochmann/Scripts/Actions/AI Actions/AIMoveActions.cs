﻿using Assets.aochmann.Scripts.Utilities;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AIMoveActions : ActionBase
{

    private AIPointData _safePoints;
    private AIControllerSetup _controllerSetups;
    private Vector3 _selectedNearesPoint = Vector3.zero;
    public int _lastPatrolPoint = -1;
    private NavMeshAgent _agent;


    #region Private Methods
    // Use this for initialization
    private void Start()
    {
        _safePoints = GetComponent<AIPointData>();
        _controllerSetups = GetComponent<AIControllerSetup>();
        _agent = GetComponent<NavMeshAgent>();
        bool result = CheckSetups();

    }

    private bool CheckSetups()
    {
        bool result = true;

        if (_safePoints == null || _controllerSetups == null) result = false;
        if (_safePoints == null)
            throw new System.NullReferenceException(string.Format("Missing AISafePoint component with some data"));
        if (_controllerSetups == null)
            throw new System.NullReferenceException(string.Format("Missing AIControllerSetup component with some data"));

        return result;
    }
    #endregion
    #region Public Methods
    public IEnumerator<BAD.NodeResult> GoToPoin()
    {
        if (!CheckSetups()) yield return BAD.NodeResult.Failure;
        Vector3 point = _selectedNearesPoint;
        float distance = 0f;

        do
        {
            Vector3 characterPosition = transform.position;
            Vector3 position = point - characterPosition;
            Vector3 direction = position.normalized;

            direction.y = 0f;
            position.y  = 0;

            distance = position.magnitude;
            transform.position = Vector3.Lerp(characterPosition, characterPosition + direction, Time.fixedDeltaTime * _controllerSetups.MoveDamping);

            yield return BAD.NodeResult.Continue;
        }
        while (distance >= 0.08f);

        _selectedNearesPoint = transform.position;

        yield return BAD.NodeResult.Success;
    }
    public IEnumerator<BAD.NodeResult> GoToPointWithDistance()
    {
        if (!CheckSetups()) yield return BAD.NodeResult.Failure;

        Vector3 point = _selectedNearesPoint;
        float distance = 0f;

        do
        {
            Vector3 characterPosition = transform.position;
            Vector3 position = point - characterPosition;
            Vector3 direction = position.normalized;

            direction.y = 0f;
            position.y  = 0;

            distance = position.magnitude;
            transform.position = Vector3.Lerp(characterPosition, characterPosition + direction, Time.fixedDeltaTime * _controllerSetups.MoveDamping);

            yield return BAD.NodeResult.Continue;
        }
        while (distance - _controllerSetups.MaxDistance >= 0.01f);
        _selectedNearesPoint = transform.position;

        yield return BAD.NodeResult.Success;
    }
    public IEnumerator<BAD.NodeResult> SelectNearestSafePoint()
    {
        if (!CheckSetups()) yield return BAD.NodeResult.Failure;
        Transform[] points = _safePoints.SafePoints;
        if (points == null || points.Length == 0) yield return BAD.NodeResult.Failure;

        Vector3 objectPosition = transform.position;
        Vector3 selectedPoint = ScriptsHelper.SelectNearestPoint(objectPosition, points);

        if (selectedPoint == objectPosition) yield return BAD.NodeResult.Failure;
        yield return BAD.NodeResult.Success;
    }
    public IEnumerator<BAD.NodeResult> GoToBunker()
    {
        if (!CheckSetups()) yield return BAD.NodeResult.Failure;

        Vector3 point = _safePoints.BunkerPosition.position;
        Vector3 direction = point - transform.position;
        direction.y = 0f;
        float length = direction.magnitude;

        _agent.Stop();
        _agent.destination = point;

        do
        {
            _agent.Resume();
            yield return BAD.NodeResult.Continue;
        }
        while (_agent.remainingDistance / length  >= 0.08f);
        _agent.velocity = Vector3.zero;
        _agent.Stop();
        yield return BAD.NodeResult.Success;
    }



    public IEnumerator<BAD.NodeResult> GoToMinigun()
    {
        if (!CheckSetups()) yield return BAD.NodeResult.Failure;
        Transform minigunPos = _safePoints.MinigunPosition;
        Vector3 point = minigunPos == null ? transform.position : minigunPos.position;
        float distance = 0f;

        do
        {
            Vector3 characterPosition = transform.position;
            Vector3 position = point - characterPosition;
            Vector3 direction = position.normalized;

            direction.y = 0f;
            position.y  = 0;

            distance = position.magnitude;
            transform.position = Vector3.Lerp(characterPosition, characterPosition + direction, Time.fixedDeltaTime * _controllerSetups.MoveDamping);

            yield return BAD.NodeResult.Continue;
        }
        while (distance - _controllerSetups.MaxDistance >= 0.01f);
        _selectedNearesPoint = transform.position;

        yield return BAD.NodeResult.Success;
    }
    public IEnumerator<BAD.NodeResult> RunToNearestEnemy()
    {
        if (!CheckSetups()) yield return BAD.NodeResult.Failure;

        GameManager gm = GameManager.Instance;

        if (gm == null || gm.EnemiesList.Count == 0) yield return BAD.NodeResult.Failure;

        Transform[] tmpPositions = gm.EnemiesList.Select(item => item.transform).ToArray();


        Vector3 point = ScriptsHelper.SelectNearestPoint(transform.position, tmpPositions);
        float distance = 0f;

        do
        {
            Vector3 characterPosition = transform.position;
            Vector3 position = point - characterPosition;
            Vector3 direction = position.normalized;

            direction.y = 0f;
            position.y  = 0;

            distance = position.magnitude;
            transform.position = Vector3.Lerp(characterPosition, characterPosition + direction, Time.fixedDeltaTime * _controllerSetups.MoveDamping);

            yield return BAD.NodeResult.Continue;
        }
        while (distance - _controllerSetups.MaxDistance >= 0.01f);
        yield return BAD.NodeResult.Success;
    }
    public IEnumerator<BAD.NodeResult> RunAwayFromEnemy()
    {
        if (!CheckSetups()) yield return BAD.NodeResult.Failure;

        GameManager gm = GameManager.Instance;

        if (gm == null || gm.EnemiesList.Count == 0) yield return BAD.NodeResult.Failure;

        Transform[] tmpPositions = gm.EnemiesList.Select(item => item.transform).ToArray();


        Vector3 point = ScriptsHelper.SelectNearestPoint(transform.position, tmpPositions);
        float distance = 0f;

        do
        {
            Vector3 characterPosition = transform.position;
            Vector3 position = point - characterPosition;
            Vector3 direction = position.normalized;

            direction.y = 0f;
            position.y  = 0;

            distance = position.magnitude;
            transform.position = Vector3.Lerp(characterPosition, characterPosition + direction, Time.fixedDeltaTime * _controllerSetups.MoveDamping);

            yield return BAD.NodeResult.Continue;
        }
        while (distance - _controllerSetups.MaxDistance >= 0.01f);
        yield return BAD.NodeResult.Success;
    }
    public IEnumerator<BAD.NodeResult> Patrol()
    {
        if (_safePoints == null || _safePoints.PatrolPoints == null || _safePoints.PatrolPoints.childCount == 0) yield return BAD.NodeResult.Failure;

        if (_lastPatrolPoint == -1) _lastPatrolPoint++;
        else if (_lastPatrolPoint >= _safePoints.PatrolPoints.childCount) _lastPatrolPoint = 0;

        Vector3 point = _safePoints.PatrolPoints.GetChild(_lastPatrolPoint).position;

        Vector3 direction = point - transform.position;
        direction.y = 0f;
        float length = direction.magnitude;
        direction = direction.normalized;

        _agent.destination = point;
        _agent.Stop();

        do
        {
            direction = point - transform.position;
            direction.y = 0f;
            length = direction.magnitude;
            _agent.Resume();
            yield return BAD.NodeResult.Continue;
        }
        while (_agent.remainingDistance / length >= 1f);


        if (_agent.remainingDistance <= 0.01f)
        {
            yield return BAD.NodeResult.Continue;
            _agent.velocity = Vector3.zero;
            _lastPatrolPoint++;
        }

        yield return BAD.NodeResult.Success;

    }
    public IEnumerator<BAD.NodeResult> StopPatrol()
    {
        _agent.Stop();
        yield return BAD.NodeResult.Success;
    }

    public IEnumerator<BAD.NodeResult> GoInsideDoor()
    {
        if (!CheckSetups() || GameManager.Instance == null) yield return BAD.NodeResult.Failure;

        Vector3 point = GameManager.Instance.InsideDoor.position;
        _agent.Stop();
        _agent.destination = point;

        do
        {
            _agent.Resume();
            yield return BAD.NodeResult.Continue;
        }
        while (!GameManager.Instance.IsBreak && _agent.remainingDistance >= 0.08f);
        _agent.velocity = Vector3.zero;
        _agent.Stop();
        yield return BAD.NodeResult.Success;
    }

    public IEnumerator<BAD.NodeResult> GoOutsideeDoor()
    {
        if (!CheckSetups() || GameManager.Instance == null) yield return BAD.NodeResult.Failure;

        Vector3 point = GameManager.Instance.OutsideDoor.position;
        _agent.Stop();
        _agent.destination = point;

        do
        {
            _agent.Resume();
            yield return BAD.NodeResult.Continue;
        }
        while (!GameManager.Instance.IsBreak && _agent.remainingDistance >= 1.08f);
        _agent.velocity = Vector3.zero;
        _agent.Stop();
        yield return BAD.NodeResult.Success;
    }



    #endregion
}
