﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemyActions : ActionBase
{
    private AIPointData _safePoints;
    private AIControllerSetup _controllerSetups;
    public int _lastPatrolPoint = -1;
    private NavMeshAgent _agent;
    private Transform _bulletSpawnPoint;
    private Transform _rotator;
    private Transform _rotationArmCenter;
    private int _enemyID = 0;


    private void Start()
    {
        _safePoints                 = GetComponent<AIPointData>();
        _controllerSetups           = GetComponent<AIControllerSetup>();
        _agent                      = GetComponent<NavMeshAgent>();
        _enemyID                    = Array.IndexOf(GameManager.Instance.Enemies, transform.gameObject);
        Transform[] childTransforms = gameObject.GetComponentsInChildren<Transform>(false);
        _bulletSpawnPoint           = childTransforms.FirstOrDefault(child => child.CompareTag("Respawn") == true);
        _rotator                    = childTransforms.FirstOrDefault(child => child.CompareTag("ArmRotator") == true);
        _rotationArmCenter          = childTransforms.FirstOrDefault(child => child.CompareTag("ArmPoint") == true);
    }

    public IEnumerator<BAD.NodeResult> GoThroughtPath()
    {
        if (_safePoints == null || _safePoints.PatrolPoints == null || _safePoints.PatrolPoints.childCount == 0) yield return BAD.NodeResult.Failure;

        if (_lastPatrolPoint == -1)
        {
            _lastPatrolPoint++;
            _lastPatrolPoint+= _enemyID % 4;
        }
        else if (_lastPatrolPoint >= _safePoints.PatrolPoints.childCount) yield return BAD.NodeResult.Failure;

        Vector3 point = _safePoints.PatrolPoints.GetChild(_lastPatrolPoint).position;

        Vector3 direction = point - transform.position;
        direction.y = 0f;
        float length = direction.magnitude;
        direction = direction.normalized;

        _agent.destination = point;
        _agent.Stop();

        do
        {
            _agent.Resume();
            yield return BAD.NodeResult.Continue;
        }
        while (_agent.remainingDistance >= 4f || _agent.remainingDistance / length >= 1f);


        if (_agent.remainingDistance <= 0.01f)
        {
            yield return BAD.NodeResult.Continue;
            _agent.velocity = Vector3.zero;
            _agent.Stop();
            _lastPatrolPoint+=4;
        }

        yield return BAD.NodeResult.Success;

    }

    public IEnumerator<BAD.NodeResult> StopGoing()
    {
        _agent.Stop();
        yield return BAD.NodeResult.Success;
    }

    public IEnumerator<BAD.NodeResult> Finsh()
    {
        GameManager.Instance.StopObjectAction(gameObject);
        yield return BAD.NodeResult.Success;
    }

    private void Shot()
    {
        if (GameManager.Instance != null && GameManager.Instance.Bullet != null)
            Instantiate(GameManager.Instance.Bullet, _bulletSpawnPoint.position, _bulletSpawnPoint.rotation);
    }

    public IEnumerator<BAD.NodeResult> ShotToAI()
    {
        if (_rotationArmCenter == null || _rotator == null) yield return BAD.NodeResult.Failure;

        int angle = _controllerSetups.EnemyAngledRangeDetection;
        float distance = _controllerSetups.EnemyRangeDetection;

        Vector3 rayPosition = transform.position;

        int stepDivider = (int)Mathf.Clamp((angle+1) / 2, 15, distance * 2);

        if (stepDivider == 0) stepDivider = 1;
        int step = (int)angle*2/stepDivider;
        RaycastHit hit;

        GameObject enemy = null;

        for (int i = -angle; i <= angle; i+= step)
        {
            Vector3 angled = Quaternion.AngleAxis(i, transform.up) * transform.forward;
            Debug.DrawRay(rayPosition, angled * distance);
            if (Physics.Raycast(rayPosition, angled, out hit, distance))
                if (hit.transform.tag.Equals("Player"))
                    enemy = hit.transform.gameObject;

        }

        if (enemy == null) yield return BAD.NodeResult.Failure;


        Vector3 enemyPosition = enemy.transform.position;
        Vector3 armPosition = _rotationArmCenter.position;
        Vector3 direction = (enemyPosition - armPosition).normalized;

        Vector2 directionY = new Vector2(direction.x, direction.z);
        Vector3 directionX = new Vector3(direction.x, 0, direction.z);

        float angleY = Mathf.Atan2(directionY.y, directionY.x) * -Mathf.Rad2Deg + 90f;
        float angleX = Vector3.Angle(direction, directionX);

        if (direction.y > 0)
            angleX *= -1;
        Quaternion rotationY = Quaternion.AngleAxis(angleY, Vector3.up);

        Vector3 rotatorX = Vector3.right * angleX;


        while (Quaternion.Angle(transform.rotation, rotationY) > 0.2f)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, rotationY, Time.deltaTime * _controllerSetups.MoveDamping);
            yield return BAD.NodeResult.Continue;
        }

        _rotator.localEulerAngles = rotatorX;

        Shot();
        _controllerSetups.CurrentAmmunitionCount--;

        yield return BAD.NodeResult.Success;

    }

}
