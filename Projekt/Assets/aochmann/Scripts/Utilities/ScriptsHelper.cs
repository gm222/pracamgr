﻿using BAD;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.aochmann.Scripts.Utilities
{
    public static class ScriptsHelper
    {

        public static Vector3 SelectNearestPoint(Vector3 objectPosition, Transform[] points)
        {
            Vector3 selectedPoint = objectPosition;
            float lastDistance = -1f;

            for (int i = 0; i < points.Length; i++)
            {
                if (points[i] == null) continue;
                Vector3 position = objectPosition - points[i].position;
                position.y       = objectPosition.y;
                float distance = position.magnitude;

                if (lastDistance == -1)
                {
                    lastDistance = distance;
                    selectedPoint = position;
                }
                else if (lastDistance > distance)
                {
                    lastDistance  = distance;
                    selectedPoint = position;
                }
            }

            return selectedPoint;
        }

        public static Vector3 SelectNearestPoint(Vector3 objectPosition, Vector3[] points)
        {
            Vector3 selectedPoint = objectPosition;
            float lastDistance = -1f;

            for (int i = 0; i < points.Length; i++)
            {
                if (points[i] == null) continue;
                Vector3 position = objectPosition - points[i];
                position.y       = objectPosition.y;
                float distance = position.magnitude;

                if (lastDistance == -1)
                {
                    lastDistance = distance;
                    selectedPoint = position;
                }
                else if (lastDistance > distance)
                {
                    lastDistance  = distance;
                    selectedPoint = position;
                }
            }

            return selectedPoint;
        }

        public static int GetNodesCount(Node node)
        {
            int nodesCount = 0;
            Queue<Node> Q = new Queue<Node>();
            Q.Enqueue(node);

            while (Q.Count > 0)
            {
                nodesCount++;
                Branch branch = Q.Dequeue() as Branch;
                if (branch != null)
                    foreach (Node child in branch.children)
                        Q.Enqueue(child);
            }

            return nodesCount;
        }

        public static void SetNodeArrayStructure(Node[] array, Node node)
        {
            array[node.ID] = node;
            Branch branch = node as Branch;
            if (branch != null)
                foreach (Node child in branch.children)
                    SetNodeArrayStructure(array, child);
        }

        public static void SetNodeTypesArray(int[] array, Node node)
        {
            array[node.ID] = node.NodeType;
            Branch branch = node as Branch;

            if (branch != null)
                foreach (Node child in branch.children)
                    SetNodeTypesArray(array, child);
        }

        public static void SetBTStructure(object[] aiStructure, Node node)
        {
            object[] list = new object[2];
            int parrent = node.parent == null ? node.ID : node.parent.ID;
            List<int> childrenNodeList = new List<int>();

            list[0] = parrent;
            list[1] = childrenNodeList;

            aiStructure[node.ID] = list;

            Branch branch = node as Branch;
            if (branch != null)
                foreach (Node child in branch.children)
                {
                    childrenNodeList.Add(child.ID);
                    SetBTStructure(aiStructure, child);
                }
        }
    }
}
