﻿using UnityEngine;

public class AIWeaponConditions : ConditionBase {

    GameObject[] _miniguns;


    #region Non Serialized Fields
    private AIControllerSetup _controllerSetups;
    #endregion
    #region Private Methods
    private void Start() {
        _miniguns = GameObject.FindGameObjectsWithTag("Minigun");
        _controllerSetups = GetComponent<AIControllerSetup>();
        bool result = CheckSetups();
    }

    private bool CheckSetups() {
        bool result = true;
        if(_controllerSetups == null) {
            result = false;
            //throw new System.NullReferenceException(string.Format("Missing AIControllerSetup component with some data"));
        }

        return result;
    }

    #endregion
    #region Public Methods
    public bool DoIHaveAmmunitionWithMe() {
        return CheckSetups() == false ? false : _controllerSetups.CurrentAmunitionWithMe > 0;
    }

    public bool CheckAmmunition() {
        return CheckSetups() == false ? false : _controllerSetups.CurrentAmmunitionCount > 0;
    }

    public bool IsSomeAmmunitionInBunker() {
        return GameManager.Instance != null && GameManager.Instance.CurrentBunkerAmmunition > 0 && GameManager.Instance.CurrentBunkerAmmunition <= GameManager.Instance.BunkerAmmunition;
    }

    public bool IsAmunitionInMinigun() {
        //bool result = true;
        //if(_miniguns == null || _miniguns.Length == 0) {
        //    result = false;
        //} else {
        //    Transform[] tmpMinigunsTransforms = _miniguns.Select(minigunTMP => minigunTMP.transform).ToArray();

        //    Vector3 selectedMinigunVector = ScriptsHelper.SelectNearestPoint(transform.position, tmpMinigunsTransforms);
        //    Transform tmpmini = tmpMinigunsTransforms.FirstOrDefault(x => x.transform.position == selectedMinigunVector);
        //    if(tmpmini == null) result = false;
        //    else {
        //        GameObject minigun = tmpmini.gameObject;
        //    }
        //}

        //if(GameManager.Instance == null || GameManager.Instance.CurrentMinigunAmmuniton <= 0 || GameManager.Instance.CurrentMinigunAmmuniton > GameManager.Instance.MinigunAmmuniton) return false;
        return GameManager.Instance != null && GameManager.Instance.CurrentMinigunAmmuniton > 0 && GameManager.Instance.CurrentMinigunAmmuniton <= GameManager.Instance.MinigunAmmuniton;

    }

    public bool IsMinigunEnabled() {
        return GameManager.Instance != null && GameManager.Instance.MinigunEnabled;
    }

    public bool DoINeedReload() {
        float currentAmount = _controllerSetups.CurrentAmmunitionCount;
        float maxAmmout     = _controllerSetups.BulletsMagazineCount;

        return currentAmount / maxAmmout <= 0.15f;
    }
    #endregion
}
