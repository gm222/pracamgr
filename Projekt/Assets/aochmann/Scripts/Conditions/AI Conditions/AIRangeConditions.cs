﻿using Assets.aochmann.Scripts.Utilities;
using System.Linq;
using UnityEngine;

public class AIRangeConditions : ConditionBase
{

    #region Non Serialized Fields
    private AIControllerSetup _controllerSetups;
    private AIPointData _safePoints;
    private Transform[] _miniguns;
    #endregion
    #region Private Methods
    private void Start()
    {
        _controllerSetups = GetComponent<AIControllerSetup>();
        _safePoints = GetComponent<AIPointData>();
        bool result = CheckSetups();
        _miniguns = GameObject.FindGameObjectsWithTag("Minigun").Select(minigun => minigun.transform).ToArray();
    }

    private bool CheckSetups()
    {
        bool result = true;

        if (_safePoints == null || _controllerSetups == null) result = false;

        if (_safePoints == null)
            throw new System.NullReferenceException(string.Format("Missing AISafePoint component with some data"));
        if (_controllerSetups == null)
            throw new System.NullReferenceException(string.Format("Missing AIControllerSetup component with some data"));

        return result;
    }

    #endregion
    #region Public Methods

    public bool CheckEnemyInSphereRange()
    {
        return Physics.OverlapSphere(transform.position, _controllerSetups.EnemyRangeDetection).FirstOrDefault(enemy => enemy.CompareTag("Enemy")) != null;
    }

    public bool CheckEnemyInAngleRange()
    {

        int angle = _controllerSetups.EnemyAngledRangeDetection;
        float distance = _controllerSetups.EnemyRangeDetection;

        Vector3 rayPosition = transform.position;
        Vector3 leftRayRotation = Quaternion.AngleAxis(-angle, transform.up) * transform.forward;
        Vector3 rightRayRotation = Quaternion.AngleAxis(angle, transform.up) * transform.forward;

        int stepDivider = (int)Mathf.Clamp((angle+1) / 2, 15, distance * 2);

        if (stepDivider == 0) stepDivider = 1;
        int step = (int)angle*2/stepDivider;
        RaycastHit hit;

        for (int i = -angle; i <= angle; i+= step)
        {
            Vector3 angled = Quaternion.AngleAxis(i, transform.up) * transform.forward;
            Debug.DrawRay(rayPosition, angled * distance);
            if (Physics.Raycast(rayPosition, angled, out hit, distance))
                if (hit.transform.tag.Equals("Enemy"))
                    return true;
        }

        return false;

    }

    public bool CheckMinigunInRange()
    {
        //return Physics.OverlapSphere(transform.position, _controllerSetups.MinigunRangeDetection).FirstOrDefault(enemy => enemy.CompareTag("Minigun")) != null;
        if (_miniguns != null)
        {
            Vector3 position = ScriptsHelper.SelectNearestPoint(transform.position, _miniguns);
            return position == transform.position ? false : Vector3.Distance(transform.position, position) <= _controllerSetups.MinigunRangeDetection;
        }

        return false;
    }

    public bool AreYouAtTheDoor()
    {
        if (GameManager.Instance == null || GameManager.Instance.Door == null) return false;

        Vector3 door = GameManager.Instance.Door.position;

        Vector3 direction = door - transform.position;
        float distance = direction.magnitude;

        return distance <= 2f;
    }

    public bool AreYouInBunker()
    {
        Vector3 direction = _safePoints.BunkerPosition.position - transform.position;
        float distance = direction.magnitude;

        return distance <= 1f;
    }

    #endregion


    //#region Unity Tests
    //private void OnDrawGizmos() {
    //    if(_controllerSetups != null)
    //        Gizmos.DrawSphere(transform.position, _controllerSetups.EnemyRangeDetection);
    //}
    //#endregion
}
