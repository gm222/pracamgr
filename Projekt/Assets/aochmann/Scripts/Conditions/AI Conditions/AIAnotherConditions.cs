﻿using System.Linq;
using UnityEngine;

public class AIAnotherConditions : ConditionBase
{

    #region Non Serialized Fields
    private AIControllerSetup _controllerSetups;
    #endregion
    #region Private Methods
    private void Start()
    {
        _controllerSetups = GetComponent<AIControllerSetup>();
        bool result = CheckSetups();
    }

    private bool CheckSetups()
    {
        bool result = true;
        if (_controllerSetups == null)
        {
            result = false;
            throw new System.NullReferenceException(string.Format("Missing AIControllerSetup component with some data"));
        }

        return result;
    }
    #endregion


    #region Public Bool Methods
    public bool CheckChanceOfSurvival()
    {

        float currentAmmo = _controllerSetups.CurrentAmmunitionCount;
        float bulletMagazine = _controllerSetups.BulletsMagazineCount;
        float ammoPacks = _controllerSetups.CurrentAmunitionWithMe;

        if (ammoPacks == 0) ammoPacks = 1;

        int ennemyOnMe = Physics.OverlapSphere(transform.position, _controllerSetups.EnemyRangeDetection+10f).Where(enemy => enemy.CompareTag("Enemy")).ToArray().Length;

        if (ennemyOnMe == 0) ennemyOnMe = 1;

        float p1 = currentAmmo / bulletMagazine;
        float p2 = p1 * ammoPacks;
        float p3 = p2 * ennemyOnMe;


        return p3 >= 0.45f;
    }

    public bool AreDoorClosed()
    {
        if (GameManager.Instance == null) return false;

        return GameManager.Instance.DoorsClosed;
    }

    #endregion
}
