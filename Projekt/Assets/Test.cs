﻿using UnityEngine;

public class Test : MonoBehaviour {

    [SerializeField]
    private Transform _enemyTransform;

    [SerializeField]
    private Transform _headTransform;

    [SerializeField]
    private Transform _bulletSpawnPointTransform;

    [SerializeField]
    private Transform _rotatorTransform;

    [SerializeField, Range(0f, 100f)]
    private float _speed = 0.2f;

    [SerializeField, Range(0f, 100f)]
    private float _rayDistance = 40f;


    private Vector3 _rotationY = Vector3.zero;
    private Vector3 _rotationZ = Vector3.zero;
    Quaternion _quaternionY;


    // Update is called once per frame
    void Update() {
        Vector3 enemyPosition = _enemyTransform.position;
        Vector3 headPosition  = transform.position;
        Vector3 direction     = (enemyPosition - headPosition).normalized;

        Vector2 tmp1          = new Vector2(direction.x, direction.z);
        Vector2 tmp2          = new Vector2(direction.x, 0);
        Vector3 tmp3          = new Vector3(direction.x, 0, direction.z);

        float angleY          = Vector2.Angle(tmp1, tmp2);
        float angleZ          = Vector3.Angle(direction, tmp3);
        Debug.Log(string.Format("{0} \t {1} \t {2}", direction, angleY, transform.position));

        if(direction.x > 0f)
            angleY = 180f -angleY;

        if(direction.y > 0f)
            angleZ *= -1f;

        if(direction.z < 0f)
            angleY *= -1f;

        _rotationY.y = angleY;
        _rotationZ.z = angleZ;

        _quaternionY = Quaternion.Euler(_rotationY);
        //var quZ = Quaternion.Euler(_rotationZ);
        //_rotatorTransform.rotation         = Quaternion.Lerp(_rotatorTransform.rotation, quZ, Time.deltaTime * _speed);
        transform.rotation                 = Quaternion.Lerp(transform.rotation, _quaternionY, Time.deltaTime * _speed);
        _rotatorTransform.localEulerAngles = _rotationZ;

    }

    public void OnDrawGizmos() {
        Vector3 bulletPosition = _bulletSpawnPointTransform.position;
        Vector3 bulletForward  = _bulletSpawnPointTransform.forward;

        Gizmos.color = Color.blue;

        Gizmos.DrawRay(bulletPosition, bulletForward * _rayDistance);
    }
}
