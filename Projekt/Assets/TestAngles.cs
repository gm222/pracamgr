﻿using UnityEngine;

public class TestAngles : MonoBehaviour
{
    [SerializeField]
    private Transform point1;

    [SerializeField]
    private Transform point2;

    [SerializeField]
    private Transform rotator;

    public Vector3 Direction = Vector3.zero;
    public Vector3 ArmPosition = Vector3.zero;
    public Vector3 ArmForwardPosition = Vector3.zero;
    public Vector3 EnemyPosition = Vector3.zero;
    public Vector3 EnemyForwardPosition = Vector3.zero;

    [SerializeField, Range(0f, 200f)]
    private float _rayDistance = 50f;

    [SerializeField, Range(0f, 10f)]
    private float _pointRadius = 1f;

    [SerializeField]
    private bool _disableRotating = false;

    public void OnDrawGizmos()
    {

        if (point1 != null)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawRay(point1.position, point1.forward * _rayDistance);
            Gizmos.DrawSphere(point1.position, _pointRadius);
        }

        if (point2 != null)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawRay(point2.position, point2.forward * _rayDistance);
            Gizmos.DrawSphere(point2.position, _pointRadius);
        }

        if (point1 && point2)
        {
            ArmPosition          = point1.position;
            EnemyPosition        = point2.position;
            ArmForwardPosition   = point1.forward;
            EnemyForwardPosition = point2.forward;


            Vector3 direction = (EnemyPosition - ArmPosition).normalized;

            Vector2 directionY = new Vector2(direction.x, direction.z);
            Vector3 directionZ = new Vector3(direction.x, 0, direction.z);

            float angleZ = Mathf.Atan2(directionY.y, directionY.x) * -Mathf.Rad2Deg + 90f;
            float angle = Vector3.Angle(direction, directionZ);

            if (direction.y > 0)
                angle *= -1;

            transform.rotation = Quaternion.AngleAxis(angleZ, Vector3.up);

            float z = Mathf.Lerp(rotator.localEulerAngles.x, angle, Time.deltaTime);
            rotator.localEulerAngles = new Vector3(z, 0f, 0f);

        }
    }

    private bool IsBehind(Transform p1, Transform p2)
    {
        return Vector3.Dot(Vector3.forward, p1.InverseTransformPoint(p2.transform.position)) < 0;
    }

}
