﻿using UnityEngine;

public class TTestRandomPoint : MonoBehaviour
{

    private float timeSet = 2f;
    private Vector3 point;
    bool hitTest = false;

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.red;


        Gizmos.DrawSphere(point, 2f);
        Gizmos.DrawLine(transform.position, point);

        if (TGameManager.Instance == null) return;

        NavMeshHit hit;

        Vector3 randomDirection = Random.insideUnitSphere * TGameManager.Instance.WalkRadius;
        randomDirection += transform.position;


        NavMesh.SamplePosition(randomDirection, out hit, TGameManager.Instance.WalkRadius, 1);
        Vector3 finalPosition = hit.position;

        GameObject enemy = TGameManager.Instance.EnemiesPrefab;
        Vector3 enemyPosition = enemy.transform.position;
        if (!(finalPosition.x == enemyPosition.x && finalPosition.y == enemyPosition.y && finalPosition.z == enemyPosition.z))
            if (Vector3.Distance(finalPosition, enemyPosition) >= 10f)
                point = finalPosition;

    }
}
