﻿using UnityEngine;

[RequireComponent(typeof(TController))]
public class TEnemyConditions : ConditionBase
{
    private TController _controllerSetups;

    private void Start()
    {
        _controllerSetups  = GetComponent<TController>();
    }

    public bool CheckAIInAngleRange()
    {
        int angle      = _controllerSetups.ObjectAngleDetection;
        float distance = _controllerSetups.ObjectDistanceDetection;

        Vector3 rayPosition      = transform.position;
        Vector3 leftRayRotation  = Quaternion.AngleAxis(-angle, transform.up) * transform.forward;
        Vector3 rightRayRotation = Quaternion.AngleAxis(angle, transform.up) * transform.forward;

        int stepDivider = (int) Mathf.Clamp((angle+1) / 2, 15, distance * 2);

        if(stepDivider == 0) stepDivider = 1;
        int step = (int)angle*2/stepDivider;
        RaycastHit hit;

        for(int i = -angle; i <= angle; i+= step)
        {
            Vector3 angled = Quaternion.AngleAxis(i, transform.up) * transform.forward;
            Debug.DrawRay(rayPosition, angled * distance);
            if(Physics.Raycast(rayPosition, angled, out hit, distance))
                if(hit.transform.tag.Equals("Player"))
                    return true;
        }

        return false;
    }



    public bool CheckAmmunition()
    {
        return _controllerSetups.CurrentAmunitionCount > 0;
    }
}
