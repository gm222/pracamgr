﻿using UnityEngine;

public class TController : MonoBehaviour {
    #region Serialized-Fields
    [SerializeField, Range(0f, 500f)]
    private float _objectDistanceDetection = 2f;

    [SerializeField, Range(0, 360)]
    private int _objectAngleDetection = 45;

    [SerializeField, Range(0f, 2000f)]
    private float _rotationDamping = 100;

    [SerializeField, Range(0f, 2000f)]
    private float _moveSpeed = 100;

    [SerializeField, Range(0f, 2000f)]
    private float _accelerationSpeed = 120;

    [SerializeField, Range(0f, 20f)]
    private float _stopDistance = 1.5f;

    [SerializeField, Range(0, 100)]
    private int _currentAmunitionCount = 10;

    [SerializeField, Range(0,100)]
    private int _currentAmunitionWithMe = 2;

    [SerializeField, Range(0, 100)]
    private int _bulletsMagazineCount = 10;

    [SerializeField, Range(0,100)]
    private int _ammunitionWithMeCount = 2;

    #endregion
    #region Non-Serialized Fields
    private NavMeshAgent _navAgent;
    #endregion
    #region Properties
    public float ObjectDistanceDetection {
        get { return _objectDistanceDetection; }
    }

    public int ObjectAngleDetection {
        get { return _objectAngleDetection; }
    }

    public int CurrentAmunitionCount {
        get {
            return _currentAmunitionCount;
        }

        set {
            _currentAmunitionCount=value;
        }
    }

    public int CurrentAmunitionWithMe {
        get {
            return _currentAmunitionWithMe;
        }

        set {
            _currentAmunitionWithMe=value;
        }
    }

    public int BulletsMagazineCount {
        get { return _bulletsMagazineCount; }
    }

    public float RotationDamping {
        get { return _rotationDamping; }
    }
    #endregion
    // Awake is called when the script instance is being loaded
    private void Awake() {
        _navAgent = GetComponent<NavMeshAgent>();
        if(_navAgent != null) {
            _navAgent.speed            = _moveSpeed;
            _navAgent.acceleration     = _accelerationSpeed;
            _navAgent.stoppingDistance = _stopDistance;
        }

        _currentAmunitionCount  = _bulletsMagazineCount;
        _currentAmunitionWithMe = _ammunitionWithMeCount;
    }

    private void OnValidate() {
        if(_navAgent != null) {
            _navAgent.speed            = _moveSpeed;
            _navAgent.acceleration     = _accelerationSpeed;
            _navAgent.stoppingDistance = _stopDistance;
        }
    }

    public void ResetData() {
        _currentAmunitionCount  = _bulletsMagazineCount;
        _currentAmunitionWithMe = _ammunitionWithMeCount;
    }
}
