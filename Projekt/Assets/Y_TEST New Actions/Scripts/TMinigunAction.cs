﻿using Assets.aochmann.Scripts.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TMinigunAction : ActionBase
{
    private Transform _rotator;
    private Transform _gunCheckPoint;
    private Transform _bulletSpawnPoint;
    private int _patrolPointID = 0;
    private Vector3[] _patrolPoints;

    void Start()
    {
        Transform[] childTransforms = gameObject.GetComponentsInChildren<Transform>(false);
        _rotator = childTransforms.FirstOrDefault(child => child.CompareTag("MinigunRotator") == true);
        _gunCheckPoint = childTransforms.FirstOrDefault(child => child.name.Equals("CheckGunPoint") == true);
        _bulletSpawnPoint = _gunCheckPoint;

        Transform patrolPoints = TGameManager.Instance.MinigunPatrolPoints;
        if (patrolPoints != null)
        {
            List<Vector3> tmpMovePoints = new List<Vector3>();
            Queue queueList = new Queue();

            queueList.Enqueue(patrolPoints);

            while (queueList.Count > 0)
            {
                Transform pointP = queueList.Dequeue() as Transform;

                if (pointP.childCount > 0)
                    for (int i = 0; i < pointP.childCount; i++)
                        queueList.Enqueue(pointP.GetChild(i));
                else
                    tmpMovePoints.Add(pointP.position);
            }

            _patrolPoints = tmpMovePoints.ToArray();
        }
    }

    public IEnumerator<BAD.NodeResult> MinigunEnemyShoot()
    {
        Transform enemy = TGameManager.Instance.NearestEnemy;

        if (enemy != null)
        {
            Vector3 direction = (enemy.position - _gunCheckPoint.position).normalized;
            Vector2 directionY = new Vector2(direction.x, direction.z);
            Vector3 directionX = new Vector3(direction.x, 0, direction.z);
            float angleOriginal = Mathf.Atan2(directionY.y, directionY.x) * -Mathf.Rad2Deg + 90f;
            float angleX = Vector3.Angle(direction, directionX);

            if (direction.y > 0)
                angleX *= -1;

            if (direction.z < 0)
                angleX *= -1;

            yield return BAD.NodeResult.Continue;

            float testAngle = angleOriginal;

            if (testAngle<0f) testAngle += 360f;

            Vector3 rot = _rotator.localEulerAngles;
            rot.z = testAngle;
            rot.x = angleX;

            _rotator.localEulerAngles = rot;

            Shot();
            TGameManager.Instance.CurrentMinigunAmmunition--;
            yield return BAD.NodeResult.Success;
        }

        yield return BAD.NodeResult.Failure;
    }

    public IEnumerator<BAD.NodeResult> SetNearestPatrolPoint()
    {
        Vector3 point = ScriptsHelper.SelectNearestPoint(_gunCheckPoint.position, _patrolPoints);
        int index = Array.IndexOf(_patrolPoints, point);

        if (index >= 0) _patrolPointID = index;

        yield return BAD.NodeResult.Success;
    }

    public IEnumerator<BAD.NodeResult> MinigunPatrol()
    {

        Vector3 patrolPoint = _patrolPoints[_patrolPointID];
        Vector3 direction = (patrolPoint - _gunCheckPoint.position).normalized;
        Vector2 directionY = new Vector2(direction.x, direction.z);
        Vector3 directionX = new Vector3(direction.x, 0, direction.z);
        float angleOriginal = Mathf.Atan2(directionY.y, directionY.x) * -Mathf.Rad2Deg + 90f;
        float angleX = Vector3.Angle(direction, directionX);

        if (direction.y > 0)
            angleX *= -1;

        if (direction.z < 0)
            angleX *= -1;

        float testAngle = angleOriginal;
        if (testAngle<0f) testAngle += 360f;

        do
        {
            if (CheckEnemyOnMinigunRange()) yield return BAD.NodeResult.Failure;
            float angleZ = Mathf.LerpAngle(_rotator.eulerAngles.y, testAngle, Time.fixedDeltaTime * 50f);
            _rotator.rotation = Quaternion.Euler(new Vector3(_rotator.eulerAngles.x, angleZ, 0f));
            yield return BAD.NodeResult.Continue;

        } while (Mathf.RoundToInt(_rotator.eulerAngles.y) != Mathf.RoundToInt(testAngle));

        _patrolPointID = (_patrolPointID + 1)%_patrolPoints.Length;

        yield return BAD.NodeResult.Success;
    }

    private void Shot()
    {
        if (GameManager.Instance != null && GameManager.Instance.Bullet != null)
            Instantiate(GameManager.Instance.Bullet, _bulletSpawnPoint.position, _bulletSpawnPoint.rotation);
    }

    private bool CheckEnemyOnMinigunRange()
    {
        Gizmos.color = Color.blue;
        int angle = TGameManager.Instance.MinigunAngle;
        float distance = TGameManager.Instance.MinigunRangeDetection;

        Vector3 rayPosition = _gunCheckPoint.position;

        RaycastHit hit;
        int stepDivider = (int)Mathf.Clamp((angle+1) / 2, 15, distance * 2);

        if (stepDivider == 0) stepDivider = 1;
        int step = (int)angle*2/stepDivider;


        for (int i = -angle; i <= angle; i+= step)
        {
            float gunNormalizedCopy = _gunCheckPoint.position.normalized.y;
            for (float j = -1f; j < gunNormalizedCopy; j+=0.1f)
            {
                Vector3 newGunCheck = _gunCheckPoint.forward;
                newGunCheck.y = j;
                Vector3 angled = Quaternion.AngleAxis(i, _gunCheckPoint.up) * newGunCheck;
                Debug.DrawRay(rayPosition, angled * distance);
                if (Physics.Raycast(rayPosition, angled, out hit, distance))
                    if (hit.transform.tag.Equals("Enemy"))
                    {
                        TGameManager.Instance.NearestEnemy = hit.transform;
                        return true;
                    }
            }
        }
        return false;
    }

}
