﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(TAIConditions))]
[RequireComponent(typeof(TController))]
[RequireComponent(typeof(NavMeshAgent))]
public class TEnemyActions : ActionBase
{

    #region Private Fields

    private bool _backwards = false;
    private int _lastPatrolPoint = -1;
    private NavMeshAgent _agent;
    private TEnemyConditions _enemyConditions;
    private TController _controllerSetups;
    private Transform _bulletSpawnPoint;
    private Transform _rotationArmCenter;
    private Transform _rotator;
    private Vector3[] _enemyPoints;

    #endregion Private Fields

    #region Public Methods

    public void StopMoving()
    {
        if (_agent != null)
        {
            if (transform.gameObject.activeInHierarchy)
                _agent.Stop();
            _agent.destination = transform.position;
        }
    }

    //good
    public IEnumerator<BAD.NodeResult> Patrol()
    {
        if (_lastPatrolPoint == -1)
        {
            _lastPatrolPoint++;
            _agent.destination = (_enemyPoints[_lastPatrolPoint]);
            _agent.Stop();
        }

        if (_lastPatrolPoint >= _enemyPoints.Length)
        {
            TGameManager.Instance.StopObjectAction(gameObject);
            yield return BAD.NodeResult.Failure;
        }

        Vector3 charPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        float distance = 0f;
        bool haveSeenOtherObject = false;

        _agent.destination = _enemyPoints[_lastPatrolPoint];

        _agent.Resume();
        do
        {
            distance                          = Vector3.Distance(transform.position, _enemyPoints[_lastPatrolPoint]);
            float distanceToLastPositionCheck = Vector3.Distance(transform.position, charPosition);

            if (distanceToLastPositionCheck >= TGameManager.Instance.DistanceCkeck || (distanceToLastPositionCheck >= 0f && distanceToLastPositionCheck < 0.1f))
            {
                haveSeenOtherObject = _enemyConditions.CheckAIInAngleRange();
                if (haveSeenOtherObject)
                {
                    _agent.Stop();
                    yield return BAD.NodeResult.Failure;
                }

                charPosition.x = transform.position.x;
                charPosition.y = transform.position.y;
                charPosition.z = transform.position.z;

            }
            yield return BAD.NodeResult.Continue;
        } while (distance >= _agent.stoppingDistance);

        _agent.Stop();
        _lastPatrolPoint = !_backwards ? _lastPatrolPoint + 1 : _lastPatrolPoint - 1;

        if (_lastPatrolPoint >= _enemyPoints.Length)
        {
            TGameManager.Instance.StopObjectAction(gameObject);
            yield return BAD.NodeResult.Failure;
        }

        if (_lastPatrolPoint < 0)
        {
            _backwards = !_backwards;
            _lastPatrolPoint += 2;
        }

        _agent.destination = _enemyPoints[_lastPatrolPoint];
        _agent.Stop();

        yield return BAD.NodeResult.Success;
    }

    //work
    public IEnumerator<BAD.NodeResult> ShotToAI()
    {

        if (!_enemyConditions.CheckAmmunition()) yield return BAD.NodeResult.Failure;

        int angle = _controllerSetups.ObjectAngleDetection;
        float distance = _controllerSetups.ObjectDistanceDetection;

        if (distance == 0f) distance = 0.1f;

        Vector3 rayPosition = transform.position;
        float min = Mathf.Min(15, distance * 2f);
        float max = Math.Max(15, distance * 2f);
        int stepDivider = (int)Mathf.Clamp((angle+1) / 2, min, max);

        if (stepDivider == 0) stepDivider = 1;
        int step = (int)angle*2/stepDivider;
        RaycastHit hit;

        GameObject enemy = null;

        for (int i = -angle; i <= angle; i+= step)
        {
            Vector3 angled = Quaternion.AngleAxis(i, transform.up) * transform.forward;
            Debug.DrawRay(rayPosition, angled * distance);
            if (Physics.Raycast(rayPosition, angled, out hit, distance))
                if (hit.transform.tag.Equals("Player"))
                    enemy = hit.transform.gameObject;
        }

        if (enemy == null) yield return BAD.NodeResult.Failure;

        Vector3 enemyPosition = enemy.transform.position;
        Vector3 armPosition = _rotationArmCenter.position;
        Vector3 direction = (enemyPosition - armPosition);
        float lenght = direction.magnitude;

        if (lenght > _controllerSetups.ObjectDistanceDetection) yield return BAD.NodeResult.Failure;

        direction              = direction.normalized;
        Vector2 directionY = new Vector2(direction.x, direction.z);
        Vector3 directionX = new Vector3(direction.x, 0, direction.z);
        float angleY = Mathf.Atan2(directionY.y, directionY.x) * -Mathf.Rad2Deg + 90f;
        float angleX = Vector3.Angle(direction, directionX);

        if (direction.y > 0)
            angleX *= -1;

        Quaternion rotationY = Quaternion.AngleAxis(angleY, Vector3.up);
        Vector3 rotatorX = Vector3.right * angleX;

        while (Quaternion.Angle(transform.rotation, rotationY) > 0.2f)
        {
            transform.rotation     = Quaternion.Slerp(transform.rotation, rotationY, Time.fixedDeltaTime * _controllerSetups.RotationDamping);
            enemyPosition          = enemy.transform.position;
            armPosition            = _rotationArmCenter.position;
            direction              = (enemyPosition - armPosition);
            lenght                 = direction.magnitude;

            if (lenght > _controllerSetups.ObjectDistanceDetection) yield return BAD.NodeResult.Failure;

            direction              = direction.normalized;
            directionY             = new Vector2(direction.x, direction.z);
            angleY                 = Mathf.Atan2(directionY.y, directionY.x) * -Mathf.Rad2Deg + 90f;
            rotationY              = Quaternion.AngleAxis(angleY, Vector3.up);

            yield return BAD.NodeResult.Continue;
        }

        directionX = new Vector3(direction.x, 0, direction.z);
        angleX     = Vector3.Angle(direction, directionX);

        if (direction.y > 0)
            angleX *= -1;

        rotatorX                  = Vector3.right * angleX;
        _rotator.localEulerAngles = rotatorX;

        Shot();

        _controllerSetups.CurrentAmunitionCount--;
        yield return BAD.NodeResult.Success;
    }

    public IEnumerator<BAD.NodeResult> FinishEnemyAction()
    {
        TGameManager.Instance.StopObjectAction(gameObject);
        yield return BAD.NodeResult.Success;
    }

    public void ResetData()
    {

        _lastPatrolPoint = -1;

    }
    #endregion Public Methods

    #region Private Methods

    // Use this for initialization
    private void Start()
    {
        _agent             = GetComponent<NavMeshAgent>();
        _controllerSetups  = GetComponent<TController>();
        _enemyConditions      = GetComponent<TEnemyConditions>();
        Transform enemyPoints = TGameManager.Instance.EnemyWayPoints;

        Transform[] childTransforms = gameObject.GetComponentsInChildren<Transform>(false);
        _bulletSpawnPoint = childTransforms.FirstOrDefault(child => child.CompareTag("Respawn") == true);

        _rotator = childTransforms.FirstOrDefault(child => child.CompareTag("ArmRotator") == true);
        _rotationArmCenter = childTransforms.FirstOrDefault(child => child.CompareTag("ArmPoint") == true);

        if (enemyPoints != null)
        {
            List<Vector3> tmpMovePoints = new List<Vector3>();
            Queue queueList = new Queue();

            queueList.Enqueue(enemyPoints);

            while (queueList.Count > 0)
            {
                Transform pointP = queueList.Dequeue() as Transform;

                if (pointP.childCount > 0)
                    for (int i = 0; i < pointP.childCount; i++)
                        queueList.Enqueue(pointP.GetChild(i));
                else
                    tmpMovePoints.Add(pointP.position);
            }

            _enemyPoints = tmpMovePoints.ToArray();
        }
    }

    private void Shot()
    {
        if (TGameManager.Instance != null && TGameManager.Instance.Bullet != null)
        {
            Instantiate(TGameManager.Instance.Bullet, _bulletSpawnPoint.position, _bulletSpawnPoint.rotation);
            _controllerSetups.CurrentAmunitionCount--;
        }
    }




    #endregion Private Methods
}
