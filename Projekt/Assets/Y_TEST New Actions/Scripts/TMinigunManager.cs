﻿using Assets.aochmann.NodeEditor.Scripts;
using BAD;
using UnityEngine;

public class TMinigunManager : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        BADReactor minigunReactor = GetComponent<BAD.BADReactor>();
        NodesContainer minigunNodesContainer = GetComponent<NodesContainer>();
        minigunNodesContainer.Initialize();
        minigunReactor.StartLogic();
    }

}
