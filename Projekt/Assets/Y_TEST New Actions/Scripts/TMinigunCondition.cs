﻿using System.Linq;
using UnityEngine;

public class TMinigunCondition : ConditionBase {

    Transform _rotator;
    Transform _gunCheckPoint;
    // Use this for initialization
    void Start() {
        Transform[] childTransforms = gameObject.GetComponentsInChildren<Transform>(false);
        _rotator = childTransforms.FirstOrDefault(child => child.CompareTag("MinigunRotator") == true);
        _gunCheckPoint = childTransforms.FirstOrDefault(child => child.name.Equals("CheckGunPoint") == true);
    }

    public bool CheckEnemyOnMinigunRange() {
        Gizmos.color = Color.blue;
        int angle      = TGameManager.Instance.MinigunAngle;
        float distance = TGameManager.Instance.MinigunRangeDetection;

        Vector3 rayPosition = _gunCheckPoint.position;

        RaycastHit hit;
        int stepDivider = (int) Mathf.Clamp((angle+1) / 2, 15, distance * 2);

        if(stepDivider == 0) stepDivider = 1;
        int step = (int)angle*2/stepDivider;


        for(int i = -angle; i <= angle; i+= step) {
            float gunNormalizedCopy = _gunCheckPoint.position.normalized.y;
            for(float j = -1f; j < gunNormalizedCopy; j+=0.1f) {
                Vector3 newGunCheck = _gunCheckPoint.forward;
                newGunCheck.y = j;
                Vector3 angled = Quaternion.AngleAxis(i, _gunCheckPoint.up) * newGunCheck;
                Debug.DrawRay(rayPosition, angled * distance);
                if(Physics.Raycast(rayPosition, angled, out hit, distance))
                    if(hit.transform.tag.Equals("Enemy")) {
                        TGameManager.Instance.NearestEnemy = hit.transform;
                        return true;
                    }
            }
        }
        return false;
    }

    public bool CheckMinigunActivate() {
        return TGameManager.Instance == null ? false : TGameManager.Instance.MinigunEnabled;
    }

    public bool CheckAmmunition() {
        return TGameManager.Instance.CurrentMinigunAmmunition > 0 && TGameManager.Instance.CurrentMinigunAmmunition <= TGameManager.Instance.MinigunAmmunition;
    }
}
