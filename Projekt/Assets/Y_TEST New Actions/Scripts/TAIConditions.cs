﻿using UnityEngine;

[RequireComponent(typeof(TController))]
public class TAIConditions : ConditionBase
{
    private TController _controllerSetups;

    private void Start()
    {
        _controllerSetups  = GetComponent<TController>();
    }

    public bool CheckEnemyInAngleRange()
    {
        int angle = _controllerSetups.ObjectAngleDetection;
        float distance = _controllerSetups.ObjectDistanceDetection;

        Vector3 rayPosition = transform.position;
        Vector3 leftRayRotation = Quaternion.AngleAxis(-angle, transform.up) * transform.forward;
        Vector3 rightRayRotation = Quaternion.AngleAxis(angle, transform.up) * transform.forward;

        int stepDivider = (int)Mathf.Clamp((angle+1) / 2, 15, distance * 2);

        if (stepDivider == 0) stepDivider = 1;
        int step = (int)angle*2/stepDivider;
        RaycastHit hit;

        for (int i = -angle; i <= angle; i+= step)
        {
            Vector3 angled = Quaternion.AngleAxis(i, transform.up) * transform.forward;
            Debug.DrawRay(rayPosition, angled * distance);
            if (Physics.Raycast(rayPosition, angled, out hit, distance))
                if (hit.transform.tag.Equals("Enemy"))
                    return true;
        }

        return false;
    }

    public bool AreYouAtTheDoor()
    {
        return Vector3.Distance(transform.position, TGameManager.Instance.Door.position) < 3f;
    }

    public bool DoIHaveAmmunitionWithMe()
    {
        return _controllerSetups.CurrentAmunitionWithMe > 0;
    }

    public bool CheckAmmunition()
    {
        return _controllerSetups.CurrentAmunitionCount > 0;
    }

    public bool IsAmunitionInMinigun()
    {
        return TGameManager.Instance != null && TGameManager.Instance.CurrentMinigunAmmunition > 0;
    }

    public bool IsMinigunEnabled()
    {
        return GameManager.Instance != null && GameManager.Instance.MinigunEnabled;
    }

    public bool DoINeedReload()
    {
        float currentAmount = _controllerSetups.CurrentAmunitionCount;
        float maxAmmout = _controllerSetups.BulletsMagazineCount;

        return currentAmount / maxAmmout <= 0.15f;
    }
}
