﻿using UnityEngine;

public class TBulletLogic : MonoBehaviour {
    #region Serialized Fields
    [SerializeField, Range(0f, 10000f)]
    private float _bulletSpeed = 500f;

    [SerializeField, Range(-360,360)]
    private float _randomAngle = 0f;

    [SerializeField, Range(0f, 10f)]
    private float _bulletLifeTime = 3f;

    #endregion
    #region Non-Serialized Fields
    private Vector3 _moveDirection;
    private float _shortestSoFar;
    #endregion

    // Use this for initialization
    void Awake() {
        transform.Rotate((Random.value * 2f * _randomAngle) - _randomAngle, 0f, Random.value * 360f);
        _moveDirection = transform.forward * _bulletSpeed;
        _shortestSoFar = Mathf.Infinity;
    }

    // Update is called once per frame
    void Update() {
        transform.rotation = Quaternion.LookRotation(_moveDirection);
        RaycastHit[] hits  = Physics.RaycastAll(transform.position, transform.forward, _bulletSpeed * Time.deltaTime);

        foreach(RaycastHit hit in hits)
            if(hit.transform.tag == "Enemy" || hit.transform.tag == "Player")
                if(Vector3.Distance(transform.position, hit.point) < _shortestSoFar) {
                    _shortestSoFar = Vector3.Distance(transform.position, hit.point);
                    TGameManager.Instance.StopObjectAction(hit.transform.gameObject, true);
                    GameObject.Destroy(this.gameObject, 0.1f);
                    return;
                }

        transform.position  += _moveDirection    * Time.deltaTime;
        _moveDirection.y    += Physics.gravity.y * Time.deltaTime;
        _bulletLifeTime     -= Time.deltaTime;

        if(_bulletLifeTime < 0)
            GameObject.Destroy(gameObject, 0.1f);
    }
}
