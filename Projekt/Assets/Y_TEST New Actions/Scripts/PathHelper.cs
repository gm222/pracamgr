﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class PathHelper : MonoBehaviour {
    [SerializeField]
    Transform _enemyPoints;

    [SerializeField]
    Transform _aiPoints;

    public void OnDrawGizmos() {
        Transform enemyPoints = _enemyPoints;
        Transform aiPoints    = _aiPoints;

        Queue queueList = new Queue();
        List<Transform> pointsList = new List<Transform>();

        if(enemyPoints != null) {
            queueList.Enqueue(enemyPoints);

            Gizmos.color = Color.magenta;

            while(queueList.Count > 0) {
                Transform pointP = queueList.Dequeue() as Transform;

                if(pointP.childCount > 0)
                    for(int i = 0; i < pointP.childCount; i++)
                        queueList.Enqueue(pointP.GetChild(i));
                else
                    pointsList.Add(pointP);
            }

            for(int i = 0; i < pointsList.Count; i++) {
                Vector3 currentPoint = pointsList[i].position;
                Gizmos.DrawSphere(currentPoint, 0.4f);
                if(i > 0) {
                    Vector3 previousePoint = pointsList[i-1].position;
                    Gizmos.DrawLine(currentPoint, previousePoint);
                }
            }
        }

        pointsList.Clear();
        if(aiPoints != null) {
            Gizmos.color = Color.blue;
            queueList.Enqueue(aiPoints);

            while(queueList.Count > 0) {
                Transform pointP = queueList.Dequeue() as Transform;

                if(pointP.childCount > 0)
                    for(int i = 0; i < pointP.childCount; i++)
                        queueList.Enqueue(pointP.GetChild(i));
                else
                    pointsList.Add(pointP);
            }

            for(int i = 0; i < pointsList.Count; i++) {
                Vector3 currentPoint = pointsList[i].position;
                Gizmos.DrawSphere(currentPoint, 0.4f);
                if(i > 0) {
                    Vector3 previousePoint = pointsList[i-1].position;
                    Gizmos.DrawLine(currentPoint, previousePoint);
                }
            }
        }
    }
}
