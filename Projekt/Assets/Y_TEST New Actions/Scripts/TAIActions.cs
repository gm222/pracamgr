﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(TAIConditions))]
[RequireComponent(typeof(TController))]
[RequireComponent(typeof(NavMeshAgent))]
public class TAIActions : ActionBase
{

    #region Private Fields

    private bool _backwards = false;
    private int _lastPatrolPoint = -1;
    private NavMeshAgent _agent;
    private TAIConditions _aiConditions;
    private TController _controllerSetups;
    private Transform _bulletSpawnPoint;
    private Transform _rotationArmCenter;
    private Transform _rotator;
    private Vector3[] _aiPoints;

    #endregion Private Fields

    #region Public Methods
    //good
    public IEnumerator<BAD.NodeResult> Patrol()
    {
        if (_lastPatrolPoint == -1)
        {
            _lastPatrolPoint++;
            _agent.destination = (_aiPoints[_lastPatrolPoint]);
            _agent.Stop();
        }

        Vector3 charPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        float distance = 0f;
        bool haveSeenOtherObject = false;

        _agent.destination = _aiPoints[_lastPatrolPoint];

        _agent.Resume();
        do
        {
            distance                          = Vector3.Distance(transform.position, _aiPoints[_lastPatrolPoint]);
            float distanceToLastPositionCheck = Vector3.Distance(transform.position, charPosition);

            if (distanceToLastPositionCheck >= TGameManager.Instance.DistanceCkeck || (distanceToLastPositionCheck >= 0f && distanceToLastPositionCheck < 0.1f))
            {
                haveSeenOtherObject = _aiConditions.CheckEnemyInAngleRange();
                if (haveSeenOtherObject)
                {
                    _agent.Stop();
                    yield return BAD.NodeResult.Failure;
                }

                charPosition.x = transform.position.x;
                charPosition.y = transform.position.y;
                charPosition.z = transform.position.z;

            }
            yield return BAD.NodeResult.Continue;
        } while (distance >= _agent.stoppingDistance);

        _agent.Stop();
        _lastPatrolPoint = !_backwards ? _lastPatrolPoint + 1 : _lastPatrolPoint - 1;

        if (_lastPatrolPoint >= _aiPoints.Length)
        {
            _backwards = !_backwards;
            _lastPatrolPoint -= 2;
        }

        if (_lastPatrolPoint < 0)
        {
            _backwards = !_backwards;
            _lastPatrolPoint += 2;
        }

        _agent.destination = _aiPoints[_lastPatrolPoint];
        _agent.Stop();

        yield return BAD.NodeResult.Success;
    }

    //good
    public IEnumerator<BAD.NodeResult> GoToBunker()
    {

        Vector3 point = TGameManager.Instance.Bunker.position;

        point.y = transform.position.y;
        _agent.Stop();
        _agent.destination = point;
        _agent.Resume();
        float distance = Vector3.Distance(transform.position, point);

        if (distance >= TGameManager.Instance.CriticalRangeToBunker) yield return BAD.NodeResult.Failure;

        do
        {
            distance = Vector3.Distance(transform.position, point);
            if (_aiConditions.AreYouAtTheDoor())
            {
                if (TGameManager.Instance.AreDoorsClosed())
                {
                    _agent.Stop();
                    _agent.velocity = Vector3.zero;
                    IEnumerator<bool> doorsOpenAction = TGameManager.Instance.DoorsOpenCloseManipulation(TGameManager.DoorsManipulationOption.Open);
                    do
                    {
                        yield return BAD.NodeResult.Continue;
                    } while (doorsOpenAction.Current == false && doorsOpenAction.MoveNext());
                    _agent.destination = point;
                    _agent.Resume();
                }
            }
            yield return BAD.NodeResult.Continue;
        } while (distance >= _agent.stoppingDistance);

        _agent.velocity = Vector3.zero;
        _agent.Stop();

        IEnumerator<bool> doorsOpenAction2 = TGameManager.Instance.DoorsOpenCloseManipulation(TGameManager.DoorsManipulationOption.Close);

        do
        {
            yield return BAD.NodeResult.Continue;
        } while (doorsOpenAction2.Current == false && doorsOpenAction2.MoveNext());

        float s = Time.realtimeSinceStartup;

        while (Time.realtimeSinceStartup - s < 10f) yield return BAD.NodeResult.Continue;

        TGameManager.Instance.StopSimulation();

        yield return BAD.NodeResult.Failure;
    }

    //work
    public IEnumerator<BAD.NodeResult> ShotToEnemy()
    {

        if (!_aiConditions.CheckAmmunition()) yield return BAD.NodeResult.Failure;

        int angle = _controllerSetups.ObjectAngleDetection;
        float distance = _controllerSetups.ObjectDistanceDetection;

        if (distance == 0f) distance = 0.1f;

        Vector3 rayPosition = transform.position;
        float min = Mathf.Min(15, distance * 2f);
        float max = Math.Max(15, distance * 2f);
        int stepDivider = (int)Mathf.Clamp((angle+1) / 2, min, max);

        if (stepDivider == 0) stepDivider = 1;
        int step = (int)angle*2/stepDivider;
        RaycastHit hit;

        GameObject enemy = null;

        for (int i = -angle; i <= angle; i+= step)
        {
            Vector3 angled = Quaternion.AngleAxis(i, transform.up) * transform.forward;
            Debug.DrawRay(rayPosition, angled * distance);
            if (Physics.Raycast(rayPosition, angled, out hit, distance))
                if (hit.transform.tag.Equals("Enemy"))
                    enemy = hit.transform.gameObject;
        }

        if (enemy == null) yield return BAD.NodeResult.Failure;

        Vector3 enemyPosition = enemy.transform.position;
        Vector3 armPosition = _rotationArmCenter.position;
        Vector3 direction = (enemyPosition - armPosition);
        float lenght = direction.magnitude;

        if (lenght > _controllerSetups.ObjectDistanceDetection) yield return BAD.NodeResult.Failure;

        direction              = direction.normalized;
        Vector2 directionY = new Vector2(direction.x, direction.z);
        Vector3 directionX = new Vector3(direction.x, 0, direction.z);
        float angleY = Mathf.Atan2(directionY.y, directionY.x) * -Mathf.Rad2Deg + 90f;
        float angleX = Vector3.Angle(direction, directionX);

        if (direction.y > 0)
            angleX *= -1;

        Quaternion rotationY = Quaternion.AngleAxis(angleY, Vector3.up);
        Vector3 rotatorX = Vector3.right * angleX;

        while (Quaternion.Angle(transform.rotation, rotationY) > 0.2f)
        {
            transform.rotation     = Quaternion.Slerp(transform.rotation, rotationY, Time.fixedDeltaTime * _controllerSetups.RotationDamping);
            enemyPosition          = enemy.transform.position;
            armPosition            = _rotationArmCenter.position;
            direction              = (enemyPosition - armPosition);
            lenght                 = direction.magnitude;

            if (lenght > _controllerSetups.ObjectDistanceDetection) yield return BAD.NodeResult.Failure;

            direction              = direction.normalized;
            directionY             = new Vector2(direction.x, direction.z);
            angleY                 = Mathf.Atan2(directionY.y, directionY.x) * -Mathf.Rad2Deg + 90f;
            rotationY              = Quaternion.AngleAxis(angleY, Vector3.up);

            yield return BAD.NodeResult.Continue;
        }

        directionX = new Vector3(direction.x, 0, direction.z);
        angleX     = Vector3.Angle(direction, directionX);

        if (direction.y > 0)
            angleX *= -1;

        rotatorX                  = Vector3.right * angleX;
        _rotator.localEulerAngles = rotatorX;

        Shot();

        _controllerSetups.CurrentAmunitionCount--;
        yield return BAD.NodeResult.Success;
    }

    //work
    public IEnumerator<BAD.NodeResult> ReloadAmmunition()
    {
        if (!(_aiConditions.DoINeedReload() && _aiConditions.DoIHaveAmmunitionWithMe())) yield return BAD.NodeResult.Failure;

        DateTime dt = DateTime.Now + TimeSpan.FromSeconds(2f);

        do { yield return BAD.NodeResult.Continue; } while (DateTime.Now < dt);
        _controllerSetups.CurrentAmunitionCount = _controllerSetups.BulletsMagazineCount;
        _controllerSetups.CurrentAmunitionWithMe--;

        yield return BAD.NodeResult.Success;
    }

    //work
    public IEnumerator<BAD.NodeResult> ActivateMinigun()
    {
        if (!_aiConditions.IsAmunitionInMinigun()) yield return BAD.NodeResult.Failure;
        if (_aiConditions.IsMinigunEnabled()) yield return BAD.NodeResult.Failure;

        Vector3 point = TGameManager.Instance.MinigunPosition.position;
        point.y = transform.position.y;

        _agent.Stop();
        _agent.destination = point;
        _agent.Resume();

        float distance = 0f;

        do
        {
            distance = Vector3.Distance(transform.position, point);
            yield return BAD.NodeResult.Continue;
        } while (distance >= _agent.stoppingDistance || distance > 2f);

        _agent.velocity = Vector3.zero;
        _agent.Stop();

        float s = Time.realtimeSinceStartup;

        while (Time.realtimeSinceStartup - s < 0.5f) yield return BAD.NodeResult.Continue;
        TGameManager.Instance.MinigunEnabled = true;
        yield return BAD.NodeResult.Success;
    }

    //work
    public IEnumerator<BAD.NodeResult> RunAwayFromEnemy()
    {
        //stop agent
        _agent.Stop();
        _agent.velocity = Vector3.zero;

        //losowanie

        //tutaj lista wszystkich przeciwników
        //na razie statycznie

        NavMeshHit hit;
        bool hitTest = false;

        //do
        //{
        //    hitTest = false;
        //    Vector3 randomDirection = UnityEngine.Random.insideUnitSphere * TGameManager.Instance.WalkRadius;
        //    randomDirection += transform.position;


        //    NavMesh.SamplePosition(randomDirection, out hit, TGameManager.Instance.WalkRadius, 1);
        //    Vector3 finalPosition = hit.position;

        //    GameObject enemy = TGameManager.Instance.EnemiesPrefab;
        //    Vector3 enemyPosition = enemy.transform.position;
        //    if (!(finalPosition.x == enemyPosition.x && finalPosition.y == enemyPosition.y && finalPosition.z == enemyPosition.z))
        //        if (Vector3.Distance(finalPosition, enemyPosition) >= 12f)
        //            hitTest = true;
        //    yield return BAD.NodeResult.Continue;
        //} while (!hitTest);


        Vector3 randomDirection = UnityEngine.Random.insideUnitSphere * TGameManager.Instance.WalkRadius;
        randomDirection += transform.position;
        NavMesh.SamplePosition(randomDirection, out hit, TGameManager.Instance.WalkRadius, 1);
        Vector3 finalPosition = hit.position;

        _agent.destination = finalPosition;
        _agent.Resume();

        float distance = 0f;
        do
        {
            distance = Vector3.Distance(transform.position, _agent.destination);
            yield return BAD.NodeResult.Continue;
        } while (distance >= _agent.stoppingDistance);

        _agent.Stop();

        yield return BAD.NodeResult.Success;
    }

    #endregion Public Methods

    #region Private Methods

    // Use this for initialization
    private void Start()
    {
        _agent             = GetComponent<NavMeshAgent>();
        _controllerSetups  = GetComponent<TController>();
        _aiConditions      = GetComponent<TAIConditions>();
        Transform aiPoints = TGameManager.Instance.AIWayPoints;

        Transform[] childTransforms = gameObject.GetComponentsInChildren<Transform>(false);
        _bulletSpawnPoint = childTransforms.FirstOrDefault(child => child.CompareTag("Respawn") == true);

        _rotator = childTransforms.FirstOrDefault(child => child.CompareTag("ArmRotator") == true);
        _rotationArmCenter = childTransforms.FirstOrDefault(child => child.CompareTag("ArmPoint") == true);


        if (aiPoints != null)
        {
            List<Vector3> tmpMovePoints = new List<Vector3>();
            Queue queueList = new Queue();

            queueList.Enqueue(aiPoints);

            while (queueList.Count > 0)
            {
                Transform pointP = queueList.Dequeue() as Transform;

                if (pointP.childCount > 0)
                    for (int i = 0; i < pointP.childCount; i++)
                        queueList.Enqueue(pointP.GetChild(i));
                else
                    tmpMovePoints.Add(pointP.position);
            }

            _aiPoints = tmpMovePoints.ToArray();


        }
    }

    private void Shot()
    {
        if (TGameManager.Instance != null && TGameManager.Instance.Bullet != null)
        {
            Instantiate(TGameManager.Instance.Bullet, _bulletSpawnPoint.position, _bulletSpawnPoint.rotation);
            _controllerSetups.CurrentAmunitionCount--;
        }
    }

    public void ResetData()
    {
        try
        {
            _lastPatrolPoint = 0;
            _agent.Stop();
            _agent.destination = transform.position;
        }
        catch (Exception)
        {
            // ignored
        }
    }

    public void StopMoving()
    {
        if (_agent != null)
            _agent.Stop();
    }

    #endregion Private Methods
}