﻿using Assets.aochmann.NodeEditor.Scripts;
using Assets.aochmann.NodeEditor.Scripts.NodesUtilities;
using Assets.aochmann.Scripts.Other;
using Assets.aochmann.Scripts.Utilities;
using Assets.Test.Scripts.Data;
using BAD;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class TGameManager : MonoSingleton<TGameManager>
{

    #region Enums
    private enum SimulationState
    {
        Playing,
        Stopped,
        Initialize
    }
    public enum DoorsManipulationOption
    {
        Open,
        Close
    }
    #endregion

    #region Serialized Fields

    [SerializeField, Range(1, 1000000)]
    private int _step = 1;
    [SerializeField, Range(1, 1000000)]
    private double _simulationTemperature = 1d;

    [SerializeField]
    private int _simulationNumber = 0;
    [SerializeField]
    private double _lastSimulationCost = 0.0d;
    [SerializeField]
    private double _theBestCost = 0.0d;

    //############################################################## Another
    //####  HEADER CONTROL
    [Header("UNITY TIME MANAGER"), Space(10f)]
    //#### END HEADER CONTROL
    [SerializeField, Range(0.1f, 100f)]
    private float _tickDuration = 0.1f;

    //############################################################## Another
    //####  HEADER CONTROL
    [Header("Prefabs Informations"), Space(10f)]
    //#### END HEADER CONTROL

    [SerializeField]
    private Transform _AIRespawnPoint;

    [SerializeField]
    private GameObject _AIObject;

    [SerializeField]
    private GameObject _enemiesPrefab;

    [SerializeField]
    private Transform _enemiesRespawnPoint;

    [SerializeField]
    private Transform _AIWayPoints;

    [SerializeField]
    private Transform _EnemyWayPoints;

    [SerializeField]
    private Transform _minigunPatrolPoints;


    //############################################################## Another
    //####  HEADER CONTROL
    [Header("Ranges Informations"), Space(10f)]
    //#### END HEADER CONTROL
    [SerializeField, Range(0f, 20f)]
    private float _distanceCheck = 2f;

    [SerializeField, Range(0f, 100f)]
    private float _walkRadius = 20f;

    [SerializeField, Range(0f, 100f)]
    private float _minigunRangeDetection = 20f;

    [SerializeField, Range(0, 360)]
    private int _minigunAngle = 30;

    [SerializeField, Range(0f, 2000f)]
    private float _criticalRangeToBunker = 300f;

    //############################################################## Another
    //####  HEADER CONTROL
    [Header("Bunker Informations"), Space(10f)]
    //#### END HEADER CONTROL

    [SerializeField]
    private Transform _bunker;

    [SerializeField]
    private Transform _door;

    [SerializeField]
    private Transform _dorsPointOpen;

    [SerializeField]
    private Transform _dorsPointClose;

    [SerializeField, Range(0f, 100f)]
    private float _doorsSpeed = 10f;

    //############################################################## Another
    //####  HEADER CONTROL
    [Header("Guns Informations"), Space(10f)]
    //#### END HEADER CONTROL

    [SerializeField]
    private Transform _minigun;

    [SerializeField]
    private GameObject _bullet;

    [SerializeField, Range(0, 100)]
    private int _minigunAmmunition = 20;

    [SerializeField]
    private int _currentMinigunAmmunition;

    [SerializeField]
    private bool _minigunEnabled = false;



    //############################################################## Another
    //####  HEADER CONTROL
    [Header("Mutating Program Informations"), Space(10f)]
    //#### END HEADER CONTROL

    [SerializeField]
    private string _programFolderLocalization = string.Empty;

    [SerializeField]
    private string _programName = string.Empty;

    [SerializeField]
    private bool _manualRunning = false;

    //####  HEADER CONTROL
    [Header("Another Informations"), Space(10f)]
    //#### END HEADER CONTROL

    [SerializeField, Range(0, 1000)]
    private int _enemiesCount = 1;

    [SerializeField, Range(1, 300)]
    private int _maxSimulationTime = 30;

    #endregion Serialized Fields

    #region Non Serialized Fields
    //AI
    private BADReactor _aiBTReactor;
    private TController _aiControllerSetup;
    private TAIActions _aiActions;
    private int[] _aiNodeTypes;
    private Node _aiNodeRoot;
    private Node[] _aiNodeMapStructure;
    private float _simulationCost;
    private object[] _aiStructure;
    private bool _wasAIShootedOff;

    //Enemies
    private GameObject[] _enemies;
    private BADReactor[] _enemiesReactor;
    private TController[] _enemiesControllerSetup;
    private int _enemiesCountInitialized = 0;
    private int _enemiesFinished = 0;
    private TEnemyActions[] _enemiesActions;
    private Node[] _enemiesStageNodes;

    //Simulation
    private float _simulationStartTime;
    private SimulationState _simulationStateState;
    private bool _isBreakSet;
    private bool _doorClosed = true;
    private Process _mutationProgram;
    private readonly List<GameObject> _shotedOffEnemiesList = new List<GameObject>();
    private bool _first = false;
    private NamedPipeClientStream _namedPipeClient;

    #endregion Non Serialized Fields

    #region Properties

    public bool IsBreak
    {
        get { return _isBreakSet; }
        set { _isBreakSet = value; }
    }

    public float TickDuration
    {
        get { return _tickDuration; }
    }

    public GameObject Bullet
    {
        get { return _bullet; }
    }

    public Transform AIWayPoints
    {
        get { return _AIWayPoints; }
    }

    public Transform EnemyWayPoints
    {
        get { return _EnemyWayPoints; }
    }

    public float DistanceCkeck
    {
        get { return _distanceCheck; }
    }

    public Transform Bunker
    {
        get { return _bunker; }
    }

    public Transform Door
    {
        get { return _door; }
    }

    public float WalkRadius
    {
        get { return _walkRadius; }
    }

    public GameObject EnemiesPrefab
    {
        get { return _enemiesPrefab; }
    }

    public Transform MinigunPosition
    {
        get { return _minigun; }
    }

    public int MinigunAmmunition
    {
        get { return _minigunAmmunition; }
    }

    public int CurrentMinigunAmmunition
    {
        get { return _currentMinigunAmmunition; }
        set { _currentMinigunAmmunition=value; }
    }

    public bool MinigunEnabled
    {
        get { return _minigunEnabled; }
        set { _minigunEnabled=value; }
    }

    public float MinigunRangeDetection
    {
        get { return _minigunRangeDetection; }
    }

    public int MinigunAngle
    {
        get { return _minigunAngle; }
    }

    public Transform MinigunPatrolPoints
    {
        get { return _minigunPatrolPoints; }
    }

    public Transform NearestEnemy { get; set; }

    public float CriticalRangeToBunker
    {
        get { return _criticalRangeToBunker; }
    }
    #endregion Properties

    #region Private Methods

    //private void OnValidate()
    //{
    //    Time.timeScale = _tickDuration;
    //    Time.fixedDeltaTime = _tickDuration * 0.02f;
    //}

    private void SetPhysics(float tick)
    {
        Time.timeScale = tick;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;
    }

    private void Start()
    {
        SetNormalPhisicTick();
        if (_manualRunning == false)
        {
            bool testMutatingProgram = true;
            string programError = string.Empty;

            if (_programFolderLocalization.Length == 0)
            {
                testMutatingProgram = false;
                programError = "Program folder localization not set !";
            }
            else if (!Directory.Exists(_programFolderLocalization))
            {
                testMutatingProgram = false;
                programError = "Directory not exists !";
            }
            else if (_programName.Length == 0)
            {

                testMutatingProgram = false;
                programError = "Program name not set !";
            }
            else if (!File.Exists(_programFolderLocalization + "\\" + _programName))
            {
                testMutatingProgram = false;
                programError = "Program not exists !";
            }

            if (!testMutatingProgram)
            {
                EditorUtility.DisplayDialog("Error", programError, "OK");
                return;
            }
        }
        _currentMinigunAmmunition = _minigunAmmunition;
        //StartCoroutine(TestEnumarators());
        StartCoroutine(TMPGameLogic());
    }

    private void ConnectPipe()
    {
        _namedPipeClient = new NamedPipeClientStream(".", "test-pipe", PipeDirection.InOut);
        _namedPipeClient.Connect(250);
        _namedPipeClient.ReadMode = PipeTransmissionMode.Message;
        if (!_namedPipeClient.IsConnected) throw new Exception("Connection Exception with server");
        UnityEngine.Debug.Log("Client Connected");
    }

    private void DisconectPipe()
    {
        if (_namedPipeClient != null)
            _namedPipeClient.Dispose();
        _namedPipeClient = null;
    }

    private void SendInitialTreeToServer()
    {
        SendObjectToServer(_aiNodeTypes, _aiStructure);
    }

    private void InitializeAI()
    {
        //AI
        _aiBTReactor              = _AIObject.GetComponent<BAD.BADReactor>();
        _aiControllerSetup        = _AIObject.GetComponent<TController>();
        NodesContainer aiNodeContainer = _AIObject.GetComponent<NodesContainer>();

        aiNodeContainer.Initialize();
        _aiActions                = _AIObject.GetComponent<TAIActions>() ?? _AIObject.AddComponent<TAIActions>();
        _aiNodeRoot = _aiBTReactor.runningGraphs[0];
        int aiNodesCount = ScriptsHelper.GetNodesCount(_aiNodeRoot);
        _aiNodeTypes = new int[aiNodesCount];
        _aiStructure = new object[aiNodesCount];
        _aiNodeMapStructure = new Node[aiNodesCount];

        ScriptsHelper.SetNodeTypesArray(_aiNodeTypes, _aiNodeRoot);
        ScriptsHelper.SetBTStructure(_aiStructure, _aiNodeRoot);
        ScriptsHelper.SetNodeArrayStructure(_aiNodeMapStructure, _aiNodeRoot);
    }

    private void InitializeEnemies()
    {
        if (_enemiesCount == 0) return;
        //Enemy
        _enemiesCountInitialized  = _enemiesCount;
        _enemies                  = new GameObject[_enemiesCount];
        _enemiesReactor           = new BADReactor[_enemiesCount];
        _enemiesControllerSetup   = new TController[_enemiesCount];
        _enemiesActions = new TEnemyActions[_enemiesCount];
        _enemiesStageNodes = new Node[_enemiesCount];

        for (int i = 0; i < _enemiesCountInitialized; i++)
        {
            _enemies[i]                 = UnityEngine.Object.Instantiate<GameObject>(_enemiesPrefab);
            EnemyLogicContainer logic = _enemies[i].GetComponent<EnemyLogicContainer>();
            _enemiesReactor[i]         = _enemies[i].GetComponent<BADReactor>();
            _enemiesControllerSetup[i] = _enemies[i].GetComponent<TController>();
            _enemiesActions[i] = _enemies[i].GetComponent<TEnemyActions>()?? _enemies[i].AddComponent<TEnemyActions>();

            NodesUtilitiesRuntime.LoadGraph(ref logic);

            NodeGraph guiGraph = logic.EnemyLogics[i%logic.EnemyLogics.Count];
            Node root = logic.Parser(new Root(), guiGraph.StartupNode, _enemiesReactor[i]);
            _enemiesStageNodes[i] = root;

            _enemiesReactor[i].ClearRuningGraphs();
            _enemiesReactor[i].runningGraphs.Add(_enemiesStageNodes[i]);
        }
    }

    private void InitializeSimulation()
    {
        //simulation
        _simulationStateState = SimulationState.Initialize;
        _simulationStartTime = Time.realtimeSinceStartup;
        _mutationProgram = System.Diagnostics.Process.Start(_programFolderLocalization + "\\" + _programName);
    }

    private void SetNormalPhisicTick()
    {
        SetPhysics(1f);
    }

    private void SetCustomPhisicTick()
    {
        SetPhysics(_tickDuration);
    }

    private IEnumerator TMPGameLogic()
    {
        SetNormalPhisicTick();
        _simulationNumber = 0;
        _lastSimulationCost = 0.0d;
        InitializeAI();
        InitializeEnemies();
        if (_manualRunning == false)
        {
            InitializeSimulation();
            yield return new WaitForSeconds(0.4f);
        }

        ResetGMFields();

        yield return new WaitForSeconds(0.4f);

        ConnectPipe();
        SendingStepAndTemperatureToServer(_step, _simulationTemperature);
        yield return new WaitForSeconds(1.0f);

        ConnectPipe();
        yield return new WaitForSeconds(.3f);
        SendInitialTreeToServer();
        StartCoroutine(ServerReciveLoop());


        //test

        //EnemiesStartLogic();

        yield return null;
    }

    private void SendingStepAndTemperatureToServer(int step, double initialTemperature)
    {
        SendObjectToServer(_step, initialTemperature);
        DisconectPipe();
    }

    private IEnumerator ServerReciveLoop()
    {
        SetNormalPhisicTick();
        yield return null;
        yield return new WaitForSeconds(0.2f);
        _simulationNumber++;
        _shotedOffEnemiesList.Clear();
        bool canSimulate = true;
        object[] serverResponse = null;

        if (!_first)
        {
            serverResponse = GetMessageFromServer();

            if (serverResponse == null) throw new Exception("Server down");

            if (serverResponse[0] is bool)
            {
                //DisconectPipe();
                //yield return new WaitForSeconds(0.3f);
                //ConnectPipe();
                //yield return new WaitForSeconds(0.3f);

                //GetTheBestTreeFromServer();
                object[] tmpCast = serverResponse[1] as object[];

                if (tmpCast == null)
                    throw new Exception("This is not a tree");

                AITreeReset();
                BuildingAITree(tmpCast);

                _simulationStateState = SimulationState.Initialize;
                StartCoroutine(RealSimulation());
                canSimulate = false;
                yield break;
            }
        }

        if (canSimulate)
        {
            AITreeStateReset();

            if (!_first)
            {
                object[] tmpCastArray = serverResponse[0] as object[];

                if (tmpCastArray == null)
                    throw new Exception("This is not a tree");

                object[] serverTree = tmpCastArray;
                AITreeReset();
                BuildingAITree(serverTree);
                //first = !first;
            }

            IsBreak = false;
            _enemiesFinished = 0;

            ResetGMFields();

            _aiBTReactor.ClearRuningGraphs();
            _aiBTReactor.runningGraphs.Add(_aiNodeRoot);
            SetCustomPhisicTick();
            yield return null;
            yield return new WaitForSeconds(0.2f);
            _simulationStartTime = Time.realtimeSinceStartup;

            EnemiesStartLogic();
            _aiBTReactor.StartLogic();

            StartCoroutine(SimulationStatusControling());
        }

        yield break;
    }

    private void GetTheBestTreeFromServer()
    {

        object[] serverResponse = GetMessageFromServer();

        object[] tmpCast = serverResponse[0] as object[];

        if (tmpCast == null)
            throw new Exception("This is not a tree");

        AITreeReset();
        BuildingAITree(tmpCast);

        _simulationStateState = SimulationState.Initialize;
        StartCoroutine(RealSimulation());
    }

    private IEnumerator SimulationStatusControling()
    {
        float timeLimit = _maxSimulationTime;
        if (Time.timeScale < 1)
            timeLimit += _maxSimulationTime*(Time.timeScale+0.01f);
        else
            timeLimit /= Time.timeScale;

        if (timeLimit < 5f)
            timeLimit = 5f;

        while (!
            (
                IsBreak
                ||
                (_enemiesFinished >= _enemiesCountInitialized)
               ||
               (Time.realtimeSinceStartup - _simulationStartTime > timeLimit)
               )
            )
        {
            yield return null;
        }


        IsBreak = true;
        float stopTime = Time.realtimeSinceStartup;
        SetNormalPhisicTick();
        StopObjectAction(_AIObject);
        yield return new WaitForSeconds(0.4f);
        _simulationCost = CalcCost(_simulationStartTime, stopTime, _AIObject, _enemies);
        _lastSimulationCost = _simulationCost;
        SendObjectToServer(_simulationCost);

        yield return new WaitForSeconds(0.2f);

        StartCoroutine(ServerReciveLoop());

        yield break;
    }

    private IEnumerator RealSimulation()
    {
        while (true)
        {
            switch (_simulationStateState)
            {
                #region Case Logic

                case SimulationState.Playing:
                    if (IsBreak || _enemiesFinished >= _enemiesCountInitialized)
                    {
                        StopSimulation();
                        this._simulationStateState = SimulationState.Stopped;
                        yield return new WaitForSeconds(0.05f);
                        yield return null;
                    }
                    break;
                case SimulationState.Stopped:
                    {
                        float stopTime = Time.realtimeSinceStartup;
                        StopObjectAction(_AIObject);
                        SetNormalPhisicTick();
                        _simulationStateState = SimulationState.Initialize;
                        yield return new WaitForSeconds(4.2f);
                    }
                    break;
                case SimulationState.Initialize:
                    {
                        #region Initialize Logic

                        IsBreak = false;
                        _enemiesFinished = 0;
                        _shotedOffEnemiesList.Clear();
                        SetPosition(ref _AIObject, _AIRespawnPoint);

                        ResetGMFields();
                        AITreeStateReset();

                        _aiBTReactor.ClearRuningGraphs();
                        _aiBTReactor.runningGraphs.Add(_aiNodeMapStructure[0]);

                        _simulationStateState = SimulationState.Playing;
                        _simulationStartTime = Time.realtimeSinceStartup;
                        SetCustomPhisicTick();
                        EnemiesStartLogic();
                        _aiBTReactor.StartLogic();

                        #endregion
                    }
                    break;

                    #endregion
            }
            yield return null; //stop one frame
        }

        yield return null; //stop one frame
    }

    private float CalcCost(float startTime, float stopTime, GameObject aiObject, GameObject[] enemies)
    {
        float distance = 3f;

        var aiPosition = aiObject.transform.position;
        GameObject[] enemiesOnMap = enemies.Where(enemy => enemy.activeInHierarchy).ToArray();

        if (enemies != null && enemies.Length > 0)
        {
            Transform[] enemiesPosition = enemiesOnMap.Select(enemy => enemy.transform).ToArray();
            if (enemiesOnMap.Length > 0)
            {
                Vector3 nearestEnemyPosition = ScriptsHelper.SelectNearestPoint(aiPosition, enemiesPosition);
                distance = Vector3.Distance(aiPosition, nearestEnemyPosition);
            }
        }

        float playTime = stopTime - startTime;
        float playerAliveScore = _wasAIShootedOff == true ? 0 : playTime;
        float score =
            3*enemiesOnMap.Length +
            5*_shotedOffEnemiesList.Count +
            5/(playTime + 1) +
            4/(float)(_enemiesFinished + 1) +
            4/(distance + 1) +
            2*_aiControllerSetup.CurrentAmunitionCount +
            1*_aiControllerSetup.CurrentAmunitionWithMe +
            10/(playerAliveScore + 1);

        if (_theBestCost < score)
            _theBestCost = score;

        return score;
    }

    private void EnemiesStartLogic()
    {
        for (int i = 0; i < _enemiesCountInitialized; i++)
        {
            _enemiesReactor[i].ClearRuningGraphs();
            _enemiesReactor[i].Reset(_enemiesStageNodes[i]);
            _enemiesReactor[i].runningGraphs.Add(_enemiesStageNodes[i]);
        }

        ResetEnemiesData();

        for (int i = 0; i < _enemiesCountInitialized; i++)
            _enemiesReactor[i].StartLogic();
    }

    private void BuildingAITree(IList<object> serverTree)
    {
        if (_aiNodeMapStructure == null)
            throw new Exception("AI node map structure equals null");

        if (serverTree == null || serverTree.Count == 0)
            throw new Exception("Server Tree list null || length = 0");

        for (int serverListIndex = 0; serverListIndex < serverTree.Count; serverListIndex++)
        {
            List<int> childrenList = serverTree[serverListIndex] as List<int>;
            Branch nodeBranch = _aiNodeMapStructure[serverListIndex] as Branch;

            //Adding children
            if (nodeBranch == null || childrenList == null) continue;

            foreach (int childIndex in childrenList)
            {
                Node tmpNode = _aiNodeMapStructure[childIndex];
                tmpNode.parent = nodeBranch;
                nodeBranch.children.Add(tmpNode);
            }
        }
    }

    private void AITreeStateReset()
    {
        if (_aiNodeMapStructure == null)
            throw new Exception("AI node map structure equals null");

        //resetowanie danych rodzica
        foreach (Node aiNodeMap in _aiNodeMapStructure)
        {
            aiNodeMap.running = false;
            aiNodeMap.state = null;
        }
    }

    private void AITreeReset()
    {
        if (_aiNodeMapStructure == null)
            throw new Exception("AI node map structure equals null");

        //resetowanie danych rodzica
        foreach (Node aiNodeMap in _aiNodeMapStructure)
        {
            aiNodeMap.parent = null;
            var branch = aiNodeMap as Branch;
            if (branch != null)
            {
                branch.children.Clear();
            }
        }
    }

    private void ResetEnemiesData()
    {
        for (int i = 0; i < _enemiesCountInitialized; i++)
        {
            _enemiesControllerSetup[i].ResetData();
            _enemies[i].SetActive(false);
            SetPosition(ref _enemies[i], _enemiesRespawnPoint);
            _enemies[i].SetActive(true);
            //Enemies[i].transform.position += new Vector3(i*2, 0, 0);
            _enemies[i].transform.position += new Vector3(i/2 + i*0.5f, 0, 0);
            _enemiesReactor[i].Reset(_enemiesStageNodes[i]);
            _enemiesActions[i].ResetData();
        }
    }

    private void ResetGMFields()
    {
        _wasAIShootedOff          = false;
        _isBreakSet               = false;
        _minigunEnabled           = false;
        _currentMinigunAmmunition = _minigunAmmunition;
        _doorClosed               = true;
        _door.position            =_dorsPointClose.position;
        SetPosition(ref _AIObject, _AIRespawnPoint);
        _aiActions.ResetData();
        _aiControllerSetup.ResetData();
        _aiBTReactor.Reset(_aiNodeMapStructure[0]);
    }

    private void SetPosition(ref GameObject obj, Transform wantedPosition)
    {
        obj.SetActive(false);
        obj.transform.position = wantedPosition.position;
        obj.SetActive(true);
    }

    private void SendObjectToServer(params object[] objectsToSend)
    {
        if (_namedPipeClient == null || !_namedPipeClient.IsConnected)
            throw new Exception("Cant send msg to server !!!");

        HelpFunctions.PipeWrite(_namedPipeClient, objectsToSend);
    }

    private object[] GetMessageFromServer()
    {
        object[] serverResponse = HelpFunctions.PipeRead(_namedPipeClient) as object[];

        return serverResponse;
    }

    private void OnApplicationQuit()
    {
        StopAllCoroutines();

        if (_namedPipeClient != null && _namedPipeClient.IsConnected)
            _namedPipeClient.Dispose();
        _namedPipeClient = null;
        if (_mutationProgram != null)
            _mutationProgram.CloseMainWindow();
    }

    private IEnumerator<bool> DoorsManipulation(Vector3 point)
    {
        float distance = 0f;

        do
        {
            Vector3 doorsPosition = _door.position;
            Vector3 position = point - doorsPosition;
            distance = position.magnitude;
            _door.position = Vector3.Lerp(doorsPosition, point, Time.fixedDeltaTime * _doorsSpeed);
            yield return false;
        }
        while (distance >= 0.18f);
        yield return true;
    }

    #endregion Private Methods

    #region Public Methods

    public void StopObjectAction(GameObject obj, bool wasShuttedDown = false)
    {
        if (obj == _AIObject)
        {
            _minigunEnabled = false;
            IsBreak = true;
            _aiBTReactor.StopLocal();
            _aiActions.StopMoving();

            for (int i = 0; i < _enemiesCountInitialized; i++)
            {
                _enemiesReactor[i].StopLocal();
                _enemiesActions[i].StopMoving();
            }

            _wasAIShootedOff = wasShuttedDown;
        }
        else
        {
            int index = Array.IndexOf(_enemies, obj);
            if (index < 0) return;
            _enemiesReactor[index].StopLocal();
            if (wasShuttedDown)
                _shotedOffEnemiesList.Add(obj);

            _enemies[index].SetActive(false);
            _enemiesFinished++;
        }
    }

    public void StopSimulation()
    {
        StopObjectAction(_AIObject);
    }

    public IEnumerator<bool> DoorsOpenCloseManipulation(DoorsManipulationOption manipulationOption)
    {
        Vector3 doorsNextPosition = Vector3.zero;

        switch (manipulationOption)
        {
            case DoorsManipulationOption.Open:
                if (!AreDoorsClosed()) yield return true;
                doorsNextPosition = _dorsPointOpen.position;
                break;
            case DoorsManipulationOption.Close:

                if (AreDoorsClosed()) yield return true;
                doorsNextPosition = _dorsPointClose.position;
                break;
        }

        IEnumerator<bool> state = DoorsManipulation(doorsNextPosition);

        do
        {
            yield return false;
        } while (state.Current == false && state.MoveNext());

        switch (manipulationOption)
        {
            case DoorsManipulationOption.Open:
                _doorClosed = false;
                break;
            case DoorsManipulationOption.Close:
                _doorClosed = true;
                break;
            default:
                break;
        }


        yield return true;
    }

    public bool AreDoorsClosed()
    {
        return _doorClosed;
    }

    #endregion Public Methods
}