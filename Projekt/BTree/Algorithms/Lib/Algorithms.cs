﻿using Algorithms.Components;
using BTreePill.Objectives;
using BTreePill.Output;
using HelpFunctions;
using PillBase;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Pipes;
using System.Text;

namespace Algorithms.Lib
{
    //
    // Algorytmy
    //
    public class SimulatedAnnealing : Algorithm
    {
        Pill pill;
        Criterion criterion;
        static Random R = new Random(0); //constant SEED
        const double EPS = 1e-6;
        double temp;
        Solution currentSol, bestSol;

        MoveComponent moveComp;
        TemperatureComponent tempComp;
        StopComponent stopComp;

        public SimulatedAnnealing(MoveComponent moveComp, TemperatureComponent tempComp, StopComponent stopComp = null)
        {
            this.moveComp = moveComp;
            this.tempComp = tempComp;
            this.stopComp = stopComp;
        }

        public override Pill Run(Pill pill, Criterion criterion)
        {
            return null;
        }

        public string Run2(Pill pill, Criterion criterion)
        {
            bool initialRecive = true;
            StringBuilder textBuilder = new StringBuilder();
            ExampleObjectiveFunction objectiveFunction = null;
            int simulationStep = 0;
            try
            {
                using (
                    NamedPipeServerStream namedPipeServer = new NamedPipeServerStream("test-pipe", PipeDirection.InOut,
                        1, PipeTransmissionMode.Message))
                {
                    Console.WriteLine("Server waiting for a connection...");
                    namedPipeServer.WaitForConnection();
                    namedPipeServer.ReadMode = PipeTransmissionMode.Message;
                    Console.WriteLine("A client has connected");

                    if (initialRecive)
                    {

                        //############################# ODBIERANIE #############################
                        if (namedPipeServer.IsConnected)
                        {
                            object[] clientResponse = HelpFunction.PipeRead(namedPipeServer) as object[];

                            if (clientResponse != null && clientResponse.Length > 1)
                            {
                                BTreePill.Instance.BTreeInstance instance =
                                    pill.instance as BTreePill.Instance.BTreeInstance;
                                instance.Nodes = new List<BTreePill.Instance.Node>();

                                int[] nodeTypes = clientResponse[0] as int[];
                                for (int i = 0; i < nodeTypes.Length; i++)
                                    instance.Nodes.Add(new BTreePill.Instance.Node(nodeTypes[i]));


                                objectiveFunction = new BTreePill.Objectives.ExampleObjectiveFunction(instance);
                                pill.objectives.Add(objectiveFunction);
                                pill.constraints.Add(new BTreePill.Constraints.ParentConstraint());

                                pill.solution = new BTreePill.Solution.BTreeSolution(instance);
                                pill.solution.AddMove(new BTreePill.Output.BTreeOutputMove1(instance));
                                var output = pill.solution.output as BTreeOutput;
                                if (output != null) output.SetTree(clientResponse[1] as object[]);


                                Criterion criterion2 = new Criterion();
                                //utworzenie obiektu definiującego kryterium optymalizacji dla algorytmu
                                criterion2.optType = OptimizationType.Maximization;
                                criterion2.objectivesAggr = new ObjectiveAggregatorEx(OptimizationType.Maximization,
                                    pill.objectives);
                                double[] weights = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
                                //tablica wag poszczególnych ograniczeń - tyle elementów ile mamy ograniczeń
                                criterion2.constraintAggr = new ConstraintAggregatorEx(pill.constraints, weights);
                                //zdefiniowanie postaci agregatora ograniczeń

                                criterion = criterion2;

                                foreach (var item in pill.objectives)
                                    item.namedPipeServer = namedPipeServer;


                                Logger.PrintSolution(1, pill, criterion, this.GetType().ToString());

                                this.pill = pill;
                                this.criterion = criterion;

                                bestSol = pill.solution.Clone();
                                currentSol = pill.solution.Clone();
                                Init(pill);


                                initialRecive = !initialRecive;
                            }
                        }
                        //######################################################################
                    }

                    double bestSolutionCost = criterion.Value(bestSol);

                    textBuilder.Append(DateTime.Now);
                    textBuilder.Append(
                        $"{Environment.NewLine}{simulationStep};accept;{pill.solution.IsFeasible(pill.constraints)}; {criterion.Value(pill.solution)}");


                    bool bestSolutionFeasible = false;
                    //główna część kodu
                    while (namedPipeServer.IsConnected && !Stop(pill))
                    {
                        simulationStep++;
                        foreach (var item in pill.objectives)
                            item.namedPipeServer = namedPipeServer;

                        pill.solution = currentSol.Clone();
                        BTreeOutput treeOutput = (pill.solution.output as BTreeOutput);

                        treeOutput.wasCalculated = false;
                        treeOutput.wasPenaltySet = false;

                        moveComp.Move(pill, criterion);

                        Console.WriteLine($"Simulation: {simulationStep}, the best solution criterion value: {bestSolutionCost}");


                        if (Accepted(pill.solution, currentSol))
                        {
                            Logger.PrintSolution(1, pill, criterion, "accept");
                            bool currentSolutionFeasibleTest = pill.solution.IsFeasible(pill.constraints);
                            string text = $"{Environment.NewLine}{simulationStep};accept;{currentSolutionFeasibleTest}; {criterion.Value(pill.solution)}";
                            textBuilder.Append(text);
                            currentSol = pill.solution.Clone();

                            if (IsBetter(currentSol, bestSol))
                            {
                                bestSolutionFeasible = currentSolutionFeasibleTest;
                                bestSol = currentSol.Clone();
                                Debug.WriteLine("Nowe rozwi: " + criterion.Value(bestSol));
                                BTreeOutput bTreeOutput = bestSol.output as BTreeOutput;
                                if (bTreeOutput != null)
                                    bestSolutionCost = criterion.Value(bestSol);
                            }
                        }
                        else
                        {
                            pill.solution = currentSol;
                            //Logger.PrintSolution(1, pill, criterion, "not accept");
                        }

                    } //Algorithm while loop



                    if (!namedPipeServer.IsConnected)
                        Console.WriteLine("The client has ended the conversation.");
                    else
                    {

                        //############################# WYSYŁANIE ##############################
                        if (namedPipeServer.IsConnected)
                        {
                            object[] treeStructure = objectiveFunction.GetTreeStructure(
                                bestSol.output);
                            HelpFunction.PipeWrite(namedPipeServer, new object[] { true, treeStructure });
                            Console.WriteLine("Sending the best solution to unity");
                            Console.WriteLine("Sended");
                            /*
                              ExampleObjectiveFunction exampleObjectiveFunction = pill2Best.objectives[0] as BTreePill.Objectives.ExampleObjectiveFunction;
            if (exampleObjectiveFunction != null)
            {
                object[] treeStructure = exampleObjectiveFunction.GetTreeStructure(pill.solution.output); 
                             
                             */
                        }
                        //######################################################################

                    }
                    textBuilder.Append($"{Environment.NewLine}{simulationStep+1};accept;{bestSolutionFeasible};{bestSolutionCost}");
                    textBuilder.Append($"{Environment.NewLine}{DateTime.Now}");

                    //FileInfo fi = new FileInfo("simulation_results.txt");
                    //using (TextWriter txtWriter = new StreamWriter(fi.Open(FileMode.Create, FileAccess.ReadWrite)))
                    //{
                    //    txtWriter.Write(textBuilder.ToString());
                    //}

                    namedPipeServer.Disconnect();
                    LoggerSingleton.Instance.Save(textBuilder.ToString());

                }
            }
            catch (Exception ex)
            {
                textBuilder.Append($"{Environment.NewLine}{simulationStep+1};accept;{pill.solution.IsFeasible(pill.constraints)};{criterion.Value(pill.solution)}");
                textBuilder.Append(Environment.NewLine + DateTime.Now);
                //LoggerSingleton.Instance.Save(textBuilder.ToString());


            }

            pill.solution = bestSol;

            //Logger.PrintSolution(1, pill, criterion, "the best solution");

            return textBuilder.ToString();
        }



        public void Init(Pill pill)
        {
            temp = tempComp.InitialTemp();
            if (stopComp != null)
                stopComp.Init(pill);
        }

        public bool Stop(Pill pill)
        {
            if (stopComp != null)
                return stopComp.Stop(pill);
            else
                return temp < EPS;
        }

        public bool Accepted(Solution nextSol, Solution currentSol)
        {
            temp = tempComp.NextTemp(temp);
            //Console.WriteLine("temp: " + temp);

            if (criterion.IsBetter(nextSol, currentSol))
                return true;

            double diff;
            if (criterion.optType == OptimizationType.Minimization)
                diff = criterion.Value(currentSol) - criterion.Value(nextSol);
            else
                diff = criterion.Value(nextSol) - criterion.Value(currentSol);

            if (diff == 0)
                return false;
            else if (diff > 0)
            {
                return true;
            }
            else
            {
                double pr = R.NextDouble();
                double kr = Math.Exp(diff / temp);

                return pr < kr;
            }
        }

        public bool IsBetter(Solution newSol, Solution bestSol)
        {
            return criterion.IsBetter(newSol, bestSol);
        }
    }

    public class RestartAlgorithm : Algorithm
    {
        Algorithm inputAlg;
        StopComponent stopComp;
        Solution bestSol;
        Solution startSol;

        public RestartAlgorithm(Algorithm inputAlg, StopComponent stopComp)
        {
            this.inputAlg = inputAlg;
            this.stopComp = stopComp;
        }

        public override Pill Run(Pill pill, Criterion criterion)
        {
            startSol = pill.solution.Clone();
            bestSol = pill.solution.Clone();
            stopComp.Init(pill);

            while (!stopComp.Stop(pill))
            {
                pill.solution = startSol.Clone();
                pill = inputAlg.Run(pill, criterion);

                if (criterion.IsBetter(pill.solution, bestSol))
                {
                    bestSol = pill.solution;
                }
            }

            pill.solution = bestSol;

            return pill;
        }
    }

    public class EmptyAlgorithm : Algorithm
    {
        public override Pill Run(Pill pill, Criterion criterion)
        {
            return pill;
        }
    }

    public class RandomMovesAlgorithm : Algorithm
    {
        MoveComponent moveComp;
        int numMoves;

        public RandomMovesAlgorithm(MoveComponent moveComp, int numMoves)
        {
            this.moveComp = moveComp;
            this.numMoves = numMoves;
        }

        public override Pill Run(Pill pill, Criterion criterion)
        {
            for (int i = 0; i < numMoves; i++)
                moveComp.Move(pill, criterion);

            return pill;
        }

        public Pill[] Run(Pill[] pills, Criterion criterion)
        {
            for (int i = 0; i < pills.Length; i++)
                pills[i] = Run(pills[i], criterion);
            return pills;
        }
    }
}
