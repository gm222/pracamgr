﻿using System;
using System.Collections.Generic;

using PillBase;

namespace Algorithms.Lib
{
    //
    // Algorytmy populacyjne
    //
    public class ReplicationAlgorithm : AlgorithmMP
    {
        int populationSize;

        public ReplicationAlgorithm(int populationSize)
        {
            this.populationSize = populationSize;
        }

        public override Pill[] Run(Pill[] pills, Criterion criterion)
        {
            Pill[] newPills = new Pill[populationSize];
            int n = pills.Length;
            for (int i = 0; i < populationSize; i++)
            {
                newPills[i] = pills[i % n].Clone();
            }
            
            return newPills;
        }

        public Pill[] Run(Pill pill, Criterion criterion)
        {
            Pill[] pills = { pill };
            return Run(pills, criterion);
        }
    }

    public class ProbSelectionAlg : AlgorithmMP
    {
        int size;

        public ProbSelectionAlg(int size)
        {
            this.size = size;
        }

        public override Pill[] Run(Pill[] pills, Criterion criterion)
        {
            Logger.PrintPopulationAvg(1, pills, criterion, this.GetType().ToString());

            double[] values = new double[pills.Length];
            for (int i = 0; i < values.Length; i++)
                values[i] = criterion.Value(pills[i].solution);

            ProbDistribution dist = new ProbDistribution(values);

            Pill[] newPills = new Pill[size];

            for (int i = 0; i < newPills.Length; i++)
                newPills[i] = pills[dist.Sample()];

            Logger.PrintPopulationAvg(1, newPills, criterion);

            return newPills;
        }
    }

    public class StaticSelectionAlg : AlgorithmMP
    {
        int size;

        public StaticSelectionAlg(int size)
        {
            this.size = size;
        }

        public override Pill[] Run(Pill[] pills, Criterion criterion)
        {
            Logger.PrintPopulationAvg(1, pills, criterion, this.GetType().ToString());

            Pill p = pills[0];
            Pill[] newPills = new Pill[size];

            //Dictionary<Pill, double> values = new Dictionary<Pill, double>();

            Array.Sort(pills, delegate(Pill a, Pill b)
            {
                if (criterion.IsBetter(a.solution, b.solution))
                    return -1;
                else if (criterion.IsBetter(b.solution, a.solution))
                    return 1;
                else
                    return 0;
            });

            int n = pills.Length;
            for (int i = 0; i < newPills.Length; i++)
                newPills[i] = pills[i].Clone();

            Logger.PrintPopulationAvg(1, pills, criterion);

            return newPills;
        }
    }

    public class CrossoverAlg : AlgorithmMP
    {
        int crossNr;

        public CrossoverAlg(int crossNr)
        {
            this.crossNr = crossNr;
        }

        public override Pill[] Run(Pill[] pills, Criterion criterion)
        {
            Logger.PrintPopulationAvg(1, pills, criterion, this.GetType().ToString());

            // If population size is odd crossover is not applied to the last element
            Pill[] newPills = new Pill[pills.Length];
            for (int i = 0; i < newPills.Length; i++)
                newPills[i] = pills[i].Clone();

            for (int i = 0; i < pills.Length - 1; i += 2)
            {
                newPills[i].solution.Crossover(crossNr, pills[i].solution, pills[i + 1].solution);
                newPills[i + 1].solution.Crossover(crossNr, pills[i + 1].solution, pills[i].solution);
            }

            Logger.PrintPopulationAvg(1, newPills, criterion);

            return newPills;
        }
    }

    public class MutationAlg : AlgorithmMP
    {
        static Random R = new Random();
        double p;
        int moveNr;

        public MutationAlg(double p, int moveNr)
        {
            this.p = p;
            this.moveNr = moveNr;
        }

        // Mutation implemented as random move
        public override Pill[] Run(Pill[] pills, Criterion criterion)
        {
            Pill pill = pills[0];

            List<Pill> mutated = new List<Pill>();
            for (int i = 0; i < pills.Length; i++)
            {
                if (R.NextDouble() < p)
                {
                    pills[i].solution.Move(moveNr);

                    mutated.Add(pills[i]);
                }
            }

            return mutated.ToArray();
        }
    }

    
    //
    // Klasy pomocnicze
    //

    public class AdapterMP : AlgorithmMP
    {
        Algorithm alg;

        public AdapterMP(Algorithm alg)
        {
            this.alg = alg;
        }

        public override Pill[] Run(Pill[] pills, Criterion criterion)
        {
            for (int i = 0; i < pills.Length; i++)
                pills[i] = alg.Run(pills[i], criterion);
            
            return pills;
        }
    }


    class ProbDistribution
    {
        double[] normValues;
        static Random R = new Random();
        public ProbDistribution(double[] values)
        {
            int n = values.Length;
            normValues = new double[n];

            for (int i = 0; i < n; i++)
            {
                if (i == 0)
                    normValues[i] = values[i];
                else
                    normValues[i] = normValues[i - 1] + values[i];
            }

            double sum = normValues[n - 1];
            normValues = Array.ConvertAll(normValues, x => x / sum);
            // Make sure last element is equal to 1.0 in case of precision errors
            normValues[n - 1] = 1.0;
        }


        // Sampling in time O(log n) using binary search
        public int Sample()
        {
            double p = ProbDistribution.R.NextDouble();
            int index = Array.BinarySearch(normValues, p);
            if (index < 0)
                index = ~index;
            return index;
        }

        // Slow version of sampling with running time O(n)
        // Might be useful for testing
        public int SampleSlow()
        {
            double p = ProbDistribution.R.NextDouble();
            for (int i = 0; i < normValues.Length; i++)
            {
                if (p <= normValues[i])
                    return i;
            }
            throw new Exception();
        }

    }

}
