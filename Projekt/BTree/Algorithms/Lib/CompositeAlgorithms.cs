﻿using System;

using PillBase;
using Algorithms.Composition;
using Algorithms.Components;

namespace Algorithms.Lib
{
    public class MemeticAlgorithm : Algorithm
    {
        Algorithm improveAlg;
        int maxSteps;
        int populationSize;

        public MemeticAlgorithm(Algorithm improveAlg, int maxSteps, int populationSize)
        {
            this.improveAlg = improveAlg;
            this.maxSteps = maxSteps;
            this.populationSize = populationSize;
        }

        public override Pill Run(Pill pill, Criterion criterion)
        {
            Pill[] pills = { pill };
            
            pills = new ReplicationAlgorithm(populationSize).Run(pills, criterion);
            pills = new RandomMovesAlgorithm(new SingleRandomMove(0), 20).Run(pills, criterion);
            pills = new AdapterMP(new SimulatedAnnealing(new SingleRandomMove(0), new DefaultTemperatureComponent(10, 1.0), new MaxSteps(500))).Run(pills, criterion);
            pills[0] = pill;

            pills = new MemeticAlgorithmMP(improveAlg, maxSteps).Run(pills, criterion);
            pills = new StaticSelectionAlg(1).Run(pills, criterion);

            return pills[0];    
        }   
    }

    public class MemeticAlgorithmMP : AlgorithmMP
    {
        int maxSteps;
        Algorithm improveAlg;

        public MemeticAlgorithmMP(Algorithm improveAlg, int maxSteps)
        {
            this.maxSteps = maxSteps;
            this.improveAlg = improveAlg;
        }

        public override Pill[] Run(Pill[] pills, Criterion criterion)
        {
            ParallelComposition alg = Create(pills.Length, criterion);
            
            for (int i = 0; i < maxSteps; i++)
            {
                pills = alg.Run(pills, criterion);
            }

            return pills;
        }

        ParallelComposition Create(int size, Criterion criterion)
        {
            double mutationRate = 0.6;
            int crossNr = 0;

            CompositionNode[] nodes = {
                new CompositionNode(),
                new CompositionNode(new ProbSelectionAlg(size), criterion),
                new CompositionNode(new CrossoverAlg(crossNr), criterion),
                new CompositionNode(improveAlg, criterion),
                new CompositionNode(new MutationAlg(mutationRate, 0), criterion),
                new CompositionNode(improveAlg, criterion),
                new CompositionNode(new StaticSelectionAlg(size), criterion)
            };

            CompositionGraph g = new CompositionGraph(nodes[0], nodes[nodes.Length - 1]);
            for (int i = 1; i < nodes.Length - 1; i++)
                g.AddNode(nodes[i]);

            g.AddEdge(nodes[0], nodes[6]);

            g.AddEdge(nodes[0], nodes[1]);
            g.AddEdge(nodes[1], nodes[2]);
            g.AddEdge(nodes[2], nodes[3]);
            g.AddEdge(nodes[3], nodes[6]);

            g.AddEdge(nodes[0], nodes[4]);
            g.AddEdge(nodes[4], nodes[5]);
            g.AddEdge(nodes[5], nodes[6]);

            ParallelComposition a = new ParallelComposition(g, nodes);

            return a;
        }
    }

    public class EvoAlgorithm : Algorithm
    {
        int maxSteps;
        int populationSize;

        public EvoAlgorithm(int maxSteps, int populationSize)
        {
            this.maxSteps = maxSteps;
            this.populationSize = populationSize;
        }

        public override Pill Run(Pill pill, Criterion criterion)
        {
            Pill[] pills = { pill };

            pills = new ReplicationAlgorithm(populationSize).Run(pills, criterion);
            pills = new RandomMovesAlgorithm(new SingleRandomMove(0), 100).Run(pills, criterion);
            //            pills = new AdapterMP(new SimulatedAnnealing(new SingleRandomMove(0), new DefaultTemperatureComponent(1000, 1.0), new MaxSteps(100))).Run(pills, criterion);
            pills[0] = pill;

            pills = new EvolutionaryAlgorithmMP(maxSteps).Run(pills, criterion);
            pills = new StaticSelectionAlg(1).Run(pills, criterion);

            return pills[0];
        }
    }

    public class EvolutionaryAlgorithmMP : AlgorithmMP
    {
        int maxSteps;

        public EvolutionaryAlgorithmMP(int maxSteps)
        {
            this.maxSteps = maxSteps;
        }

        public override Pill[] Run(Pill[] pills, Criterion criterion)
        {
            ParallelComposition alg = Create(pills.Length, criterion);

            for (int i = 0; i < maxSteps; i++)
            {
                pills = alg.Run(pills, criterion);
            }

            return pills;
        }

        ParallelComposition Create(int size, Criterion criterion)
        {
            double mutationRate = 0.1;
            int crossNr = 0;

            CompositionNode[] nodes = {
                new CompositionNode(),
                new CompositionNode(new ProbSelectionAlg(size), criterion),
                new CompositionNode(new CrossoverAlg(crossNr), criterion),
                new CompositionNode(new MutationAlg(mutationRate, 0), criterion),
                new CompositionNode(new StaticSelectionAlg(size), criterion)
            };

            CompositionGraph g = new CompositionGraph(nodes[0], nodes[nodes.Length - 1]);
            for (int i = 1; i < nodes.Length - 1; i++)
                g.AddNode(nodes[i]);

            g.AddEdge(nodes[0], nodes[4]);

            g.AddEdge(nodes[0], nodes[1]);
            g.AddEdge(nodes[1], nodes[2]);
            g.AddEdge(nodes[2], nodes[3]);
            g.AddEdge(nodes[3], nodes[4]);

            ParallelComposition a = new ParallelComposition(g, nodes);

            return a;
        }
    }

}