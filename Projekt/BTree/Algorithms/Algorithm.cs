﻿using PillBase;
using System;
using System.IO;

namespace Algorithms
{
    public abstract class Algorithm
    {
        public abstract Pill Run(Pill pill, Criterion criterion);
    }

    public abstract class AlgorithmMP
    {
        public abstract Pill[] Run(Pill[] pills, Criterion criterion);
    }

    public class LoggerSingleton
    {
        public static LoggerSingleton Instance { get; } = new LoggerSingleton();
        public void Save(string text)
        {
            using (FileStream fs = File.Create(@"C:\simulation_results.txt"))
            {
                using (StreamWriter tw = new StreamWriter(fs))
                {
                    tw.Write(text);
                }
            }
        }

    }

    public class Logger
    {
        static int priority = 1;

        public static void PrintSolution(int priority, Pill pill, Criterion criterion, string label = "")
        {
            if (!CheckPriority(priority))
                return;

            PrintLabel(label);

            string msg = "not feasible";
            if (pill.solution.IsFeasible(pill.constraints))
                msg = "feasible";
            Console.WriteLine(criterion.Value(pill.solution) + " " + msg);
        }

        public static void PrintPopulationAvg(int priority, Pill[] pills, Criterion criterion, string label = "")
        {
            if (!CheckPriority(priority))
                return;

            PrintLabel(label);

            if (pills.Length == 0)
            {
                Console.WriteLine("Avg: empty");
                return;
            }

            Pill p = pills[0];
            double sum = 0;
            foreach (Pill pill in pills)
                sum += criterion.Value(pill.solution);
            Console.WriteLine("Avg: " + sum / pills.Length);
        }

        public static void PrintPopulation(int priority, Pill[] pills, Criterion cr)
        {
            if (!CheckPriority(priority))
                return;

            Pill p = pills[0];
            foreach (Pill pill in pills)
                Console.WriteLine(cr.Value(pill.solution));
        }

        static void PrintLabel(string label)
        {
            Console.Write(label + ": ");
        }

        static bool CheckPriority(int priority)
        {
            return priority >= Logger.priority;
        }
    }
}