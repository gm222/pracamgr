﻿using System;

using PillBase;

namespace Algorithms.Components
{
    //
    // Komponenty
    //
    public interface StopComponent
    {
        void Init(Pill pill);
        bool Stop(Pill pill);
    }

    public class MaxSteps : StopComponent
    {
        int maxSteps;
        int steps;

        public MaxSteps(int maxSteps)
        {
            this.maxSteps = maxSteps;
        }

        public void Init(Pill pill)
        {
            steps = 0;
        }

        public bool Stop(Pill pill)
        {
            steps++;
            if (steps%1000 == 0) System.Diagnostics.Debug.WriteLine("Iter " + steps.ToString());
            return steps > maxSteps;
        }
    }


    public class MaxAccepts : StopComponent
    {
        Solution currentSol;
        int maxUpdates;
        int maxSteps;
        int updates;
        int steps;

        public MaxAccepts(int maxAccepts, int maxSteps)
        {
            this.maxUpdates = maxAccepts;
            this.maxSteps = maxSteps;
        }

        public void Init(Pill pill)
        {
            this.currentSol = pill.solution;
            this.updates = 0;
            this.steps = 0;
        }

        public bool Stop(Pill pill)
        {
            Solution currentSol = pill.solution;
            steps++;

            if (this.currentSol != currentSol)
            {
                updates++;
                this.currentSol = currentSol;
            }

            return (updates >= maxUpdates) || (steps > maxSteps);
        }
    }

    public class MaxStepsWithoutAccept : StopComponent
    {
        Solution currentSol;
        int maxSteps;
        int steps;

        public MaxStepsWithoutAccept(int maxSteps)
        {
            this.maxSteps = maxSteps;
        }

        public void Init(Pill pill)
        {
            this.currentSol = pill.solution;
            this.steps = 0;
        }

        public bool Stop(Pill pill)
        {
            Solution currentSol = pill.solution;

            if (this.currentSol == currentSol)
            {
                steps++;
            }
            else
            {
                //Console.WriteLine("moves " + steps);
                this.currentSol = currentSol;
                steps = 0;
            }

            return steps > maxSteps;
        }
    }

    public class MaxStepsWithoutAccept2 : StopComponent
    {
        Solution currentSol;
        int maxSteps;
        int maxSteps2;
        int steps;
        int totalSteps;

        public MaxStepsWithoutAccept2(int maxSteps, int maxSteps2)
        {
            this.maxSteps = maxSteps;
            this.maxSteps2 = maxSteps2;
        }

        public void Init(Pill pill)
        {
            this.currentSol = pill.solution;
            this.steps = 0;
            this.totalSteps = 0;
        }

        public bool Stop(Pill pill)
        {
            Solution currentSol = pill.solution;

            if (this.currentSol == currentSol)
            {
                steps++;
                totalSteps++;
            }
            else
            {
                //Console.WriteLine("moves " + steps);
                this.currentSol = currentSol;
                steps = 0;
                totalSteps++;
            }
            if (totalSteps % 100 == 0) System.Diagnostics.Debug.WriteLine("Iter " + totalSteps.ToString());
            return (steps > maxSteps) || (totalSteps > maxSteps2);
        }
    }

    public class UntilFeasible : StopComponent
    {
        Pill pill;

        public void Init(Pill pill)
        {
            this.pill = pill;
        }

        public bool Stop(Pill pill)
        {
            return pill.solution.IsFeasible(pill.constraints);
        }
    }

    public interface TemperatureComponent
    {
        double InitialTemp();
        double NextTemp(double temp);
    }

    public class DefaultTemperatureComponent : TemperatureComponent
    {
        double initialTemp;
        double tempMultiplier;

        public DefaultTemperatureComponent(double initialTemp, double tempMultiplier)
        {
            this.initialTemp = initialTemp;
            this.tempMultiplier = tempMultiplier;
        }

        public double InitialTemp()
        {
            return initialTemp;
        }

        public double NextTemp(double temp)
        {
            return temp * tempMultiplier;
        }
    }

    public interface MoveComponent
    {
        void Move(Pill pill, Criterion criterion);
    }

    public class SingleRandomMove : MoveComponent
    {
        int moveNr;

        public SingleRandomMove(int moveNr)
        {
            this.moveNr = moveNr;
        }

        public void Move(Pill pill, Criterion criterion)
        {
            pill.solution.Move(moveNr);
        }
    }

    public class MultiRandomMove : MoveComponent
    {
        static Random R = new Random();

        public void Move(Pill pill, Criterion criterion)
        {
            int moveNr = R.Next(pill.solution.MoveCount);
            pill.solution.Move(moveNr);
        }
    }

    public class RoundRobinMove : MoveComponent
    {
        int moveNr;

        public RoundRobinMove()
        {
            moveNr = -1;
        }

        public void Move(Pill pill, Criterion criterion)
        {
            moveNr = (moveNr + 1) % pill.solution.MoveCount;
            pill.solution.Move(moveNr);
        }
    }
    
}

