﻿using System;
using System.Collections.Generic;

using PillBase;



namespace Algorithms.Composition
{
    class CompositionNode
    {
        public Pill[] pills;
        Algorithm alg;
        AlgorithmMP algMP;
        Criterion criterion;

        // Node with empty functionality
        public CompositionNode()
        {
        }

        public CompositionNode(Algorithm alg, Criterion criterion)
        {
            this.alg = alg;
            this.criterion = criterion;
        }

        public CompositionNode(AlgorithmMP algMP, Criterion criterion)
        {
            this.algMP = algMP;
            this.criterion = criterion;
        }

        public Pill[] Eval()
        {
            if (alg != null)
            {
                for (int i = 0; i < pills.Length; i++)
                    pills[i] = alg.Run(pills[i], criterion);
            }
            else if (algMP != null)
            {
                pills = algMP.Run(pills, criterion);
            }

            // Zerowanie wierzcholka - potrzebne przy powrotach do tego samego wierzcholka
            Pill[] retPills = pills;
            pills = null;
            return retPills;
        }

        public void AddPills(Pill[] addPills)
        {
            if (pills == null)
                pills = addPills;
            else
                pills = Join(pills, addPills);
        }

        Pill[] Join(Pill[] p1, Pill[] p2)
        {
            Pill[] result = new Pill[p1.Length + p2.Length];
            p1.CopyTo(result, 0);
            p2.CopyTo(result, p1.Length);
            return result;
        }
    }

    class CompositionGraph : Graph<CompositionNode>
    {
        public CompositionGraph(CompositionNode startNode, CompositionNode endNode)
            : base(startNode, endNode)
        {
        }
    }



    class ParallelComposition : AlgorithmMP
    {
        CompositionGraph g;
        CompositionNode[] orderedNodes;

        public ParallelComposition(CompositionGraph g, CompositionNode[] orderedNodes)
        {
            this.g = g;
            this.orderedNodes = orderedNodes;
        }

        public override Pill[] Run(Pill[] pills, Criterion criterion)
        {
            orderedNodes[0].pills = pills;
            foreach (CompositionNode node in orderedNodes)
            {
                pills = node.Eval();
                List<CompositionNode> outNodes = g.GetOutNodes(node);
                for (int i = 0; i < outNodes.Count; i++)
                {
                    if (i == 0)
                        outNodes[i].AddPills(pills);
                    else
                        outNodes[i].AddPills(Clone(pills));
                }
            }

            return pills;
        }

        Pill[] Clone(Pill[] pills)
        {
            Pill[] newPills = new Pill[pills.Length];
            for (int i = 0; i < pills.Length; i++)
                newPills[i] = pills[i].Clone();
            return newPills;
        }
    }


    class ConditionalComposition : AlgorithmMP
    {
        CompositionGraph g;
        Dictionary<Tuple<CompositionNode, CompositionNode>, Condition> conditions = new Dictionary<Tuple<CompositionNode, CompositionNode>, Condition>();

        public ConditionalComposition(CompositionGraph g)
        {
            this.g = g;
        }

        public void AddCondition(CompositionNode node1, CompositionNode node2, Condition cond)
        {
            conditions[new Tuple<CompositionNode, CompositionNode>(node1, node2)] = cond;
        }

        public override Pill[] Run(Pill[] pills, Criterion criterion)
        {
            CompositionNode startNode = g.GetStartNode();

            startNode.pills = pills;

            CompositionNode node = startNode;
            CompositionNode nextNode;
            while (true)
            {
                pills = node.Eval();

                nextNode = NextNode(node);
                if (nextNode != null)
                {
                    nextNode.AddPills(pills);
                    node = nextNode;
                }
                else
                    break;
            }

            return pills;
        }

        CompositionNode NextNode(CompositionNode node)
        {
            Condition cond;
            List<CompositionNode> outNodes = g.GetOutNodes(node);
            CompositionNode outNode;
            for (int i = 0; i < outNodes.Count; i++)
            {
                outNode = outNodes[i];
                if (conditions.TryGetValue(new Tuple<CompositionNode, CompositionNode>(node, outNode), out cond))
                {
                    if (cond.Satisfied())
                        return outNode;
                }
                else
                    return outNode;
            }
            return null;
        }
    }

    interface Condition
    {
        bool Satisfied();
    }

    class ProbCondition : Condition
    {
        static Random R = new Random();
        double p;

        public ProbCondition(double p)
        {
            this.p = p;
        }

        public bool Satisfied()
        {
            return R.NextDouble() < p;
        }
    }

    class IterCondition : Condition
    {
        int maxIter;
        int iter;

        public IterCondition(int maxIter)
        {
            this.maxIter = maxIter;
            iter = 0;
        }

        public bool Satisfied()
        {
            iter++;
            return iter <= maxIter;
        }
    }





    //
    // Generic graphs
    //
    class Graph<T>
    {
        List<T> nodes;
        protected Dictionary<T, List<T>> outNodes;
        T startNode;
        T endNode;

        public Graph(T startNode, T endNode)
        {
            nodes = new List<T>();
            outNodes = new Dictionary<T, List<T>>();

            this.startNode = startNode;
            this.endNode = endNode;
            AddNode(startNode);
            AddNode(endNode);
        }

        public void AddNode(T node)
        {
            nodes.Add(node);
            outNodes[node] = new List<T>();
        }

        public void AddEdge(T node1, T node2)
        {
            outNodes[node1].Add(node2);
        }

        public List<T> GetOutNodes(T node)
        {
            return outNodes[node];
        }

        public T GetStartNode()
        {
            return startNode;
        }

        public T GetEndNode()
        {
            return endNode;
        }
    }

}