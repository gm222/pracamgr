﻿using System;
using System.Collections.Generic;

namespace PillBase
{
    /// <summary>
    /// Klasa definiuje instancję problemu dla której szukane jest rozwiązanie.  
    /// </summary>
    public abstract class Instance
    {
    }

    /// <summary>
    /// Klasa definiuje abstrakcyjne rozwiązanie problemu. 
    /// 
    /// Rozwiązanie składa się z dwóch komponentów: obiektu Output oraz obiektu Representation
    /// (oba to klasy abstrakcyjne).
    /// W klasie pochodnej określamy właściwe klasy pochodne dla Output i Representation oraz     
    /// definiujemy implementację metody Clone.
    /// Dopuszczamy możliwość definiowania rozwiązania tylko za pomocą klasy Output (bez
    /// Representation).
    /// 
    /// Klasa Output definiuje rozwiązanie w postaci wyjściowej (zdekodowanej) - w formie właściwej
    /// dla użytkownika lub interfejsu. Jest to postać zbliżona do opisu rozwiązania w języku 
    /// naturalnym i nie wymagająca interpretacji. Zakładamy, że będzie to obiekt przekazywany do 
    /// interfejsu użytkownika.
    /// 
    /// Klasa Representation definiuje reprezentację (np. kombinatoryczną) rozwiązania - w formie 
    /// właściwej dla definiowania i wykonywania modyfikacji rozwiązania - tworząc przestrzeń
    /// rozwiązań dla algorytmów.
    /// 
    /// Na potrzeby algorytmów generycznych dla rozwiązania definiowane są  
    /// listy ruchów losowych (randommoves) - dla algorytmów przeszukiwania lokalnego,
    /// oraz operacje krzyżowania (crossovers) - dla algorytmów ewolucyjnych.
    /// Odpowiednie operacje wykonywane są na rzecz obiektu rozwiązania poprzez metody Move
    /// i Crossover.
    /// </summary>
    public abstract class Solution
    {
        public Output output;
        public Representation representation;
        protected List<RandomMove> randommoves = new List<RandomMove>();
        protected List<CrossoverBase> crossovers = new List<CrossoverBase>();

        // Dodanie ruchu r do listy ruchów
        public void AddMove(RandomMove r)
        {
            randommoves.Add(r);
        }
        // Wykonanie ruchu i z listy ruchów
        public void Move(int i)
        {
            randommoves[i].Move(this);
        }
        // Pobranie liczby ruchów na liście
        public int GetMoveCount()
        {
            return randommoves.Count;
        }
        // Pobranie liczby ruchów na liście
        public int MoveCount
        {
            get
            {
                return randommoves.Count;
            }
        }
        // Dodanie operatora krzyżowania do listy
        public void AddCrossover(CrossoverBase c)
        {
            crossovers.Add(c);
        }
        // Wykonanie i-tegi operatora krzyżowania rozwiązań s1 i s2
        public void Crossover(int i, Solution s1, Solution s2)
        {
            Solution s = crossovers[i].Crossover(s1, s2);
            representation = s.representation;
            output = s.output;
        }
        // Pobranie liczby operatorów krzyżowania na liście
        public int GetCrossoverCount()
        {
            return crossovers.Count;
        }
        // Pobranie liczby operatorów krzyżowania na liście
        public int CrossoverCount
        {
            get
            {
                return crossovers.Count;
            }
        }
        // Zwrócenie głębokiej kopii obiektu 
        public abstract Solution Clone();
        // Weryfikacja dopuszczalności rozwiązania względem przekazanej listy ograniczeń
        public bool IsFeasible(List<Constraint> constraints)
        {
            bool retVal = true;
            for (int i = 0; i < constraints.Count; i++)
                retVal &= constraints[i].IsFeasible(this);
            return retVal;
        }
    }

    /// <summary>
    /// Abstrakcyjna klasa postaci wyjściowej rozwiązania. Klasa pochodna musi zdefiniować właściwą
    ///  postać oraz metodę Clone.
    /// </summary>
    public abstract class Output
    {
        // Zwrócenie głębokiej kopii obiektu
        public abstract Output Clone();
    }

    /// <summary>
    /// Abstrakcyjna klasa reprezentacji rozwiązania. Klasa pochodna musi zdefiniować właściwą postać,
    /// metodę Clone. 
    /// Abstrakcyjne metody TranslateTo i TranslateFrom definiują sposób dekodowania postaci wyjściowej 
    /// z representacji (TranslateTo)
    /// oraz kodowania do postaci reprezentacji napodstawie postaci wyjściowej (TranslateFrom). Ich   
    /// postać ustalana jest w klasie pochodnej.
    /// </summary>
    public abstract class Representation
    {
        public abstract Representation Clone();
        public abstract void TranslateTo(Output s);
        public abstract void TranslateFrom(Output s);
    }

    /// <summary>
    /// Abstrakcyjna klasa bazowa dla losowego ruchu wykonanego na rozwiązaniu (Solution)
    /// </summary>
    public abstract class RandomMove
    {
        public abstract void InternalMove(Solution s);
        public void Move(Solution s)
        {
            InternalMove(s);
        }
    }

    /// <summary>
    /// Abstrakcyjna klasa bazowa dla losowego ruchu wykonanego na reprezentacji rozwiązania.
    /// Dba o synchronizację między Output i Representation.
    /// 
    /// Klasa pochodna definiuje metodę Move(Representation r)
    /// </summary>
    public abstract class RepresentationMove : RandomMove
    {
        public override void InternalMove(Solution s)
        {
            Move(s.representation);
            s.representation.TranslateTo(s.output);
        }
        public abstract void Move(Representation r);
    }

    /// <summary>
    /// Abstrakcyjna klasa bazowa dla losowego ruchu wykonanego na postaci wyjściowej rozwiązania.
    /// Dba o synchronizację między Output i Representation (jeśli jest zdefiniowane).
    /// 
    /// Klasa pochodna definiuje metodę Move(Output o)
    /// </summary>
    public abstract class OutputMove : RandomMove
    {
        public override void InternalMove(Solution s)
        {
            Move(s.output);
            if (s.representation != null) s.representation.TranslateFrom(s.output);
        }
        public abstract void Move(Output o);
    }

    /// <summary>
    /// Interfejs do wykonania skryptu po stronie VBA.
    /// </summary>
    public interface IMoveScriptEvaluator
    {
        void EvaluateMove(Instance instance, Representation representation);
    }
    /// <summary>
    /// Klasa ruchu na reprezentacji wykonanego jako skrypt po stronie VBA.
    /// </summary>
    public class ScriptedMove : RepresentationMove
    {
        IMoveScriptEvaluator ScriptEvaluator;
        Instance Instancja;
        public ScriptedMove(IMoveScriptEvaluator scriptEvaluator, Instance par)
        {
            ScriptEvaluator = scriptEvaluator;
            Instancja = par;
        }
        public override void Move(Representation r)
        {
            ScriptEvaluator.EvaluateMove(Instancja, r);
        }
    }

    /// <summary>
    /// Abstrakcyjna klasa bazowa dla operatora krzyżowania wykonanego na parze rozwiązań (Solution)
    /// </summary>
    public abstract class CrossoverBase
    {
        public abstract Solution InternalCrossover(Solution s1, Solution s2);
        public Solution Crossover(Solution s1, Solution s2)
        {
            return InternalCrossover(s1, s2);
        }
    }

    /// <summary>
    /// Abstrakcyjna klasa bazowa dla operatora krzyżowania wykonanego na parze reprezentacji
    /// rozwiązania.
    /// Dba o synchronizację między Output i Representation.
    /// 
    /// Klasa pochodna definiuje metodę Crossover(Representation r1, Representation r2)
    /// </summary>
    public abstract class RepresentationCrossover : CrossoverBase
    {
        public abstract Representation Crossover(Representation r1, Representation r2);
        public override Solution InternalCrossover(Solution s1, Solution s2)
        {
            Solution s = s1.Clone();
            s.representation = Crossover(s1.representation, s2.representation);
            s.representation.TranslateTo(s.output);
            return s;
        }
    }

    /// <summary>
    /// Abstrakcyjna klasa bazowa dla operatora krzyżowania wykonanego na parze postaci wyjściowych
    /// rozwiązania.
    /// Dba o synchronizację między Output i Representation (jeśli jest zdefiniowane).
    /// 
    /// Klasa pochodna definiuje metodę Crossover(Output o1, Output o2)
    /// </summary>
    public abstract class OutputCrossover : CrossoverBase
    {
        public abstract Output Crossover(Output o1, Output o2);
        public override Solution InternalCrossover(Solution s1, Solution s2)
        {
            Solution s = (Solution)Activator.CreateInstance(s1.GetType());
            s.output = Crossover(s1.output, s2.output);
            if (s.representation != null) s.representation.TranslateFrom(s.output);
            return s;
        }
    }

    /// <summary>
    /// Definicja kierunku optymalizacji
    /// </summary>
    public enum OptimizationType
    {
        Minimization = 1,
        Maximization = 2
    }

    /// <summary>
    /// Abstrakcyjna klasa bazowa dla funkcji oceny rozwiązania. Udostępnia metodę Value(Solution s)
    /// oraz definiuje kierunek optymalizacji.
    /// 
    /// Klasa pochodna definiuje metodę Value(Output o), działającą na Output. 
    /// Nie przewidujemy odpowiednika działającego na reprezentacji - w takim przypadku Output może 
    /// pełnić rolę reprezentacji.
    /// </summary>
    public abstract class ObjectiveFunction
    {
        public System.IO.Pipes.NamedPipeServerStream namedPipeServer;
        public OptimizationType optType;
        public double Value(Solution s)
        {
            return Value(s.output);
        }
        public abstract double Value(Output o);
    }

    /// <summary>
    /// Interfejs do uruchomienia skryptu po stranie VBA.
    /// </summary>
    public interface IObjectiveScriptEvaluator
    {
        double EvaluateValue(Instance instancja, Output output);
    }
    /// <summary>
    /// Klasa wyznaczająca ocenę rozwiązania jako skrypt po stronie VBA.
    /// </summary>
    public class ScriptedObjective : ObjectiveFunction
    {
        IObjectiveScriptEvaluator ScriptEvaluator;
        Instance Instancja;
        public ScriptedObjective(IObjectiveScriptEvaluator scriptEvaluator, Instance par)
        {
            ScriptEvaluator = scriptEvaluator;
            Instancja = par;
            optType = OptimizationType.Maximization;
        }
        public override double Value(Output o)
        {
            return ScriptEvaluator.EvaluateValue(Instancja, o);
        }
    }

    /// <summary>
    /// Abstrakcyjna klasa bazowa dla ograniczenia definiowanego na rozwiązaniu (Solution).
    /// Udostępnia metody IsFeasible oraz Penalty.
    /// </summary>
    public abstract class Constraint
    {
        public abstract bool InternalIsFeasible(Solution s);
        public abstract double InternalPenalty(Solution s);
        public bool IsFeasible(Solution s)
        {
            return InternalIsFeasible(s);
        }
        public double Penalty(Solution s)
        {
            return InternalPenalty(s);
        }
    }

    /// <summary>
    /// Abstrakcyjna klasa bazowa dla ograniczenia definiowanego na Output.
    /// Klasapochodna definiuje metody IsFeasible oraz Penalty działąjące na Output.
    /// </summary>
    public abstract class OutputConstraint : Constraint
    {
        public abstract bool IsFeasible(Output o);
        public abstract double Penalty(Output o);
        public override bool InternalIsFeasible(Solution s)
        {
            return IsFeasible(s.output);
        }
        public override double InternalPenalty(Solution s)
        {
            return Penalty(s.output);
        }
    }

    /// <summary>
    /// Abstrakcyjna klasa bazowa dla ograniczenia definiowanego na Representation.
    /// Klasapochodna definiuje metody IsFeasible oraz Penalty działąjące na Representation.
    /// </summary>
    public abstract class RepresentationConstraint : Constraint
    {
        public abstract bool IsFeasible(Representation r);
        public abstract double Penalty(Representation r);
        public override bool InternalIsFeasible(Solution s)
        {
            return IsFeasible(s.representation);
        }
        public override double InternalPenalty(Solution s)
        {
            return Penalty(s.representation);
        }
    }

    /// <summary>
    /// Interfejs do uruchomienia skryptu po stranie VBA.
    /// </summary>
    public interface IConstraintScriptEvaluator
    {
        bool EvaluateIsFeasible(Instance instance, Output output);
        double EvaluatePenalty(Instance instance, Output output);
    }
    /// <summary>
    /// Klasa dla ograniczenia definiowanego jako skrypt po stronie VBA działający na Output.
    /// </summary>
    public class ScriptedConstraint : OutputConstraint
    {
        private IConstraintScriptEvaluator ScriptEvaluator;
        Instance Instancja;
        public ScriptedConstraint(IConstraintScriptEvaluator scriptEvaluator, Instance par)
        {
            ScriptEvaluator = scriptEvaluator;
            Instancja = par;
        }
        public override bool IsFeasible(Output o)
        {
            return ScriptEvaluator.EvaluateIsFeasible(Instancja, o);
        }
        public override double Penalty(Output o)
        {
            return ScriptEvaluator.EvaluatePenalty(Instancja, o);
        }
    }

    /// <summary>
    /// Abstrakcyjna klasa bazowa dla Pill - klasy definiującej problem optymalizacji jako czwórkę:
    /// instancja, rozwiązanie, funkcje oceny i ograniczenia.
    /// 
    /// Klasa pochodna definiuje właściwe klasy dla poszczególnych składników oraz metodę Clone.
    /// </summary>
    public abstract class Pill
    {
        public Instance instance;
        public Solution solution;
        public List<ObjectiveFunction> objectives = new List<ObjectiveFunction>();
        public List<Constraint> constraints = new List<Constraint>();
        public virtual Pill Clone()
        {
            Pill clone = (Pill)Activator.CreateInstance(this.GetType());
            clone.constraints = constraints;
            clone.instance = instance;
            clone.objectives = objectives;
            clone.solution = solution.Clone();

            return clone;
        }
    }

    /// <summary>
    /// Abstrakcyjna klasa agregująca wiele ograniczeń do pojedynczej wartości jako ważoną sumę kar za
    /// niedopuszczalność rozwiązania względem poszczególnych ograniczeń.
    /// 
    /// Klasa pochodna definiuje metody IsFeasible oraz Penalty.
    /// </summary>
    public abstract class ConstraintAggregator
    {
        public abstract bool IsFeasible(Solution s);
        public abstract double Penalty(Solution s);
    }

    /// <summary>
    /// Przykładowa klasa agregująca ograniczenia jako ważoną sumę kar za niedopuszczalność
    /// rozwiązania względem poszczególnych ograniczeń.
    /// 
    /// Udostępnia metody IsFeasible oraz Penalty
    /// </summary>
    public class ConstraintAggregatorEx : ConstraintAggregator
    {
        List<Constraint> constraints;
        public double[] weights;
        public ConstraintAggregatorEx(List<Constraint> constraints)
        {
            this.constraints = constraints;
            weights = new double[constraints.Count];
            for (int i = 0; i < constraints.Count; i++) weights[i] = 1.0;
        }
        public ConstraintAggregatorEx(List<Constraint> constraints, double[] weights)
        {
            this.constraints = constraints;
            this.weights = weights;
        }
        public override bool IsFeasible(Solution s)
        {
            for (int i = 0; i < constraints.Count; i++)
                if (constraints[i].IsFeasible(s) == false) return false;
            return true;
        }
        public override double Penalty(Solution s)
        {
            double suma = 0;
            for (int i = 0; i < constraints.Count; i++)
                suma += weights[i] * constraints[i].Penalty(s);
            return suma;
        }
    }

    /// <summary>
    /// Abstrakcyjna klasa agregująca wiele funkcji oceny rozwiązania do pojedynczej wartości.
    /// 
    /// Klasa pochodna definiuje metody Value oraz IsBetter.
    /// </summary>
    public abstract class ObjectiveAggregator
    {
        public OptimizationType optType;
        public abstract double Value(Solution s);
        public abstract Boolean IsBetter(Solution s1, Solution s2);
    }

    /// <summary>
    /// Przykładowa klasa agregująca wiele funkcji oceny rozwiązania jako ważoną sumę ocen.
    /// 
    /// Udostępnia metody Value oraz IsBetter
    /// </summary>
    public class ObjectiveAggregatorEx : ObjectiveAggregator
    {
        private List<ObjectiveFunction> objectives;
        public double[] weights;

        public ObjectiveAggregatorEx(OptimizationType optType, List<ObjectiveFunction> objectives)
        {
            this.optType = optType;
            this.objectives = objectives;

            weights = new double[objectives.Count];
            for (int i = 0; i < objectives.Count; i++) weights[i] = 1.0;
        }
        public ObjectiveAggregatorEx(OptimizationType optType, List<ObjectiveFunction> objectives, double[] weights)
        {
            this.optType = optType;
            this.objectives = objectives;
            this.weights = weights;
        }
        public override double Value(Solution s)
        {
            double suma = 0;
            for (int i = 0; i < objectives.Count; i++)
                suma += weights[i] * objectives[i].Value(s);
            return suma;
        }
        public override Boolean IsBetter(Solution s1, Solution s2)
        {
            if (optType == OptimizationType.Minimization)
            {
                if (Value(s1) < Value(s2)) return true; else return false;
            }
            else
            {
                if (Value(s1) > Value(s2)) return true; else return false;
            }
        }
    }
    /// <summary>
    /// Klasa definiuje kryterium optymalizacji dla algorytmów generycznych jako agregację wielu funkci oceny rozwiązania
    /// oraz wielu ograniczeń do jednej wartości typu double.
    /// 
    /// Udostępnia metody Value, ObjectiveValue oraz IsBetter
    /// </summary>
    public class Criterion
    {
        public OptimizationType optType;
        public ConstraintAggregator constraintAggr;
        public ObjectiveAggregator objectivesAggr;

        public Criterion()
        {
        }

        public Criterion(ObjectiveAggregator objectivesAggr)
        {
            this.objectivesAggr = objectivesAggr;
            this.optType = objectivesAggr.optType;
        }

        public Criterion(ConstraintAggregator constraintAggr)
        {
            this.constraintAggr = constraintAggr;
            this.optType = OptimizationType.Maximization;
        }

        public Criterion(ObjectiveAggregator objectivesAggr, ConstraintAggregator constraintAggr)
        {
            this.objectivesAggr = objectivesAggr;
            this.constraintAggr = constraintAggr;
            this.optType = objectivesAggr.optType;
        }

        public double Value(Solution s)
        {
            if (optType == OptimizationType.Minimization)
            {
                double suma = 0.0;
                if (constraintAggr != null) suma += constraintAggr.Penalty(s);
                if (suma > 1000d &&  objectivesAggr != null) suma += objectivesAggr.Value(s);
                return suma;
            }
            else
            {
                double suma = 0.0;
                if (constraintAggr != null) suma -= constraintAggr.Penalty(s);
                if (suma < 1000d && objectivesAggr != null) suma += objectivesAggr.Value(s);
                return suma;
            }
        }

        public double ObjectiveValue(Solution s)
        {
            double suma = 0.0;
            if (objectivesAggr != null) suma += objectivesAggr.Value(s);
            return suma;
        }

        public Boolean IsBetter(Solution s1, Solution s2)
        {
            if (s2 == null) return true;

            bool firstSolutionFeasibleTest = constraintAggr.IsFeasible(s1);
            bool secondSolutionFeasibleTest = constraintAggr.IsFeasible(s2);

            if (firstSolutionFeasibleTest && !secondSolutionFeasibleTest)
                return true;
            if (firstSolutionFeasibleTest && secondSolutionFeasibleTest)
                return objectivesAggr.IsBetter(s1, s2);
            if (!firstSolutionFeasibleTest && !secondSolutionFeasibleTest)
                if (constraintAggr.Penalty(s1) < constraintAggr.Penalty(s2))
                    return true;
                else return false;
            return false;
        }
    }
}


