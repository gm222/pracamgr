﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using Nuclex.Cloning;

using PillBase;
using BTreePill.Instance;
using BTreePill.Output;

namespace BTreePill.Solution
{
    public class BTreeSolution : PillBase.Solution
    {
        public BTreeSolution()
        {
        }

        public BTreeSolution(BTreeInstance instance)
        {
            output = new BTreeOutput(instance);
        }

        public override PillBase.Solution Clone()
        {
            return (BTreeSolution)ExpressionTreeCloner.DeepFieldClone(this);
        }
    }
}