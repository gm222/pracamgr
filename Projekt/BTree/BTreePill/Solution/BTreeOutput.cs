﻿using BTreePill.Instance;
using Nuclex.Cloning;
using PillBase;
using System;
using System.Collections.Generic;

namespace BTreePill.Output
{
    public class BTreeOutput : PillBase.Output
    {
        public List<BTNode> BTree { get; set; }
        private BTreeInstance instance;

        public bool wasCalculated = false;
        public double Cost;
        public bool wasPenaltySet;
        public double penalty;

        public BTreeOutput(BTreeInstance instance)
        {
            this.instance = instance;
            BTree = new List<BTNode>();
            //tu drzewo początkowe na podstawie instancji
            for (int k = 0; k < instance.Nodes.Count; k++)
            {
                BTree.Add(new BTNode(instance.Nodes[k], 0));
            }

            //for(int k = 1; k < instance.Nodes.Count; k++) {
            //    BTree[0].Potomki.Add(k);
            //}
        }

        public void SetTree(object[] nodesStructure)
        {
            //ustawianie rodzicow
            BTree.Clear();
            for (int i = 0; i < nodesStructure.Length; i++)
            {
                object[] structure = nodesStructure[i] as object[];
                int parent = Convert.ToInt32(structure[0]);
                List<int> children = structure[1] as List<int>;

                BTree.Add(new BTNode(instance.Nodes[i], parent));

                for (int j = 0; j < children.Count; j++)
                    BTree[i].Potomki.Add(children[j]);
            }
        }

        public override PillBase.Output Clone()
        {
            return (BTreeOutput)ExpressionTreeCloner.DeepFieldClone(this);
        }
    }

    public class BTNode
    {
        public Node Node { get; set; }
        public int Rodzic { get; set; }
        public List<int> Potomki { get; set; }

        public BTNode(Node node, int parent)
        {
            Node = node;
            Rodzic = parent;
            Potomki = new List<int>();
        }

        public virtual BTNode Clone()
        {
            return (BTNode)ExpressionTreeCloner.DeepFieldClone(this);
        }
    }

    public class BTreeOutputMove1 : OutputMove, CloneStop
    {
        static Random R = new Random();
        BTreeInstance instance;
        int[] potomki;

        public BTreeOutputMove1(BTreeInstance instance)
        {
            this.instance = instance;
            potomki = new int[instance.Nodes.Count];
        }

        private void dodajPotomki(BTreeOutput output, int node)
        {
            potomki[node] = 1;
            foreach (int potomek in output.BTree[node].Potomki) dodajPotomki(output, potomek);
        }

        public override void Move(PillBase.Output output)
        {
            BTreeOutput outt = (output as BTreeOutput);
            //losujemy przenoszony węzeł: [1; n)
            int remBlock = 1 + R.Next(instance.Nodes.Count - 1);
            //usuwamy z listy potomków rodzica
            int origRodzic = outt.BTree[remBlock].Rodzic;
            outt.BTree[origRodzic].Potomki.RemoveAll(x => x == remBlock);
            //losujemy nowego rodzica
            for (int i = 0; i < instance.Nodes.Count; i++) potomki[i] = 0;
            dodajPotomki(outt, remBlock); //wpisujemy jedynki pod indeksy potomków przenoszonego węzła (i węzła)
            List<int> rodzice = new List<int>();
            for (int i = 0; i < instance.Nodes.Count; i++)
            {
                if (potomki[i] == 0) rodzice.Add(i); //dodajemy jako potencjalnych rodziców węzły niebędące potomkami przenoszonego węzła
            }
            int newParent = rodzice[R.Next(rodzice.Count)];
            //losujemy nowe miejsce na liscie
            int newPosition = R.Next(outt.BTree[newParent].Potomki.Count + 1);
            //podpinamy węzeł
            outt.BTree[newParent].Potomki.Insert(newPosition, remBlock);
            outt.BTree[remBlock].Rodzic = newParent;
        }
    }

}
