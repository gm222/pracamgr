﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PillBase;

using BTreePill.Instance;

namespace BTreePill
{
    public class BTreePill : Pill
    {
        public BTreePill()
        {
            instance = new BTreeInstance();
            constraints = new List<PillBase.Constraint>();
            objectives = new List<PillBase.ObjectiveFunction>();
        }
    }
}
