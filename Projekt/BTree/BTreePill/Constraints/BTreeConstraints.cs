﻿using BTreePill.Output;
using Nuclex.Cloning;
using PillBase;
using System.Collections.Generic;
using System.Linq;

namespace BTreePill.Constraints
{
    public class ParentConstraint : OutputConstraint, CloneStop
    {


        public ParentConstraint()
        {

        }
        public override double Penalty(PillBase.Output o)
        {
            double penalty = 0;
            BTreeOutput BTo = (BTreeOutput)o;
            //! w przyszłości zrobić zależność taką, że jak mamy początkowe drzewo, wtedy jeśli przed jakąś akcją jest warunek to ograniczenie
            //! musi być takie, że przed tą akcją też musi być warunek

            if (BTo.wasPenaltySet)
                return BTo.penalty;

            BTo.wasPenaltySet = true;

            var root = BTo.BTree[0];

            if (root.Potomki == null || root.Potomki.Count == 0)
            {
                return BTo.penalty=100000;
            } //? root musi mieć przynajmniej jedno dziecko

            //! Warunek dla roota - musi mieć jednego kompozytora lub dekoratora lub akcje/kondycje
            if (root.Potomki.Count > 1) penalty += 500000;

            for (int i = 1; i < BTo.BTree.Count; i++)
            {

                BTNode currentNode = BTo.BTree[i];
                int currentNodeType = currentNode.Node.Type;
                int currentNodeParentType = BTo.BTree[currentNode.Rodzic].Node.Type;
                List<int> currentNodeChildren = BTo.BTree[i].Potomki;


                if (currentNodeType == 8)
                {
                    if (currentNodeChildren.Count == 0)
                        penalty += 5;
                    else
                    {
                        int emptySelectorsChildren = currentNodeChildren.Count(
                                x => BTo.BTree[x].Node.Type == 7 && BTo.BTree[x].Potomki.Count == 0);
                        if (emptySelectorsChildren > 0)
                            penalty += emptySelectorsChildren*10000;
                    }
                }

                if (currentNodeType == 7)
                {
                    if (currentNodeChildren.Count == 0)
                    {
                        penalty += 1000;
                        if (currentNodeParentType == 8)
                            penalty += 1000;
                    }
                    else
                    {
                        int badNodeTest = currentNodeChildren.Count(x => BTo.BTree[x].Node.Type == 8 && BTo.BTree[x].Potomki.Count == 0);
                        if (badNodeTest > 0)
                            penalty += badNodeTest*10000;
                    }
                }

                //? Warunki na rodzica
                if ((currentNodeType < 5 || currentNodeType == 9) && (currentNodeParentType < 5 || currentNodeParentType == 9))
                    penalty+=50000;

                //? Warunki na dzieci
                //! Dla liści
                if ((currentNodeType < 5 || currentNodeType == 9) && (currentNode.Potomki != null && BTo.BTree[i].Potomki.Count > 0))
                    penalty+=50000;

                //! Dla dekoratorów
                if ((currentNodeType > 9 && currentNodeType < 22) && (currentNode.Potomki != null && (BTo.BTree[i].Potomki.Count == 0 || BTo.BTree[i].Potomki.Count > 1)))
                    penalty+=50000;
            }
            BTo.penalty = penalty;
            return penalty;
        }

        public override bool IsFeasible(PillBase.Output o)
        {

            BTreeOutput BTo = (BTreeOutput)o;

            //return Penalty(o) == 0;
            return Penalty(o) <= BTo.BTree.Count*5;
        }
    }
}
