﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using Nuclex.Cloning;
using PillBase;


namespace BTreePill.Instance
{
    public partial class BTreeInstance : PillBase.Instance, CloneStop
    {
        public List<Node> Nodes { get; set; }
    }

    public class Node : CloneStop
    {
        public int Type { get; set; }

        public Node(int t)
        {
            Type = t;
        }
    }

}
