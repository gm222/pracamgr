﻿using BTreePill.Instance;
using BTreePill.Output;
using HelpFunctions;
using Nuclex.Cloning;
using PillBase;
using System;
using System.Collections.Generic;

namespace BTreePill.Objectives
{
    /// <summary>
    /// Klasa definiuje kryterium dla problemu PDPVRP okreslone przez
    /// sume przychodu z realizacji zlecen pomniejszona o koszt przejazdu tras przez pojazdy
    /// </summary>
    public class ExampleObjectiveFunction : ObjectiveFunction, CloneStop
    {
        BTreeInstance instance;
        List<int> permutacja = new List<int>();

        public ExampleObjectiveFunction(PillBase.Instance par)
        {
            instance = (par as BTreeInstance);
            optType = OptimizationType.Maximization;
        }

        private void searchPotomki(BTreeOutput output, int node)
        {
            foreach (int potomek in output.BTree[node].Potomki)
            {
                permutacja.Add(potomek);
                searchPotomki(output, potomek);
            }
        }

        public object[] GetTreeStructure(PillBase.Output s)
        {
            BTreeOutput BTo = (BTreeOutput)s;

            //ustalenie kolejnosci dodawania węzłów do drzewa (na liście nie ma root'a)
            permutacja.Clear();
            searchPotomki(BTo, 0);

            object[] treeStructure = new object[permutacja.Count+1];

            for (int i = 0; i < treeStructure.Length; i++)
                treeStructure[i] = new List<int>();

            foreach (int node in permutacja)
            {
                //utwórz węzeł o nr = node ...
                //... i podepnij go pod węzeł o nr = parent
                int parent = BTo.BTree[node].Rodzic;// perm + tab rodzicow
                (treeStructure[parent] as List<int>).Add(node);
            }

            return treeStructure;
        }

        public override double Value(PillBase.Output s)
        {
            BTreeOutput BTo = (BTreeOutput)s;

            if (BTo.wasCalculated) return BTo.Cost;

            //ustalenie kolejnosci dodawania węzłów do drzewa (na liście nie ma root'a)
            permutacja.Clear();
            searchPotomki(BTo, 0);
            //przykład tworzenia drzewa
            //zakłądamy, że istnieje ROOT o nr = 0

            object[] treeStructure = new object[permutacja.Count];

            for (int i = 0; i < treeStructure.Length; i++)
                treeStructure[i] = new List<int>();

            foreach (int node in permutacja)
            {
                try
                {
                    //utwórz węzeł o nr = node ...
                    //... i podepnij go pod węzeł o nr = parent
                    int parent = BTo.BTree[node].Rodzic % treeStructure.Length; // perm + tab rodzicow
                    (treeStructure[parent] as List<int>).Add(node);
                }
                catch (Exception ex)
                {
                    //ignore   
                }
            }

            float cost = optType == OptimizationType.Maximization ? float.MaxValue : 0f;

            if (namedPipeServer != null)
            {
                //############################# WYSYŁANIE ##############################
                if (namedPipeServer.IsConnected)
                {
                    //object sendObject = new object[] { permutacja };
                    HelpFunction.PipeWrite(namedPipeServer, new object[] { treeStructure, permutacja });
                }
                //######################################################################

                //############################# ODBIERANIE #############################
                if (namedPipeServer.IsConnected)
                {
                    object[] clientResponse = HelpFunction.PipeRead(namedPipeServer) as object[];

                    if (clientResponse != null && clientResponse.Length >= 1)
                        cost = (float)clientResponse[0];

                }
                //######################################################################
            }

            BTo.Cost = cost;
            BTo.wasCalculated = true;

            //tutaj robie tworzenie nowego drzewa następnie wysyłam do unity i czekam na wynik funkcji oceniającej jakość symulacji


            //oczekiwanie rpc pilowanie
            //tataj komunikacja z unity - wysyłanie

            //tutaj też komunikacja unity z programem do zwracania danych
            //double max = BTo.BTree[0].Potomki.Count; //tu wartość funkcji kryterialnej dla drzewa (im mniej tym lepsze drzewo)
            //return max;
            return cost;
        }
    }

}
