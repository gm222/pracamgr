﻿using Algorithms.Components;
using Algorithms.Lib;
using HelpFunctions;
using PillBase;
using System;
using System.IO;
using System.IO.Pipes;
using System.Threading;

namespace BTree
{
    public class LoggerSingleton
    {
        public static LoggerSingleton Instance { get; } = new LoggerSingleton();
        public void Save(string text)
        {
            using (FileStream fs = File.Create(Directory.GetCurrentDirectory() + @"\\simulation_results.txt"))
            {
                using (StreamWriter tw = new StreamWriter(fs))
                {
                    tw.Write(text);
                    Thread.Sleep(1000);
                }
            }
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            int steps = 1;
            double temperature = 20000;
            using (NamedPipeServerStream namedPipeServer = new NamedPipeServerStream("test-pipe", PipeDirection.InOut,
                    1, PipeTransmissionMode.Message))
            {
                namedPipeServer.WaitForConnection();
                namedPipeServer.ReadMode = PipeTransmissionMode.Message;

                object[] clientResponse = HelpFunction.PipeRead(namedPipeServer) as object[];
                if (clientResponse != null && clientResponse.Length > 0)
                {
                    object s = clientResponse[0];
                    object t = clientResponse[1];

                    if (s is int)
                        steps = (int)s;

                    if (t is double)
                        temperature = (double)t;

                }
            }

            Console.WriteLine(AppDomain.CurrentDomain.BaseDirectory);
            BTreePill.BTreePill pill = new BTreePill.BTreePill();
            Criterion criterion2 = new Criterion(); //utworzenie obiektu definiującego kryterium optymalizacji dla algorytmu
            criterion2.optType        = OptimizationType.Maximization;
            criterion2.objectivesAggr = new ObjectiveAggregatorEx(OptimizationType.Maximization, pill.objectives);
            double[] weights = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }; //tablica wag poszczególnych ograniczeń - tyle elementów ile mamy ograniczeń
            criterion2.constraintAggr = new ConstraintAggregatorEx(pill.constraints, weights); //zdefiniowanie postaci agregatora ograniczeń

            string text = new SimulatedAnnealing(
                new SingleRandomMove(0),
                new DefaultTemperatureComponent(temperature, 0.998),
                new MaxSteps(steps)
                ).Run2(pill, criterion2);
            Console.WriteLine("okej");
            LoggerSingleton.Instance.Save(text);

            //Console.WriteLine("Waiting to send the best solution to unity");
            //// Console.WriteLine($"The best solution cost: {criterion2.Value(pill2Best.solution)}");

            ////wysyłka ostatniego drzewa
            //var obiective = pill2Best.objectives[0] as BTreePill.Objectives.ExampleObjectiveFunction;
            //try
            //{
            //    obiective?.namedPipeServer?.Disconnect();
            //    obiective?.namedPipeServer?.Dispose();
            //}
            //catch (Exception)
            //{
            //    // ignored
            //}

            //if (obiective != null)
            //{
            //    obiective.namedPipeServer = null;
            //}

            //ExampleObjectiveFunction exampleObjectiveFunction = pill2Best.objectives[0] as BTreePill.Objectives.ExampleObjectiveFunction;
            //if (exampleObjectiveFunction != null)
            //{
            //    object[] treeStructure = exampleObjectiveFunction.GetTreeStructure(pill.solution.output);

            //    using (NamedPipeServerStream namedPipeServer = new NamedPipeServerStream("test2", PipeDirection.InOut, 1, PipeTransmissionMode.Message))
            //    {
            //        namedPipeServer.WaitForConnection();
            //        namedPipeServer.ReadMode = PipeTransmissionMode.Message;

            //        //############################# WYSYŁANIE ##############################
            //        if (namedPipeServer.IsConnected)
            //        {
            //            HelpFunction.PipeWrite(namedPipeServer, new object[] { treeStructure });
            //        }
            //        //######################################################################
            //    }
            //}
            ////mozna dodać porównanie stare i nowe do wyświetlenia

            //Console.WriteLine("Sended :) Press any key to exit");
            Console.ReadKey();

            //(pill.solution.output as BTreePill.Output.BTreeOutput).BTree; // dostanie się do rozwiązania
        }
    }
}
