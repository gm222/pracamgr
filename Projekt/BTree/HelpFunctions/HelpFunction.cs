﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Runtime.Serialization.Formatters.Binary;

namespace HelpFunctions {
    public static class HelpFunction {

        public static void PipeWrite(System.IO.Pipes.PipeStream pipe, object message) {
            try {
                byte[] byteMsg = ObjectToByteArray(message);
                pipe.Write(byteMsg, 0, byteMsg.Length);
                pipe.Flush();
                pipe.WaitForPipeDrain();
            } catch(Exception) { }
        }

        public static object PipeRead(PipeStream pipe) {
            byte[] messageBuffer = new byte[1000000];
            List<byte> list      = new List<byte>();

            try {
                do {
                    int r = pipe.Read(messageBuffer, 0, messageBuffer.Length);
                    if(r == 0) break;
                    list.AddRange(messageBuffer);
                    messageBuffer = new byte[messageBuffer.Length];
                    break;
                }
                while(!pipe.IsMessageComplete);
            } catch(Exception) {
                return null;
            }

            return ByteArrayToObject(list.ToArray());

        }

        private static byte[] ObjectToByteArray(object obj) {
            if(obj == null)
                return null;

            byte[] arr;
            try {
                BinaryFormatter bf = new BinaryFormatter();
                using(MemoryStream ms = new MemoryStream()) {
                    bf.Serialize(ms, obj);
                    ms.Flush();
                    arr = ms.ToArray();
                }
            } catch(Exception) { return null; }

            return arr;
        }

        private static System.Object ByteArrayToObject(byte[] arrBytes) {
            BinaryFormatter binForm = new BinaryFormatter();
            object obj;
            try {
                using(MemoryStream memStream = new MemoryStream()) {
                    memStream.Write(arrBytes, 0, arrBytes.Length);
                    memStream.Seek(0, SeekOrigin.Begin);
                    obj  = binForm.Deserialize(memStream) as object;
                    memStream.Flush();
                }
            } catch(Exception) {
                return null;
            }

            return obj;
        }
    }
}
